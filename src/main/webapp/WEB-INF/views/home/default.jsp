<%--
  Created by IntelliJ IDEA.
  User: honza
  Date: 8. 10. 2018
  Time: 19:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:layout-frontend>
    <jsp:attribute name="title">Domovská stránka</jsp:attribute>

    <jsp:body>

        <div class="ui segments" style="padding-left: 0!important; padding-right: 0!important;">
            <div class="ui blue raised very padded segment" style="width: 100%">
                <h2 class="ui header">Vítejte v GreyBank</h2>
            </div>
            <div class="ui segment" style="padding: 0!important;">
                <img class="ui fluid image" src="${request.contextPath}/frontend/img/homepage.png" alt="no-money">
            </div>
            <div class="ui segment">
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec vitae turpis quis nisi aliquet egestas. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Curabitur a neque non enim euismod tempor. Duis a ipsum vel odio efficitur tristique rutrum nec turpis. Nam consectetur nisl tellus, a semper metus bibendum molestie. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Suspendisse eget ex condimentum, molestie tellus a, laoreet ipsum. Donec tristique tellus magna, egestas tristique metus tincidunt aliquet. Pellentesque semper justo eleifend nibh convallis, eget eleifend dui accumsan. Curabitur at magna pulvinar, iaculis ex vel, molestie ligula. Nam eu rhoncus tellus. Quisque sit amet pretium velit, ac tincidunt eros. Donec at erat est. Mauris vehicula ante eu purus tempor maximus. Integer scelerisque condimentum ipsum eu blandit. Nunc sollicitudin leo non blandit convallis.</p>
                <p>Etiam mollis volutpat mi non placerat. Maecenas vehicula hendrerit nunc in viverra. Curabitur sed pharetra justo. Proin ac lacus sed enim tristique laoreet quis consequat ligula. Pellentesque ut libero id ligula congue tincidunt at nec odio. Proin ac auctor risus. Cras rhoncus tristique rutrum. Mauris volutpat libero et orci congue, vitae cursus erat tincidunt. Pellentesque placerat tortor vel sodales euismod. Proin metus sem, euismod eget scelerisque nec, suscipit sed velit. Proin condimentum consectetur nulla ut ornare.</p>
            </div>
        </div>
    </jsp:body>
</t:layout-frontend>