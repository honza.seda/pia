<%--
  Created by IntelliJ IDEA.
  User: honza
  Date: 27. 12. 2018
  Time: 22:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="editDetailsUri" value="${s:mvcUrl('UserCtl#edit').build()}" scope="page"/>
<c:set var="submitUserSettingsUri" value="${s:mvcUrl('UserCtl#submitUserSettings').build()}" scope="page"/>

<t:layout-frontend>
    <jsp:attribute name="title">Podrobnosti účtu</jsp:attribute>

    <jsp:body>
        <div class="ui blue raised very padded segment" style="width: 100%">
            <h2 class="ui header"><i class="info icon blue"></i> Podrobnosti účtu</h2>
            <div class="ui equal width grid stackable">
                <div class="column">
                    <div class="ui equal width grid stackable">
                        <div class="column">
                            <div class="column">
                                <h4>Majitel účtu:</h4>
                                <div class="ui list">
                                    <div class="item">
                                        <i class="id badge outline icon"></i>
                                        <div class="content">
                                            ${userDetail.firstName} ${userDetail.lastName}<br>
                                            ${userDetail.pin}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="equal width row">
                            <div class="column">
                                <h4>Kontaktní údaje:</h4>
                                <div class="ui list">
                                    <div class="item">
                                        <i class="marker icon"></i>
                                        <div class="content">
                                            <c:if test="${!empty(userDetail.street) || !empty(userDetail.houseNumber)}">
                                                ${userDetail.street} ${userDetail.houseNumber}
                                                <br>
                                            </c:if>
                                            <c:if test="${!empty(userDetail.city) || !empty(userDetail.zip)}">
                                                ${userDetail.city}, ${userDetail.zip}
                                                <br>
                                            </c:if>
                                            <c:if test="${!empty(userDetail.country)}">
                                                ${userDetail.country.labelName}
                                                <br>
                                            </c:if>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <i class="mail icon"></i>
                                        <div class="content">
                                                ${userDetail.email}
                                        </div>
                                    </div>
                                    <div class="item">
                                        <i class="phone icon"></i>
                                        <div class="content">
                                                ${userDetail.phone}
                                        </div>
                                    </div>
                                </div>

                                <a href="${editDetailsUri}"><i class="edit icon"></i> Upravit kontaktní údaje</a>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="column">
                    <div class="ui equal width grid stackable">
                        <div class="column">
                            <h4>Bankovní účet:</h4>
                            <div class="ui list">
                                <div class="item">
                                    <i class="id card icon"></i>
                                    <div class="content">

                                    ${userAccount.accountNumber}/${bankCode}<br>
                                        ${userAccount.currency}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="equal width row">
                            <div class="column">
                                <h4>Platební karta:</h4>
                                <div class="ui list">
                                    <div class="item">
                                        <i class="credit card icon"></i>
                                        <div class="content">
                                                <fmt:formatNumber type = "number" pattern = "####,####,####,####" value = "${card.cardNumber}" />
                                        <br>
                                            expirace: ${card.expirationYear}/${card.expirationMonth}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="ui blue raised very padded segment" style="width: 100%">
            <h2 class="ui header"><i class="settings icon blue"></i> Další nastavení účtu</h2>
            <form method="post" action="${submitUserSettingsUri}" style="width: 100%; padding: 0 !important; margin: 0!important;">
                <table class="ui very basic table very padded">
                    <thead>
                        <tr>
                            <th class="ten wide">Nastavení</th>
                            <th class="six wide right aligned">Hodnota</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Emailové notifikace (oznámení o platbách)</td>
                        <td class="right aligned">
                            <div class="ui toggle checkbox">
                                <form:checkbox path="userAccount.sendNotifications" />
                                <label></label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Zahraniční platby kartou</td>
                        <td class="right aligned">
                            <div class="ui toggle checkbox">
                                <form:checkbox path="card.internationalPayments" />
                                <label></label>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="ui grid">
                    <div class="ui right aligned column">
                        <button type="submit" class="ui primary basic button">Uložit nastavení</button>
                    </div>
                </div>
            </form>
        </div>
    </jsp:body>
</t:layout-frontend>
