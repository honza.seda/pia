<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<c:set var="loginUri" value="${s:mvcUrl('UserCtl#login').build()}" scope="page"/>

<t:layout-frontend>
    <jsp:attribute name="title">Přihlášení</jsp:attribute>

    <jsp:body>
        <div class="ui two column centered grid">
            <div class="sixteen wide tablet eight wide computer column">
                <form name='loginForm' class="ui large form"
                      action="${loginUri}" method='POST'>
                    <div class="ui blue padded segment">
                        <div class="ui blue header large">
                            <div class="content">
                                Přihlášení do GreyBank
                            </div>
                        </div>
                        <div class="ui basic padded segment">
                            <div class="field">
                                <div class="ui left icon input">
                                    <i class="user icon"></i>
                                    <input type="text" name="login" placeholder="Uživatelské jméno" value="${login}">
                                </div>
                            </div>
                            <div class="field">
                                <div class="ui left icon input">
                                    <i class="lock icon"></i>
                                    <input type="password" name="password" placeholder="Heslo">
                                </div>
                            </div>

                            <input name="submit" type="submit"
                                   value="Přihlásit se" class="ui fluid large blue submit button">
                            <c:if test="${param.failedLogin != null}">
                                <div class="ui center aligned padded grid">
                                    <div class="ui pointing red basic label">
                                        Špatné uživatelské jméno nebo heslo
                                    </div>
                                </div>
                            </c:if>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </jsp:body>
</t:layout-frontend>