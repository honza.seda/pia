<%--
  Created by IntelliJ IDEA.
  User: honza
  Date: 30. 12. 2018
  Time: 23:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:layout-error>
    <jsp:attribute name="title">Chyba</jsp:attribute>

    <jsp:body>

            <h2 class="ui center aligned very padded icon header">
                <i class="exclamation circle blue icon"></i>
                <div class="content">
                    Chyba
                    <div class="sub header">Při zpracování požadavku došlo k blíže nespecifikované chybě.</div>
                </div>
            </h2>
    </jsp:body>
</t:layout-error>
