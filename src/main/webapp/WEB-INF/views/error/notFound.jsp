<%--
  Created by IntelliJ IDEA.
  User: honza
  Date: 30. 12. 2018
  Time: 23:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:layout-error>
    <jsp:attribute name="title">Stránka nenalezena</jsp:attribute>

    <jsp:body>
            <h2 class="ui center aligned very padded icon header">
                <i class="file outline blue icon"></i>
                <div class="content">
                    404 - Stránka nenalezena
                    <div class="sub header">Stránka kterou se pokoušíte načíst nebyla nalezena.</div>
                </div>
            </h2>
    </jsp:body>
</t:layout-error>
