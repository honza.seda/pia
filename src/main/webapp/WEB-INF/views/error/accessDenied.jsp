<%--
  Created by IntelliJ IDEA.
  User: honza
  Date: 31. 12. 2018
  Time: 14:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:layout-error>
    <jsp:attribute name="title">Přístup odepřen</jsp:attribute>

    <jsp:body>
            <h2 class="ui center aligned very padded icon header">
                <i class="user times blue icon"></i>
                <div class="content">
                    403 - Přístup odepřen
                    <div class="sub header">Pro zobrazení této stránky nemáte dostatečná oprávnění</div>
                </div>
            </h2>
    </jsp:body>
</t:layout-error>
