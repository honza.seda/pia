<%--
  Created by IntelliJ IDEA.
  User: honza
  Date: 6. 1. 2019
  Time: 20:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>

<t:layout-error>
    <jsp:attribute name="title">Neplatný požadavek</jsp:attribute>

    <jsp:body>
        <h2 class="ui center aligned very padded icon header">
            <i class="exclamation circle blue icon"></i>
            <div class="content">
                400 - Neplatný požadavek
                <div class="sub header">Chybně zapsaný požadavek.</div>
            </div>
        </h2>
    </jsp:body>
</t:layout-error>
