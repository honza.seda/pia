<%--
  Created by IntelliJ IDEA.
  User: honza
  Date: 8. 10. 2018
  Time: 19:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="transactionHistoryUri" value="${s:mvcUrl('TransactionCtl#history').build()}" scope="page"/>
<c:set var="transactionDetailUri" value="${s:mvcUrl('TransactionCtl#detail').build()}" scope="page"/>


<t:layout-frontend>
    <jsp:attribute name="title">Domovská stránka</jsp:attribute>

    <jsp:body>

        <div class="ui blue raised very padded segment" style="width: 100%">
            <h2 class="ui header"><i class="address card outline icon blue"></i> Přehled účtu</h2>
            <div class="ui equal width grid stackable">
                <div class="column">
                    <div class="ui equal width grid stackable">
                        <div class="column">
                            <div class="column"><h4>Číslo účtu:</h4>${account.accountNumber}/${bankCode}</div>
                        </div>

                        <div class="equal width row">
                            <div class="column"><h4>Majitel účtu:</h4>${userName}</div>
                        </div>

                    </div>
                </div>
                <div class="column">
                    <div class="ui equal width grid stackable">
                        <div class="column"><h4>Běžný zůstatek:</h4>
                            <fmt:setLocale value = "cs"/>
                            <fmt:formatNumber type = "number" minFractionDigits = "2" maxFractionDigits = "2" value = "${account.balance}" />
                                ${account.currency}</div>
                        <div class="equal width row">
                            <div class="column" data-inverted=""
                                 data-tooltip="Aktuální zůstatek včetně transakcí, které čekají na zpracování"
                                 data-variation="wide"
                                 data-position="bottom left">
                                <h4>
                                    Použitelný zůstatek: <i class="info circle blue icon"></i>
                                </h4>
                                <fmt:formatNumber type = "number" minFractionDigits = "2" maxFractionDigits = "2" value = "${account.availableBalance}" />
                                    ${account.currency}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="ui blue raised very padded segment" style="width: 100%">
            <h2 class="ui header"><i class="clock outline icon blue"></i> Poslední transakce</h2>
            <table class="ui very basic selectable very padded stackable table">
                <thead>
                <tr>
                    <th>Datum</th>
                    <th>Podrobnosti</th>
                    <th class="ui right aligned">Částka</th>
                </tr>
                </thead>
                <tbody>
                <c:choose>
                    <c:when test="${empty(recentTransactions)}">
                        <tr>
                            <td class="center aligned" colspan="3">
                                Nenalezeny žádné transakce.
                            </td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach items="${recentTransactions}" var="item">
                            <tr>
                                <td>
                                    <a id="detail-link" href="${transactionDetailUri}${item.id}">
                                            ${item.scheduledTime.dayOfMonth}. ${item.scheduledTime.monthValue}. <small>${item.scheduledTime.year}</small>
                                    </a>
                                </td>
                                <td>
                                    <div class="ui image header large">
                                        <c:choose>
                                            <c:when test="${item.outgoing}">
                                                <i class="minus icon red"></i>
                                                <div class="content">
                                                    ${item.transactionDetail.targetAccountNumber}/${item.transactionDetail.targetBankCode}
                                                    <div class="sub header">
                                                        Odchozí úhrada.
                                            </c:when>
                                            <c:otherwise>
                                                <i class="plus icon"></i>
                                                <div class="content">
                                                        ${item.transactionDetail.senderAccountNumber}/${item.transactionDetail.senderBankCode}
                                                    <div class="sub header">
                                                        Příchozí úhrada.
                                            </c:otherwise>
                                        </c:choose>
                                                        <c:if test="${!empty(item.transactionDetail.ks)}">KS: ${item.transactionDetail.ks}, </c:if>
                                                        <c:if test="${!empty(item.transactionDetail.ss)}">SS: ${item.transactionDetail.ss}, </c:if>
                                                        <c:if test="${!empty(item.transactionDetail.vs)}">VS: ${item.transactionDetail.vs}, </c:if>
                                                        <c:if test="${!empty(item.transactionDetail.message)}"><br>${item.transactionDetail.message}</c:if>

                                                    </div>
                                                </div>
                                    </div>
                                </td>
                                <td class="ui <c:if test="${item.outgoing}">red</c:if> header right aligned">
                                    <fmt:setLocale value = "cs"/>
                                    <c:if test="${item.outgoing}">-</c:if><fmt:formatNumber value = "${item.transactionDetail.amount.doubleValue()}" minFractionDigits = "2" maxFractionDigits = "2" /> ${account.currency}
                                </td>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
                </tbody>
            </table>
            <a href="${transactionHistoryUri}"><i class="calendar outline icon"></i> Historie transakcí</a>
        </div>

        <c:if test="${!empty(waitingTransactions)}">
            <div class="ui blue raised very padded segment" style="width: 100%">
                <h2 class="ui header"><i class="clock outline icon blue"></i> Transakce čekající na zpracování</h2>
                <table class="ui very basic table very padded stackable">
                    <thead>
                    <tr>
                        <th>Datum splatnosti</th>
                        <th>Podrobnosti</th>
                        <th class="ui right aligned">Částka</th>
                    </tr>
                    </thead>
                    <tbody>
                            <c:forEach items="${waitingTransactions}" var="item">
                                <tr>
                                    <td>
                                            ${item.transactionDetail.dueDate.dayOfMonth}.${item.transactionDetail.dueDate.monthValue}.${item.transactionDetail.dueDate.year}
                                    </td>
                                    <td>
                                        <div class="ui image header large">
                                            <div class="content">
                                                    ${item.transactionDetail.targetAccountNumber}/${item.transactionDetail.targetBankCode}
                                                <div class="sub header">
                                                    Odchozí úhrada.
                                                    <c:if test="${!empty(item.transactionDetail.ks)}">KS: ${item.transactionDetail.ks}, </c:if>
                                                    <c:if test="${!empty(item.transactionDetail.ss)}">SS: ${item.transactionDetail.ss}, </c:if>
                                                    <c:if test="${!empty(item.transactionDetail.vs)}">VS: ${item.transactionDetail.vs}, </c:if>
                                                    <c:if test="${!empty(item.transactionDetail.message)}"><br>${item.transactionDetail.message}</c:if>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="ui red header right aligned">
                                        <fmt:setLocale value = "cs"/>
                                        -<fmt:formatNumber value = "${item.transactionDetail.amount.doubleValue()}" minFractionDigits = "2" maxFractionDigits = "2" /> ${account.currency}
                                    </td>
                                </tr>
                            </c:forEach>
                    </tbody>
                </table>
            </div>
        </c:if>

        <script>
            $('table.selectable tbody tr').click( function() {
                window.location = $(this).find('#detail-link').attr('href');
            }).hover( function() {
                $(this).toggleClass('hover');
            });
        </script>

    </jsp:body>
</t:layout-frontend>