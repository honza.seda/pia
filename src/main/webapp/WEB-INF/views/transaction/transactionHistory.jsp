<%--
  Created by IntelliJ IDEA.
  User: honza
  Date: 26. 12. 2018
  Time: 16:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@taglib prefix="tc" tagdir="/WEB-INF/tags/components" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="paginatorLinkUri" value="${s:mvcUrl('TransactionCtl#history').build()}" scope="page"/>
<c:set var="transactionDetailUri" value="${s:mvcUrl('TransactionCtl#detail').build()}" scope="page"/>

<t:layout-frontend>
    <jsp:attribute name="title">Historie transakcí</jsp:attribute>

    <jsp:body>

        <div class="ui blue raised very padded segment" style="width: 100%">
            <h2 class="ui header"><i class="history icon blue"></i>Historie transakcí</h2>
            <table class="ui very basic selectable very padded stackable table">
                <thead>
                <tr>
                    <th>Datum</th>
                    <th>Podrobnosti</th>
                    <th class="ui right aligned">Částka</th>
                </tr>
                </thead>
                <tbody>
                <c:choose>
                    <c:when test="${empty(transactions)}">
                        <tr>
                            <td class="center aligned" colspan="3">
                                Nenalezeny žádné transakce.
                            </td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach items="${transactions}" var="item">
                            <tr>
                                <td>
                                    <a id="detail-link" href="${transactionDetailUri}${item.id}">
                                            ${item.processedTime.dayOfMonth}. ${item.processedTime.monthValue}. <small>${item.processedTime.year}</small>
                                    </a>
                                </td>
                                <td>
                                    <div class="ui image header large">
                                        <c:choose>
                                        <c:when test="${item.outgoing}">
                                        <i class="minus icon red"></i>
                                        <div class="content">
                                                ${item.transactionDetail.targetAccountNumber}/${item.transactionDetail.targetBankCode}
                                            <div class="sub header">
                                                Odchozí úhrada.

                                                </c:when>
                                                <c:otherwise>
                                                <i class="plus icon"></i>
                                                <div class="content">
                                                        ${item.transactionDetail.senderAccountNumber}/${item.transactionDetail.senderBankCode}
                                                    <div class="sub header">
                                                        Příchozí úhrada.
                                                        </c:otherwise>
                                                        </c:choose>

                                                        <c:if test="${!empty(item.transactionDetail.ks)}">KS: ${item.transactionDetail.ks}, </c:if>
                                                        <c:if test="${!empty(item.transactionDetail.ss)}">SS: ${item.transactionDetail.ss}, </c:if>
                                                        <c:if test="${!empty(item.transactionDetail.vs)}">VS: ${item.transactionDetail.vs}, </c:if>
                                                        <c:if test="${!empty(item.transactionDetail.message)}"><br>${item.transactionDetail.message}</c:if>

                                                    </div>
                                                </div>
                                            </div>
                                </td>
                                <td class="ui <c:if test="${item.outgoing}">red</c:if> header right aligned">
                                    <fmt:setLocale value = "cs"/>
                                    <c:if test="${item.outgoing}">-</c:if><fmt:formatNumber value = "${item.transactionDetail.amount.doubleValue()}" minFractionDigits = "2" maxFractionDigits = "2" /> ${account.currency}
                                </td>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
                </tbody>
            </table>

            <tc:paginator/>
        </div>

        <script>
            $('tbody tr').click( function() {
                window.location = $(this).find('#detail-link').attr('href');
            }).hover( function() {
                $(this).toggleClass('hover');
            });
        </script>

    </jsp:body>
</t:layout-frontend>