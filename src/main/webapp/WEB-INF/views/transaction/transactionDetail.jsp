<%--
  Created by IntelliJ IDEA.
  User: honza
  Date: 30. 12. 2018
  Time: 19:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="transactionHistoryUri" value="${s:mvcUrl('TransactionCtl#history').build()}" scope="page"/>

<t:layout-frontend>
    <jsp:attribute name="title">Detail transakce</jsp:attribute>

    <jsp:body>

        <div class="ui blue raised very padded segment" style="width: 100%">
            <div class="ui equal width grid segment-header">
                <div class="left floated column">
                    <div class="ui header large"><i class="file alternate outline blue icon"></i> Detail transakce</div>
                </div>
                <div class="right floated right aligned column">
                    <a href="${transactionHistoryUri}" class="ui primary button"><i class="angle left icon"></i> Zpět</a>
                </div>
            </div>

            <div class="ui equal width stackable grid">
                <div class="column">
                    <h5 class="ui header">Účet odesílatele</h5>
                        ${transaction.transactionDetail.senderAccountNumber}/${transaction.transactionDetail.senderBankCode}
                </div>
                <div class="column">
                    <h5 class="ui header">Účet příjemce</h5>
                        ${transaction.transactionDetail.targetAccountNumber}/${transaction.transactionDetail.targetBankCode}
                </div>


                <div class="equal width row">
                    <div class="column">
                        <h5 class="ui header" style="margin-bottom: 0!important;">Částka</h5>
                        <div class="ui <c:if test="${transaction.outgoing}">red</c:if> tiny header" style="margin-top: 0 !important;">
                            <fmt:setLocale value="cs"/>
                            <c:if test="${transaction.outgoing}">-</c:if><fmt:formatNumber value="${transaction.transactionDetail.amount.doubleValue()}" minFractionDigits="2"
                                              maxFractionDigits="2"/> CZK
                        </div>
                    </div>
                </div>

                <div class="equal width row">
                    <div class="column">
                        <h5 class="ui header">Typ transakce</h5>
                        <c:choose>
                            <c:when test="${transaction.outgoing}">
                                Odchozí úhrada.
                            </c:when>
                            <c:otherwise>
                                Příchozí úhrada.
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>


                <div class="equal width row">
                    <div class="column">
                        <h5 class="ui header">Datum splatnosti</h5>
                            ${transaction.transactionDetail.dueDate.dayOfMonth}.${transaction.transactionDetail.dueDate.monthValue}.${transaction.transactionDetail.dueDate.year}
                    </div>
                    <div class="column">
                        <h5 class="ui header">Zpracování transakce</h5>
                            ${transaction.processedTime.dayOfMonth}.${transaction.processedTime.monthValue}.${transaction.processedTime.year} ${transaction.processedTime.hour}:${transaction.processedTime.minute}
                    </div>
                </div>

                <div class="ui divider"></div>

                <div class="equal width row">
                    <div class="column">
                        <h5 class="ui grey header">Konstantní symbol</h5>
                            ${transaction.transactionDetail.ks}
                    </div>
                    <div class="column">
                        <h5 class="ui grey header">Variabilní symbol</h5>
                            ${transaction.transactionDetail.vs}
                    </div>
                    <div class="column">
                        <h5 class="ui grey header">Specifický symbol</h5>
                            ${transaction.transactionDetail.ss}
                    </div>
                </div>

                <div class="equal width row">
                    <div class="column">
                        <h5 class="ui grey header">Poznámka</h5>
                            ${transaction.transactionDetail.message}
                    </div>
                </div>
            </div>
        </div>
    </jsp:body>
</t:layout-frontend>