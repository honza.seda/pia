<%--
  Created by IntelliJ IDEA.
  User: honza
  Date: 3. 1. 2019
  Time: 16:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@taglib prefix="tc" tagdir="/WEB-INF/tags/components" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="newStandingOrderUri" value="${s:mvcUrl('TransactionCtl#standingNew').build()}" scope="page"/>
<c:set var="standingTransactionDetailUri" value="${s:mvcUrl('TransactionCtl#standingDetail').build()}" scope="page"/>
<c:set var="standingTransactionCancelUri" value="${s:mvcUrl('TransactionCtl#standingCancel').build()}" scope="page"/>
<c:set var="standingTransactionEditUri" value="${s:mvcUrl('TransactionCtl#standingEdit').build()}" scope="page"/>

<c:set var="paginatorLinkUri" value="${s:mvcUrl('TransactionCtl#standing').build()}" scope="page"/>

<t:layout-frontend>
    <jsp:attribute name="title">Trvalé příkazy</jsp:attribute>
    <jsp:body>
        <div class="ui blue raised very padded segment" style="width: 100%">
            <div class="ui grid segment-header">
                <div class="left floated six wide column">
                    <div class="ui header large"><i class="clock outline blue icon"></i> Trvalé příkazy</div>
                </div>
                <div class="right floated six wide right aligned column">
                    <a href="${newStandingOrderUri}" class="ui primary button"><i class="plus icon"></i> Vytvořit nový trvalý příkaz</a>
                </div>
            </div>
            <table class="ui very basic selectable very padded stackable table">
                <thead>
                <tr>
                    <th>Název</th>
                    <th>Nastavení</th>
                    <th>Podrobnosti platby</th>
                    <th class="hidden"></th>
                </tr>
                </thead>
                <tbody>
                <c:choose>
                    <c:when test="${empty(standingTransactionList)}">
                        <tr>
                            <td class="center aligned" colspan="4">
                                Nemáte vytvořený žádný trvalý příkaz
                            </td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <fmt:setLocale value = "cs"/>
                        <c:forEach items="${standingTransactionList}" var="item">
                            <tr>
                                <td>
                                        ${item.standingTransactionSettings.name}
                                </td>
                                <td>
                                    <div class="ui header medium">
                                        <div class="content">
                                            <div class="sub header large">
                                                Datum první platby:
                                                    ${item.standingTransactionSettings.startDate.dayOfMonth}.
                                                    ${item.standingTransactionSettings.startDate.monthValue}.
                                                    ${item.standingTransactionSettings.startDate.year}<br>
                                                <c:if test="${!empty(item.standingTransactionSettings.endDate)}">
                                                    Datum ukončení:
                                                    ${item.standingTransactionSettings.endDate.dayOfMonth}.
                                                    ${item.standingTransactionSettings.endDate.monthValue}.
                                                    ${item.standingTransactionSettings.endDate.year}<br>
                                                </c:if>
                                                Frekvence spouštění: ${item.standingTransactionSettings.frequency.labelName}<br>
                                                <c:if test="${!empty(item.standingTransactionSettings.repeats)}">
                                                    Počet opakování: ${item.standingTransactionSettings.repeats}<br>
                                                </c:if>
                                                <c:if test="${item.standingTransactionSettings.active}">
                                                    <div class="ui blue label" style="margin-left: 0;">
                                                        <i class="clock outline icon"></i> Aktivní
                                                    </div>
                                                </c:if>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="ui header medium">
                                        <div class="content">
                                                ${item.targetAccountNumber}/${item.targetBankCode}
                                            <div class="sub header large">
                                                částka: <fmt:formatNumber value = "${item.amount.doubleValue()}" minFractionDigits = "2" maxFractionDigits = "2" /> ${item.account.currency}
                                            </div>
                                                    <div class="sub header">
                                                        <c:if test="${!empty(item.ks)}">KS: ${item.ks}, </c:if>
                                                        <c:if test="${!empty(item.vs)}">VS: ${item.vs}, </c:if>
                                                        <c:if test="${!empty(item.ss)}">SS: ${item.ss}</c:if>
                                                        <c:if test="${!empty(item.message)}">
                                                            <br>${item.message}
                                                        </c:if>
                                                    </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="ui header right aligned tiny">
                                    <a id="detail-link" href="${standingTransactionDetailUri}${item.id}" class="ui compact tiny primary basic button">Detail</a>
                                    <c:if test="${item.standingTransactionSettings.active}">
                                        <a href="${standingTransactionEditUri}${item.id}" class="ui compact tiny primary basic button">Upravit</a>
                                        <a href="${standingTransactionCancelUri}${item.id}" class="ui compact tiny negative basic button" onclick = "return confirm('Opravdu chcete zrušit tento trvalý příkaz?');"><i class="times icon" style="margin-right: 0"></i>Zrušit</a>
                                    </c:if>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
                </tbody>
            </table>

            <tc:paginator/>

            <script>
                $('tbody tr').click( function() {
                    window.location = $(this).find('#detail-link').attr('href');
                }).hover( function() {
                    $(this).toggleClass('hover');
                });
            </script>
        </div>
    </jsp:body>
</t:layout-frontend>