<%--
  Created by IntelliJ IDEA.
  User: honza
  Date: 3. 1. 2019
  Time: 23:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="standingTransactionListUri" value="${s:mvcUrl('TransactionCtl#standing').build()}" scope="page"/>
<c:set var="transactionDetailUri" value="${s:mvcUrl('TransactionCtl#detail').build()}" scope="page"/>
<c:set var="standingTransactionEditUri" value="${s:mvcUrl('TransactionCtl#standingEdit').build()}" scope="page"/>
<c:set var="standingTransactionCancelUri" value="${s:mvcUrl('TransactionCtl#standingCancel').build()}" scope="page"/>

<t:layout-frontend>
    <jsp:attribute name="title">Detail trvalého příkazu</jsp:attribute>

    <jsp:body>

        <div class="ui blue raised very padded segment" style="width: 100%">
            <div class="ui equal width grid segment-header">
                <div class="left floated column">
                    <div class="ui header large"><i class="file alternate outline blue icon"></i> Detail trvalého příkazu</div>
                </div>
                <div class="right floated right aligned column">
                    <a href="${standingTransactionListUri}" class="ui primary button"><i class="angle left icon"></i> Zpět</a>
                    <c:if test="${transactionDetail.standingTransactionSettings.active}">
                        <a href="${standingTransactionEditUri}${transactionDetail.id}" class="ui primary button"><i class="edit icon"></i> Upravit</a>
                        <a href="${standingTransactionCancelUri}${transactionDetail.id}" class="ui negative button" onclick = "return confirm('Opravdu chcete zrušit tento trvalý příkaz?');"><i class="times icon" style="margin-right: 0"></i>Zrušit</a>
                    </c:if>
                </div>
            </div>

            <div class="ui equal width stackable grid">
                <div class="equal width row">
                    <div class="column">
                        <h5 class="ui header">Účet příjemce</h5>
                            ${transactionDetail.targetAccountNumber}/${transactionDetail.targetBankCode}
                    </div>
                    <div class="column">
                        <h5 class="ui header" style="margin-bottom: 0!important;">Částka</h5>
                        <div class="ui tiny header" style="margin-top: 0 !important;">
                            <fmt:setLocale value="cs"/>
                            <fmt:formatNumber value="${transactionDetail.amount.doubleValue()}" minFractionDigits="2"
                                              maxFractionDigits="2"/> CZK
                        </div>
                    </div>
                </div>
                <div class="ui divider"></div>

                <div class="equal width row">
                    <div class="column">
                        <h5 class="ui grey header">Konstantní symbol</h5>
                            ${transactionDetail.ks}
                    </div>
                    <div class="column">
                        <h5 class="ui grey header">Variabilní symbol</h5>
                            ${transactionDetail.vs}
                    </div>
                    <div class="column">
                        <h5 class="ui grey header">Specifický symbol</h5>
                            ${transactionDetail.ss}
                    </div>
                </div>

                <div class="equal width row">
                    <div class="column">
                        <h5 class="ui grey header">Poznámka</h5>
                            ${transactionDetail.message}
                    </div>
                </div>

                <div class="ui divider"></div>

                <div class="equal width row">
                    <div class="column">
                        <h5 class="ui header">Datum první platby</h5>
                            ${transactionDetail.standingTransactionSettings.startDate.dayOfMonth}.
                            ${transactionDetail.standingTransactionSettings.startDate.monthValue}.
                            ${transactionDetail.standingTransactionSettings.startDate.year}
                    </div>
                    <div class="column">
                        <h5 class="ui header">Datum ukončení</h5>
                            ${transactionDetail.standingTransactionSettings.endDate.dayOfMonth}.
                            ${transactionDetail.standingTransactionSettings.endDate.monthValue}.
                            ${transactionDetail.standingTransactionSettings.endDate.year}
                    </div>
                    <div class="column">
                        <h5 class="ui header">Datum další platby</h5>
                            ${transactionDetail.standingTransactionSettings.lastTransaction.dayOfMonth}.
                            ${transactionDetail.standingTransactionSettings.lastTransaction.monthValue}.
                            ${transactionDetail.standingTransactionSettings.lastTransaction.year}
                    </div>
                </div>

                <div class="equal width row">
                    <div class="column">
                        <h5 class="ui header">Frekvence spouštění</h5>
                            ${transactionDetail.standingTransactionSettings.frequency.labelName}
                    </div>
                </div>
                <div class="equal width row">
                    <div class="column">
                        <h5 class="ui header">Počet opakování</h5>
                            ${transactionDetail.standingTransactionSettings.repeats}
                    </div>
                </div>
            </div>
        </div>
        <div class="ui blue raised very padded segment" style="width: 100%">
            <h2 class="ui header"><i class="settings icon blue"></i> Provedené transakce trvalého příkazu</h2>
            <table class="ui very basic table padded">
                <thead>
                <tr>
                    <th class="twelve wide">Datum provedení</th>
                    <th class="four wide right aligned"></th>
                </tr>
                </thead>
                <tbody>
                <c:choose>
                    <c:when test="${empty(processedTransactions)}">
                        <tr>
                            <td class="center aligned" colspan="2">
                                Nenalezeny žádné transakce.
                            </td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach items="${processedTransactions}" var="item">
                            <tr>
                                <td>
                                        ${item.processedTime.dayOfMonth}.${item.processedTime.monthValue}.${item.processedTime.year}&nbsp
                                        ${item.processedTime.hour}:${item.processedTime.minute}
                                </td>
                                <td class="right aligned">
                                    <a id="detail-link" href="${transactionDetailUri}${item.id}">
                                        Detail&nbsp<i class="search blue icon"></i>
                                    </a>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
                </tbody>
            </table>
        </div>
    </jsp:body>
</t:layout-frontend>