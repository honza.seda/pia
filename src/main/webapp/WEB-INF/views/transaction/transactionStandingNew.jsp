<%--
  Created by IntelliJ IDEA.
  User: honza
  Date: 20. 10. 2018
  Time: 18:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@taglib prefix="tc" tagdir="/WEB-INF/tags/components" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="proccessTransactionStandingUri" value="${s:mvcUrl('TransactionCtl#proccessTransactionStanding').build()}" scope="page"/>

<t:layout-frontend>
    <jsp:attribute name="title">Nový trvalý příkaz</jsp:attribute>

    <jsp:body>
        <tc:account/>

        <form method="post" action="${proccessTransactionStandingUri}" style="width: 100%; padding: 0 !important;">

            <div class="ui segments" style="width: 100%; padding: 0;">

                <div class="ui blue raised very padded segment" style="width: 100%">
                    <h2 class="ui header"><i class="plus icon blue"></i> Nový trvalý příkaz</h2>
                    <div class="ui form">
                        <div class="fields">
                            <div class="sixteen wide field required <form:errors path="standingTransactionSettings.name">error</form:errors>">
                                <label for="name">Název trvalého příkazu</label>
                                <form:input path="standingTransactionSettings.name" type="text" id="name" name="name" />
                                <form:errors path="standingTransactionSettings.name" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                        </div>
                        <div class="fields">
                            <div class="eight wide required field <form:errors path="transactionDetail.targetAccountNumber">error</form:errors>">
                                <label for="account_number">Číslo účtu</label>
                                <form:input path="transactionDetail.targetAccountNumber" type="text" id="account_number" name="account_number"
                                            placeholder="Číslo účtu"/>
                                <form:errors path="transactionDetail.targetAccountNumber" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                            <div class="four wide required field <form:errors path="transactionDetail.targetBankCode">error</form:errors>">
                                <label for="bank_code">Kód banky</label>
                                <form:select path="transactionDetail.targetBankCode" id="bank_code" class="ui search dropdown" items="${bankCodes}" itemValue="code" itemLabel="nameForSelect">
                                </form:select>
                                <form:errors path="transactionDetail.targetBankCode" cssClass="ui pointing red basic label" element="div"/>

                            </div>
                            <div class="four wide required field <form:errors path="transactionDetail.amount">error</form:errors>">
                                <label for="amount">Částka</label>
                                <div class="ui right labeled input">
                                    <form:input path="transactionDetail.amount" type="text" id="amount" name="amount"
                                                placeholder="Částka"/>
                                    <div class="ui label">
                                        CZK
                                    </div>
                                </div>
                                <form:errors path="transactionDetail.amount" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                        </div>
                        <h4 class="ui dividing header">Nastavení opakování trvalého příkazu</h4>

                        <div class="fields">
                            <div class="five wide required field date-select <form:errors path="standingTransactionSettings.startDate">error</form:errors>">
                                <label for="start_date">Datum první platby</label>
                                <div class="ui input left icon">
                                    <i class="calendar icon"></i>
                                    <form:input path="standingTransactionSettings.startDate" type="date" id="start_date" name="start_date" min="${minDate}" placeholder="yyyy-mm-dd" />
                                </div>
                                <form:errors path="standingTransactionSettings.startDate" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                            <div class="five wide field date-select <form:errors path="standingTransactionSettings.endDate">error</form:errors>">
                                <label for="end_date">Datum ukončení</label>
                                <div class="ui input left icon">
                                    <i class="calendar icon"></i>
                                    <form:input path="standingTransactionSettings.endDate" type="date" id="end_date" name="end_date" min="${minDate}" placeholder="yyyy-mm-dd" />
                                </div>
                                <form:errors path="standingTransactionSettings.endDate" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                        </div>
                        <div class="fields">
                            <div class="five wide required field <form:errors path="standingTransactionSettings.frequency">error</form:errors>">
                                <label for="frequency">Frekvence plateb</label>
                                <form:select path="standingTransactionSettings.frequency" name="frequency" id="frequency" class="ui dropdown" items="${frequency}" itemLabel="labelName">
                                </form:select>
                                <form:errors path="standingTransactionSettings.frequency" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                            <div class="five wide field <form:errors path="standingTransactionSettings.repeats">error</form:errors>">
                                <label for="repeats">Počet opakování</label>
                                <form:input path="standingTransactionSettings.repeats" type="text" id="repeats" name="repeats" />
                                <form:errors path="standingTransactionSettings.repeats" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                        </div>
                        <h4 class="ui dividing header">Podrobnosti příkazu</h4>

                        <div class="fields">
                            <div class="four wide field <form:errors path="transactionDetail.ks">error</form:errors>">
                                <label for="ks">Konstantní symbol</label>
                                <form:input path="transactionDetail.ks" type="text" id="ks" name="ks" />
                                <form:errors path="transactionDetail.ks" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                            <div class="four wide field <form:errors path="transactionDetail.vs">error</form:errors>">
                                <label for="vs">Variabilní symbol</label>
                                <form:input path="transactionDetail.vs" type="text" id="vs" name="vs" />
                                <form:errors path="transactionDetail.vs" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                            <div class="four wide field <form:errors path="transactionDetail.ss">error</form:errors>">
                                <label for="ss">Specifický symbol</label>
                                <form:input path="transactionDetail.ss" type="text" id="ss" name="ss" />
                                <form:errors path="transactionDetail.ss" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                        </div>
                        <div class="field <form:errors path="transactionDetail.message">error</form:errors>">
                            <label for="message">Poznámka příjemci</label>
                            <form:textarea path="transactionDetail.message" id="message" name="message" rows="2"/>
                            <form:errors path="transactionDetail.message" cssClass="ui pointing red basic label" element="div"/>
                        </div>
                    </div>
                </div>

                <div class="ui clearing right aligned secondary segment">
                    <form:hidden path="transactionDetail.dueDate" />

                    <button class="ui primary button">Vytvořit trvalý příkaz</button>
                </div>
            </div>
        </form>
    </jsp:body>
</t:layout-frontend>
