<%--
  Created by IntelliJ IDEA.
  User: honza
  Date: 20. 10. 2018
  Time: 18:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@taglib prefix="tc" tagdir="/WEB-INF/tags/components" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="proccessTransactionOrderUri" value="${s:mvcUrl('TransactionCtl#proccessTransactionOrder').build()}" scope="page"/>
<c:set var="getTemplateUri" value="${s:mvcUrl('TransactionCtl#getTemplate').build()}" scope="page"/>

<t:layout-frontend>
    <jsp:attribute name="title">Nový příkaz k úhradě</jsp:attribute>

    <jsp:body>

        <tc:account/>
        <form:form method="post" modelAttribute="transactionDetail" action="${proccessTransactionOrderUri}" style="width: 100%; padding: 0 !important;">

            <div class="ui segments" style="width: 100%; padding: 0;">

                <div class="ui blue raised very padded segment" style="width: 100%">
                    <h2 class="ui header"><i class="plus icon blue"></i> Nový příkaz k úhradě</h2>
                    <div class="ui form">
                        <div class="six wide computer sixteen wide tablet field date-select">
                            <label for="template">Použít šablonu</label>
                            <select id="template" class="ui fluid search dropdown">
                                <option value="">Název šablony</option>
                                <c:forEach items="${templates}" var="item">
                                    <option value="${item.id}">${item.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                    <div class="ui divider"></div>
                    <div class="ui form">
                        <div class="fields">
                            <div class="eight wide required field <form:errors path="targetAccountNumber">error</form:errors>">
                                <label for="account_number">Číslo účtu</label>
                                <form:input path="targetAccountNumber" type="text" id="account_number" name="account_number"
                                            placeholder="Číslo účtu"/>
                                <form:errors path="targetAccountNumber" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                            <div class="four wide required field <form:errors path="targetBankCode">error</form:errors>">
                                <label for="bank_code">Kód banky</label>
                                <form:select path="targetBankCode" id="bank_code" class="ui search dropdown" items="${bankCodes}" itemValue="code" itemLabel="nameForSelect">
                                </form:select>
                                <form:errors path="targetBankCode" cssClass="ui pointing red basic label" element="div"/>

                            </div>
                            <div class="four wide required field <form:errors path="amount">error</form:errors>">
                                <label for="amount">Částka</label>
                                <div class="ui right labeled input">
                                    <form:input path="amount" type="text" id="amount" name="amount"
                                                placeholder="Částka"/>
                                    <div class="ui label">
                                        CZK
                                    </div>
                                </div>
                                <form:errors path="amount" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                        </div>

                        <div class="fields">
                            <div class="six wide field date-select <form:errors path="dueDate">error</form:errors>">
                                <label for="due_date">Datum splatnosti</label>
                                <div class="ui input left icon">
                                    <i class="calendar icon"></i>
                                    <form:input path="dueDate" type="date" id="due_date" name="due_date" min="${minDate}" placeholder="yyyy-mm-dd"/>
                                </div>
                                <form:errors path="dueDate" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                            <div class="six wide field date-select <form:errors path="dueTime">error</form:errors>">
                                <label for="due_date">Čas splatnosti</label>
                                <div class="ui input left icon">
                                    <i class="calendar icon"></i>
                                    <form:input path="dueTime" type="text" id="due_time" name="due_time" placeholder="hh:mm"/>
                                </div>
                                <form:errors path="dueTime" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                        </div>
                        <div class="fields">
                            <div class="four wide field <form:errors path="ks">error</form:errors>">
                                <label for="ks">Konstantní symbol</label>
                                <form:input path="ks" type="text" id="ks" name="ks" />
                                <form:errors path="ks" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                            <div class="four wide field <form:errors path="vs">error</form:errors>">
                                <label for="vs">Variabilní symbol</label>
                                <form:input path="vs" type="text" id="vs" name="vs" />
                                <form:errors path="vs" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                            <div class="four wide field <form:errors path="ss">error</form:errors>">
                                <label for="ss">Specifický symbol</label>
                                <form:input path="ss" type="text" id="ss" name="ss" />
                                <form:errors path="ss" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                        </div>
                        <div class="field <form:errors path="message">error</form:errors>">
                            <label for="message">Poznámka příjemci</label>
                            <form:textarea path="message" id="message" name="message" rows="2"/>
                            <form:errors path="message" cssClass="ui pointing red basic label" element="div"/>
                        </div>
                        <div class="field">
                            <div class="g-recaptcha" data-sitekey="${captchaSite}" data-callback="onReCaptchaSuccess" data-expired-callback="onReCaptchaExpired"></div>
                            <c:set var="captchaHasBindError"><form:errors path="captcha"/></c:set>
                            <div id="captchaError" class="ui pointing red basic label" <c:if test="${empty(captchaHasBindError)}">style="display: none"</c:if>>${captchaHasBindError}</div>
                        </div>
                    </div>
                </div>
                <div class="ui clearing right aligned secondary segment">
                    <button class="ui primary button">Zadat příkaz</button>
                </div>
            </div>
        </form:form>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#transactionDetail').submit(function (event) {
                    saveOrder(event);
                });
            });

            $('#template').on('change', function() {
                getTemplate( this.value );
            });

            function saveOrder(event){
                event.preventDefault();
                $("#captchaError").html("").hide();

                if(grecaptcha) {
                    if (grecaptcha.getResponse().length === 0) {
                        $("#captchaError").html("Ověřte že nejste robot.").show();
                        return;
                    }
                }

                document.getElementById("transactionDetail").submit();
            }

            function getTemplate(templateId) {
                $.ajax({
                    type: "POST",
                    url: '${getTemplateUri}',
                    data: JSON.stringify(templateId),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        populateForm($('#transactionDetail'), data);
                    }
                });
            }
        </script>
    </jsp:body>
</t:layout-frontend>