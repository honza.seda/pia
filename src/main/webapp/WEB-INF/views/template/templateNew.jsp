<%--
  Created by IntelliJ IDEA.
  User: honza
  Date: 20. 12. 2018
  Time: 20:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="saveNewTemplateUri" value="${s:mvcUrl('TemplateCtl#saveNewTemplate').build()}" scope="page"/>

<t:layout-frontend>
    <jsp:attribute name="title">Nová šablona</jsp:attribute>

    <jsp:body>
        <form:form method="post" action="${saveNewTemplateUri}" modelAttribute="transactionTemplate" style="width: 100%">

            <div class="ui segments" style="width: 100%; padding: 0;">

                <div class="ui blue raised very padded segment" style="width: 100%">
                    <h2 class="ui header">Nová šablona</h2>
                    <div class="ui form">
                        <div class="field">
                            <div class="fields">
                                <div class="sixteen wide field <form:errors path="name">error</form:errors>">
                                    <label for="account_number">Název šablony*</label>
                                    <form:input path="name" type="text" id="firstname" name="firstname"
                                                placeholder="Název šablony"/>
                                    <form:errors path="name" cssClass="ui pointing red basic label" element="div"/>
                                </div>
                            </div>
                            <div class="fields">
                                <div class="eight wide field <form:errors path="targetAccountNumber">error</form:errors>">
                                    <label for="account_number">Číslo účtu*</label>
                                    <form:input path="targetAccountNumber" type="text" id="account_number" name="account_number"
                                                placeholder="Číslo účtu"/>
                                    <form:errors path="targetAccountNumber" cssClass="ui pointing red basic label" element="div"/>
                                </div>
                                <div class="four wide field <form:errors path="targetBankCode">error</form:errors>">
                                    <label for="bank_code">Kód banky*</label>
                                    <form:select path="targetBankCode" id="bank_code" class="ui search dropdown" items="${bankCodes}" itemValue="code" itemLabel="nameForSelect">
                                    </form:select>
                                    <form:errors path="targetBankCode" cssClass="ui pointing red basic label" element="div"/>

                                </div>
                                <div class="four wide field <form:errors path="amount">error</form:errors>">
                                    <label for="amount">Částka*</label>
                                    <div class="ui right labeled input">
                                        <form:input path="amount" type="text" id="amount" name="amount"
                                                    placeholder="Částka"/>
                                        <div class="ui label">
                                            CZK
                                        </div>
                                    </div>
                                    <form:errors path="amount" cssClass="ui pointing red basic label" element="div"/>
                                </div>
                            </div>
                        </div>
                        <div class="fields">
                            <div class="four wide field <form:errors path="ks">error</form:errors>">
                                <label for="ks">Konstantní symbol</label>
                                <form:input path="ks" type="text" id="ks" name="ks" />
                                <form:errors path="ks" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                            <div class="four wide field <form:errors path="vs">error</form:errors>">
                                <label for="vs">Variabilní symbol</label>
                                <form:input path="vs" type="text" id="vs" name="vs" />
                                <form:errors path="vs" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                            <div class="four wide field <form:errors path="ss">error</form:errors>">
                                <label for="ss">Specifický symbol</label>
                                <form:input path="ss" type="text" id="ss" name="ss" />
                                <form:errors path="ss" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                        </div>
                        <div class="field <form:errors path="message">error</form:errors>">
                            <label for="message">Poznámka příjemci</label>
                            <form:textarea path="message" id="message" name="message" rows="2"/>
                            <form:errors path="message" cssClass="ui pointing red basic label" element="div"/>
                        </div>
                    </div>
                </div>
                <div class="ui clearing right aligned secondary segment">
                    <input type="submit" class="ui primary button" value="Uložit šablonu">
                </div>
            </div>
        </form:form>
    </jsp:body>
</t:layout-frontend>
