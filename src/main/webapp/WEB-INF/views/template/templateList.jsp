<%--
  Created by IntelliJ IDEA.
  User: honza
  Date: 20. 12. 2018
  Time: 19:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@taglib prefix="tc" tagdir="/WEB-INF/tags/components" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:set var="templateNewUri" value="${s:mvcUrl('TemplateCtl#newTemplate').build()}" scope="page"/>
<c:set var="templateEditUri" value="${s:mvcUrl('TemplateCtl#editTemplate').build()}" scope="page"/>
<c:set var="deleteTemplateUri" value="${s:mvcUrl('TemplateCtl#deleteTemplate').build()}" scope="page"/>
<c:set var="useTemplateUri" value="${s:mvcUrl('TemplateCtl#useTemplate').build()}" scope="page"/>

<c:set var="paginatorLinkUri" value="${s:mvcUrl('TemplateCtl#templates').build()}" scope="page"/>

<t:layout-frontend>
    <jsp:attribute name="title">Seznam šablon</jsp:attribute>
    <jsp:body>
        <div class="ui blue raised very padded segment" style="width: 100%">
            <div class="ui grid segment-header">
                <div class="left floated six wide column">
                    <div class="ui header large"><i class="copy outline icon blue"></i> Šablony příkazů</div>
                </div>
                <div class="right floated six wide right aligned column">
                    <a href="${templateNewUri}" class="ui primary button"><i class="plus icon"></i> Nová šablona</a>
                </div>
            </div>
            <table class="ui very basic selectable very padded stackable table">
                <thead>
                <tr>
                    <th>Název šablony</th>
                    <th>Cíl platby</th>
                    <th>Podrobnosti</th>
                    <th class="hidden"></th>
                </tr>
                </thead>
                <tbody>
                <c:choose>
                    <c:when test="${empty(transactionTemplates)}">
                        <tr>
                            <td class="center aligned" colspan="4">
                                Nemáte vytvořenou žádnou šablonu.
                            </td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <fmt:setLocale value = "cs"/>
                        <c:forEach items="${transactionTemplates}" var="item">
                            <tr>
                                <td>
                                        ${item.name}
                                </td>
                                <td>
                                    <div class="ui header medium">
                                        <div class="content">
                                                ${item.targetAccountNumber}/${item.targetBankCode}
                                            <div class="sub header large">
                                                částka: <fmt:formatNumber value = "${item.amount.doubleValue()}" minFractionDigits = "2" maxFractionDigits = "2" /> ${item.account.currency}
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <div class="ui header medium">
                                        <div class="content">
                                            <div class="sub header">
                                                <c:if test="${!empty(item.ks)}">KS: ${item.ks}, </c:if>
                                                <c:if test="${!empty(item.vs)}">VS: ${item.vs}, </c:if>
                                                <c:if test="${!empty(item.ss)}">SS: ${item.ss}</c:if>
                                                <c:if test="${!empty(item.message)}">
                                                    <br>${item.message}
                                                </c:if>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td class="ui header right aligned tiny">
                                    <a href="${useTemplateUri}${item.id}" class="ui compact tiny primary basic button">Použít</a>
                                    <a id="detail-link" href="${templateEditUri}${item.id}" class="ui compact tiny primary basic button">Upravit</a>
                                    <a href="${deleteTemplateUri}${item.id}" class="ui compact tiny negative basic button" onclick = "return confirm('Opravdu chcete šablonu smazat?');"><i class="times icon" style="margin-right: 0"></i></a>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
                </tbody>
            </table>

            <tc:paginator/>

            <script>
                $('tbody tr').click( function() {
                    window.location = $(this).find('#detail-link').attr('href');
                }).hover( function() {
                    $(this).toggleClass('hover');
                });
            </script>
        </div>
    </jsp:body>
</t:layout-frontend>