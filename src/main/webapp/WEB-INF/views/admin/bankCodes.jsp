<%--
  Created by IntelliJ IDEA.
  User: honza
  Date: 26. 12. 2018
  Time: 21:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@taglib prefix="tc" tagdir="/WEB-INF/tags/components" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<c:set var="updateBankCodesUri" value="${s:mvcUrl('AdminCtl#updateBankCodes').build()}" scope="page"/>
<c:set var="paginatorLinkUri" value="${s:mvcUrl('AdminCtl#bankCodes').build()}" scope="page"/>

<t:layout-admin>
    <jsp:attribute name="title">Seznam uživatelských účtů</jsp:attribute>
    <jsp:body>
        <div class="ui blue raised very padded segment" style="width: 100%">
            <div class="ui grid segment-header">
                <div class="left floated six wide column">
                    <div class="ui header large">Kódy bank</div>
                </div>
                <div class="right floated six wide right aligned column">
                    <a href="${updateBankCodesUri}" class="ui primary button"><i class="sync alternate icon"></i> Aktualizovat kódy</a>
                </div>
            </div>
            <table class="ui very basic selectable very padded stackable table">
                <thead>
                <tr>
                    <th>Kód banky</th>
                    <th>Název</th>
                    <th>BIC kód (SWIFT)</th>
                </tr>
                </thead>
                <tbody>
                        <c:forEach items="${codes}" var="item">
                            <tr>
                                <td class="ui header">
                                    ${item.code}
                                </td>
                                <td>
                                    ${item.name}
                                </td>
                                <td>
                                    ${item.swift}
                                </td>
                            </tr>
                        </c:forEach>
                </tbody>
            </table>
            <tc:paginator/>
        </div>
    </jsp:body>
</t:layout-admin>
