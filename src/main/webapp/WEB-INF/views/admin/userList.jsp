<%--
  Created by IntelliJ IDEA.
  User: honza
  Date: 23. 12. 2018
  Time: 13:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@taglib prefix="tc" tagdir="/WEB-INF/tags/components" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<c:set var="userEditUri" value="${s:mvcUrl('AdminCtl#userEdit').build()}" scope="page"/>
<c:set var="userDeleteUri" value="${s:mvcUrl('AdminCtl#userDelete').build()}" scope="page"/>
<c:set var="paginatorLinkUri" value="${s:mvcUrl('AdminCtl#userList').build()}" scope="page"/>

<t:layout-admin>
    <jsp:attribute name="title">Seznam uživatelských účtů</jsp:attribute>
    <jsp:body>
        <div class="ui blue raised very padded segment" style="width: 100%">
            <h2 class="ui header">Seznam uživatelských účtů</h2>
            <table class="ui very basic selectable very padded stackable table">
                <thead>
                <tr>
                    <th>Jméno Příjmení</th>
                    <th>Rodné číslo</th>
                    <th>Email</th>
                    <th class="hidden"></th>
                </tr>
                </thead>
                <tbody>
                <c:choose>
                    <c:when test="${empty(users)}">
                        <tr>
                            <td class="center aligned" colspan="4">
                                Nemáte uloženou žádnou šablonu
                            </td>
                        </tr>
                    </c:when>
                    <c:otherwise>
                        <c:forEach items="${users}" var="item">
                            <tr>
                                <td>
                                    ${item.firstName} ${item.lastName}
                                </td>
                                <td>
                                    ${item.pin}
                                </td>
                                <td>
                                    ${item.email}
                                </td>
                                <td class="ui header right aligned tiny">
                                    <a href="${userEditUri}${item.id}" class="ui compact tiny primary basic button">Upravit</a>
                                    <a href="${userDeleteUri}${item.id}" class="ui compact tiny negative basic button" onclick = "return confirm('Opravdu chcete smazat tento uživatelský účet?');">Smazat</a>
                                </td>
                            </tr>
                        </c:forEach>
                    </c:otherwise>
                </c:choose>
                </tbody>
            </table>
            <tc:paginator/>
        </div>
    </jsp:body>
</t:layout-admin>
