<%--
  Created by IntelliJ IDEA.
  User: honza
  Date: 23. 12. 2018
  Time: 14:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:set var="userEditActionUri" value="${s:mvcUrl('AdminCtl#userEditAction').build()}" scope="page"/>

<t:layout-admin>
    <jsp:attribute name="title">Editace uživatelského účtu</jsp:attribute>

    <jsp:body>

        <form:form method="post" action="${userEditActionUri}" modelAttribute="user" style="width: 100%">
            <div class="ui segments" style="width: 100%; padding: 0;">
                <div class="ui blue raised very padded segment" style="width: 100%">
                    <h2 class="ui header">Editace uživatelského účtu</h2>
                    <div class="ui form">
                        <h4 class="ui dividing header">Osobní údaje</h4>
                        <div class="field">
                            <div class="fields">
                                <div class="eight wide field  <form:errors path="firstName">error</form:errors>">
                                    <label for="firstname">Jméno*</label>
                                    <form:input path="firstName" type="text" id="firstname" name="firstname"
                                                placeholder="Jméno"/>
                                    <form:errors path="firstName" cssClass="ui pointing red basic label" element="div"/>
                                </div>
                                <div class="eight wide field <form:errors path="lastName">error</form:errors>">
                                    <label for="lastname">Příjmení*</label>
                                    <form:input path="lastName" type="text" id="lastname" name="lastname"
                                                placeholder="Příjmení"/>
                                    <form:errors path="lastName" cssClass="ui pointing red basic label" element="div"/>
                                </div>
                            </div>
                        </div>
                        <div class="four wide field <form:errors path="pin">error</form:errors>">
                            <label for="pin">Rodné číslo (bez lomítka)*</label>
                            <form:input path="pin" type="text" id="pin" name="pin" placeholder="Rodné číslo"/>
                            <form:errors path="pin" cssClass="ui pointing red basic label" element="div"/>
                        </div>
                        <h4 class="ui dividing header">Adresa</h4>
                        <div class="fields">
                            <div class="twelve wide field <form:errors path="street">error</form:errors>">
                                <label for="street">Ulice</label>
                                <form:input path="street" id="street" type="text" name="street"/>
                                <form:errors path="street" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                            <div class="four wide field <form:errors path="houseNumber">error</form:errors>">
                                <label for="house_number">Číslo popisné</label>
                                <form:input path="houseNumber" id="house_number" type="text" name="house_number"/>
                                <form:errors path="houseNumber" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                        </div>
                        <div class="fields">
                            <div class="twelve wide field <form:errors path="city">error</form:errors>">
                                <label for="city">Město</label>
                                <form:input path="city" id="city" type="text" name="city"/>
                                <form:errors path="city" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                            <div class="four wide field <form:errors path="zip">error</form:errors>">
                                <label for="zip">PSČ</label>
                                <form:input path="zip" id="zip" type="text" name="zip"/>
                                <form:errors path="zip" cssClass="ui pointing red basic label" element="div"/>
                            </div>
                        </div>
                        <div class="field <form:errors path="country">error</form:errors>">
                            <label for="country">Stát</label>
                            <form:select path="country" id="country" class="ui fluid search dropdown" items="${countries}" itemLabel="labelName" />
                            <form:errors path="country" cssClass="ui pointing red basic label" element="div"/>
                        </div>
                        <h4 class="ui dividing header">Kontaktní údaje</h4>
                        <div class="field <form:errors path="email">error</form:errors>">
                            <label for="email">Email*</label>
                            <form:input path="email" id="email" type="text" name="email"/>
                            <form:errors path="email" cssClass="ui pointing red basic label" element="div"/>
                        </div>
                        <div class="field <form:errors path="phone">error</form:errors>">
                            <label for="phone">Telefon</label>
                            <form:input path="phone" id="phone" type="text" name="phone"/>
                            <form:errors path="phone" cssClass="ui pointing red basic label" element="div"/>
                        </div>
                    </div>
                </div>
                <div class="ui clearing right aligned secondary segment">
                    <form:hidden path="id" id="id" />
                    <input type="submit" value="Uložit upravené údaje" class="ui primary button" />
                </div>
            </div>
        </form:form>
    </jsp:body>
</t:layout-admin>