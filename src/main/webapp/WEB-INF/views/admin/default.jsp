<%--
  Created by IntelliJ IDEA.
  User: honza
  Date: 21. 10. 2018
  Time: 14:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<t:layout-admin>
    <jsp:attribute name="title">Přehled</jsp:attribute>

    <jsp:body>

        <div class="ui blue raised very padded segment" style="width: 100%">
            <h2 class="ui header">Přehled administrace</h2>
            <div class="content">
                <h4 class="ui header">
                    <i class="settings icon"></i>
                    <div class="content">
                        Parametry aplikace
                    </div>
                </h4>
                <div class="ui list">
                    <div class="item">
                        <div class="header">Kód banky</div>
                            ${bankCode}
                    </div>
                    <div class="item">
                        <div class="header">Interval zpracování příkazů</div>
                            ${transactionProcessRate}
                    </div>
                    <div class="item">
                        <div class="header">Interval aktualizace bankovních kódů</div>
                            ${updateBankCodesRate}
                    </div>
                </div>
            </div>
        </div>

    </jsp:body>
</t:layout-admin>