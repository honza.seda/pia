<%@tag description="Account overview template" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@attribute name="account" %>
<%@attribute name="bankCode" %>

<c:if test="${account != null}">
    <div class="ui blue raised very padded segment" style="width: 100%">
        <div class="ui equal width stackable grid">
            <div class="column">
                <h5 class="header">Účet</h5>
                    ${account.accountNumber}/${bankCode}
            </div>
            <div class="column">
                <h5 class="header">Běžný zůstatek</h5>
                <fmt:setLocale value = "cs"/>

                <fmt:formatNumber type = "number" minFractionDigits = "2" maxFractionDigits = "2" value = "${account.balance}" />
                    ${account.currency}
            </div>
            <div class="column">
                <h5 class="header">Použitelný zůstatek</h5>
                <fmt:formatNumber type = "number" minFractionDigits = "2" maxFractionDigits = "2" value = "${account.availableBalance}" />
                    ${account.currency}
            </div>
        </div>
    </div>
</c:if>