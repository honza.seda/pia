<%@tag description="Paginator template" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@attribute name="paginator" %>
<%@attribute name="paginatorLinkUri" %>

<c:if test="${paginator != null}">
    <div class="ui equal width middle aligned stackable grid">
        <div class="center aligned sixteen wide column">

            <c:if test="${paginator.pageCount > 1}">
                <div aria-label="Pagination Navigation" role="navigation"
                     class="ui pagination pointing secondary menu blue centered">
                    <a href="${paginatorLinkUri}?page=${paginator.first().pageNumber}&limit=${paginator.pageSize}"
                       class="item">
                        <i class="angle double left icon"></i>
                    </a>
                    <a href="${paginatorLinkUri}?page=${paginator.previous().pageNumber}&limit=${paginator.pageSize}"
                       class="item">
                        <i class="angle left icon"></i>
                    </a>
                    <c:forEach begin="${paginator.startingPage}" end="${paginator.endingPage}" var="i">
                        <a href="${paginatorLinkUri}?page=${i}&limit=${paginator.pageSize}"
                           class="<c:if test="${paginator.pageNumber eq i}">active </c:if>item">
                                ${i}
                        </a>
                    </c:forEach>
                    <a href="${paginatorLinkUri}?page=${paginator.next().pageNumber}&limit=${paginator.pageSize}"
                       class="item">
                        <i class="angle right icon"></i>
                    </a>
                    <a href="${paginatorLinkUri}?page=${paginator.last().pageNumber}&limit=${paginator.pageSize}"
                       class="item">
                        <i class="angle double right icon"></i>
                    </a>
                </div>
            </c:if>
        </div>

        <div class="right aligned column">
            <div class="ui middle aligned equal width grid">
                <div class="left aligned column">
                        ${paginator.itemCount} výsledků
                </div>
                <div class="column">
                    <div class="content">
                        <div class="sub header">
                            Výsledků na stránku
                        </div>
                    </div>
                    <div class="ui horizontal link list">
                        <c:forEach items="${paginator.allowedLimits}" var="limit">
                            <a href="${historyUri}?page=1&limit=${limit}"
                               class="<c:if test="${limit == paginator.pageSize}">active </c:if>item">${limit}</a>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>
    </div>
</c:if>