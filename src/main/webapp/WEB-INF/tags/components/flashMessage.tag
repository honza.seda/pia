<%@tag description="Account overview template" pageEncoding="UTF-8" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@attribute name="flashMessage" %>

<c:if test="${not empty flashMessage}">
    <div class="ui ${flashMessage.type} icon message">
        <i class="${flashMessage.icon} icon"></i>
        <div class="content">
            <div class="header">
                    ${flashMessage.messageHeader}
            </div>
            <p>
                    ${flashMessage.message}
            </p>
        </div>
    </div>
</c:if>