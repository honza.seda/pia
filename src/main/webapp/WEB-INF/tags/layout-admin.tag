<%@tag description="Admin Page template" pageEncoding="UTF-8" %>
<%@attribute name="title" fragment="true" %>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="tc" tagdir="/WEB-INF/tags/components" %>

<html>
<head>
    <link rel="stylesheet" href="${request.contextPath}/admin/css/style.css"/>
    <link rel="stylesheet" href="${request.contextPath}/semantic/semantic.css"/>
    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>
    <script src="${request.contextPath}/semantic/semantic.js"></script>
    <script src="${request.contextPath}/admin/js/script.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <title>
        GreyBank Administrace |
        <jsp:invoke fragment="title"/>
    </title>
</head>
<body>

<c:set var="currentUri" value="${requestScope['javax.servlet.forward.request_uri']}" scope="page"/>

<c:set var="homepageUri" value="${s:mvcUrl('AdminCtl#defaultPage').build()}" scope="page"/>
<c:set var="loginUri" value="${s:mvcUrl('UserCtl#login').build()}" scope="page"/>
<c:set var="logoutUri" value="${s:mvcUrl('UserCtl#logout').build()}" scope="page"/>
<c:set var="registerNewUri" value="${s:mvcUrl('AdminCtl#registrationPage').build()}" scope="page"/>
<c:set var="userListUri" value="${s:mvcUrl('AdminCtl#userList').build()}" scope="page"/>
<c:set var="bankCodesUri" value="${s:mvcUrl('AdminCtl#bankCodes').build()}" scope="page"/>

<div class="ui inverted segment header-menu">
    <div class="ui inverted secondary menu header-menu">
        <div class="ui container">
            <div class="header item menu-logo">
                <a href="${homepageUri}" class="menu-icon">
                    <i class="small bordered inverted blue gofore icon"></i>
                </a>
                <div class="ui grid">
                    <div class="computer only tablet only column">
                        <div class="content">
                            <a href="${homepageUri}" style="color: white">GreyBank
                                <span class="menu-secondary-text">administrace</span>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
            <div class="right menu">
                <c:choose>
                    <c:when test="${isLoggedIn}">
                        <%--Primary menu for logged in users--%>
                        <div class="ui right aligned">
                            <div class="item menu-name">
                                    ${userName}
                            </div>
                        </div>
                        <div class="ui right aligned">
                            <a class="item" href="${logoutUri}">
                                <i class="sign-out icon"></i>
                                <div class="ui grid">
                                    <div class="computer only tablet only column">
                                        Odhlásit
                                    </div>
                                </div>
                            </a>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <%--Primary menu for non logged users--%>
                        <div class="ui right aligned">
                            <a class="item" href="${loginUri}">
                                <i class="sign-in icon"></i>Přihlásit
                            </a>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</div>

<div class="ui stackable grid container menu-margin">
    <div class="ui four wide column">
        <div class="ui blue raised segment">
            <div class="ui secondary vertical menu admin-menu">
                <a href="${userListUri}" class="${currentUri == userListUri ? ' active' : ''} item">
                    Uživatelské účty
                </a>
                <a href="${registerNewUri}" class="${currentUri == registerNewUri ? ' active' : ''} item">
                    Registrace účtu
                </a>
                <a href="${bankCodesUri}" class="${currentUri == bankCodesUri ? ' active' : ''} item">
                    Kódy bank
                </a>
            </div>
        </div>
    </div>

    <div class="ui twelve wide column">
        <tc:flashMessage/>
        <jsp:doBody/>
    </div>
</div>

</body>
</html>