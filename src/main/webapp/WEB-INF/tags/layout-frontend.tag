<%@tag description="Frontend Page template" pageEncoding="UTF-8" %>
<%@attribute name="title" fragment="true" %>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="tc" tagdir="/WEB-INF/tags/components" %>

<html>
<head>
    <link rel="stylesheet" href="${request.contextPath}/frontend/css/style.css"/>
    <link rel="stylesheet" href="${request.contextPath}/semantic/semantic.css"/>
    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>
    <script src="${request.contextPath}/semantic/semantic.js"></script>
    <script src="${request.contextPath}/frontend/js/script.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <title>
        GreyBank |
        <jsp:invoke fragment="title"/>
    </title>
</head>
<body>

<%-- URI links definition --%>
<c:set var="currentUri" value="${requestScope['javax.servlet.forward.request_uri']}" scope="page"/>
<c:set var="homepageUri" value="${s:mvcUrl('HomeCtl#homeDefault').build()}" scope="page"/>
<c:set var="dashboardUri" value="${s:mvcUrl('DashboardCtl#dashboardDefault').build()}" scope="page"/>
<c:set var="loginUri" value="${s:mvcUrl('UserCtl#login').build()}" scope="page"/>
<c:set var="logoutUri" value="${s:mvcUrl('UserCtl#logout').build()}" scope="page"/>
<c:set var="detailUri" value="${s:mvcUrl('UserCtl#detail').build()}" scope="page"/>

<c:set var="transactionOrderUri" value="${s:mvcUrl('TransactionCtl#order').build()}" scope="page"/>
<c:set var="standingOrderUri" value="${s:mvcUrl('TransactionCtl#standing').build()}" scope="page"/>
<c:set var="templateListUri" value="${s:mvcUrl('TemplateCtl#templates').build()}" scope="page"/>
<c:set var="transactionHistoryUri" value="${s:mvcUrl('TransactionCtl#history').build()}" scope="page"/>

<%--Primary menu bar--%>
<div class="ui inverted segment header-menu">
    <div class="ui inverted secondary menu header-menu">
        <div class="ui container">
            <div class="header item menu-logo">
                <a href="${homepageUri}" class="menu-icon">
                    <i class="small bordered inverted blue gofore icon"></i>
                </a>
                <div class="ui grid">
                    <div class="computer only tablet only column">
                        <div class="content">
                            <a href="${homepageUri}" style="color: white">GreyBank</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="right menu">
                <c:choose>
                    <c:when test="${isLoggedIn}">
                        <%--Primary menu for logged in users--%>
                        <div class="ui right aligned">
                                <a href="${detailUri}" class="item">
                                        ${userName}
                                </a>
                        </div>
                        <div class="ui right aligned">
                            <a class="item" href="${detailUri}">
                                <i class="cog icon"></i>
                            </a>
                        </div>
                        <div class="ui right aligned">
                            <a class="item" href="${logoutUri}">
                                <i class="sign-out icon"></i>
                                <div class="ui grid">
                                    <div class="computer only tablet only column">
                                        Odhlásit
                                    </div>
                                </div>
                            </a>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <%--Primary menu for non logged users--%>
                        <div class="ui right aligned">
                            <a class="item" href="${loginUri}">
                                <i class="sign-in icon"></i><span class="ui ">Přihlásit</span>
                            </a>
                        </div>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</div>

<%--Secondary menu for logged in users--%>
<c:if test="${isLoggedIn}">
    <div class="ui inverted segment">
        <div class="ui container">
            <div class="ui grid">
                <div class="ui tablet computer only column">
                    <div class="ui inverted secondary pointing menu">
                        <a href="${dashboardUri}" class="${currentUri == dashboardUri ? ' active' : ''} item">
                            <i class="home icon"></i>
                        </a>
                        <a href="${transactionOrderUri}" class="${currentUri == transactionOrderUri ? ' active' : ''} item">
                            Příkaz k úhradě
                        </a>
                        <a href="${standingOrderUri}" class="${currentUri == standingOrderUri ? ' active' : ''} item">
                            Trvalé příkazy
                        </a>
                        <a href="${transactionHistoryUri}" class="${currentUri == transactionHistoryUri ? ' active' : ''} item">
                            Historie transakcí
                        </a>
                        <a href="${templateListUri}" class="${currentUri == templateListUri ? ' active' : ''} item">
                            Šablony
                        </a>
                    </div>
                </div>
                <div class="ui mobile only column">
                    <div class="ui icon top left inverted floating dropdown button">
                        <i class="bars icon"></i>
                        <div class="ui massive inverted vertical menu mobile-dropdown">
                            <a href="${dashboardUri}" class="${currentUri == dashboardUri ? ' active' : ''} item">
                                <i class="home icon"></i> Přehled
                            </a>
                            <a href="${transactionOrderUri}" class="${currentUri == transactionOrderUri ? ' active' : ''} item">
                                Příkaz k úhradě
                            </a>
                            <a href="${standingOrderUri}" class="${currentUri == standingOrderUri ? ' active' : ''} item">
                                Trvalé příkazy
                            </a>
                            <a href="${transactionHistoryUri}" class="${currentUri == transactionHistoryUri ? ' active' : ''} item">
                                Historie transakcí
                            </a>
                            <a href="${templateListUri}" class="${currentUri == templateListUri ? ' active' : ''} item">
                                Šablony
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</c:if>

<%--Main content container--%>
<div class="ui grid container menu-margin">
    <tc:flashMessage/>
    <jsp:doBody/>
</div>
</body>
</html>
