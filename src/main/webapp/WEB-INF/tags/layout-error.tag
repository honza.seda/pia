<%@tag description="Frontend Page template" pageEncoding="UTF-8" %>
<%@attribute name="title" fragment="true" %>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="s" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="tc" tagdir="/WEB-INF/tags/components" %>

<html>
<head>
    <link rel="stylesheet" href="${request.contextPath}/frontend/css/style.css"/>
    <link rel="stylesheet" href="${request.contextPath}/semantic/semantic.css"/>
    <script
            src="https://code.jquery.com/jquery-3.1.1.min.js"
            integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
            crossorigin="anonymous"></script>
    <script src="${request.contextPath}/semantic/semantic.js"></script>
    <script src="${request.contextPath}/frontend/js/script.js"></script>

    <title>
        GreyBank |
        <jsp:invoke fragment="title"/>
    </title>
</head>
<body>

<%-- URI links definition --%>
<c:set var="homepageUri" value="${s:mvcUrl('HomeCtl#homeDefault').build()}" scope="page"/>
<c:set var="adminUri" value="${s:mvcUrl('AdminCtl#defaultPage').build()}" scope="page"/>

<%--Primary menu bar--%>
<div class="ui inverted segment header-menu">
    <div class="ui inverted secondary menu header-menu">
        <div class="ui container">
            <div class="header item menu-logo">
                <c:choose>
                    <c:when test="${userRole eq 'ROLE_ADMIN'}">
                        <a href="${adminUri}" class="menu-icon">
                            <i class="small bordered inverted blue gofore icon"></i>
                        </a>
                        <div class="content">
                            <a href="${adminUri}" style="color: white">GreyBank</a>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <a href="${homepageUri}" class="menu-icon">
                            <i class="small bordered inverted blue gofore icon"></i>
                        </a>
                        <div class="content">
                            <a href="${homepageUri}" style="color: white">GreyBank</a>
                        </div>
                    </c:otherwise>
                </c:choose>

            </div>
        </div>
    </div>
</div>

<%--Main content container--%>
<div class="ui grid container menu-margin">
    <div class="ui blue raised very padded segment" style="width: 100%">
        <jsp:doBody/>
        <div class="ui very padded grid">
            <div class="sixteen wide center aligned middle aligned column">
                <c:choose>
                    <c:when test="${userRole eq 'ROLE_ADMIN'}">
                        <a href="${adminUri}">Administrace</a>
                    </c:when>
                    <c:otherwise>
                        <a href="${homepageUri}">Domovská stránka</a>
                    </c:otherwise>
                </c:choose>
            </div>
        </div>
    </div>
</div>
</body>
</html>
