$(document).ready(function () {
    $('select.dropdown')
        .dropdown({
            allowAdditions: true
        })
    ;

    $('.ui.dropdown')
        .dropdown()
    ;
});

var onReCaptchaSuccess = function(response) {
    $("#captchaError").html("").hide();
};

var onReCaptchaExpired = function(response) {
    $("#captchaError").html("Platnost reCaptcha vypršela. Ověřte znovu, že nejste robot.").show();
    grecaptcha.reset();
};

function  populateForm(form, data) {
    $.each(data, function(key, value) {

        if(value !== null && typeof value === 'object' ) {
            this.populateForm(form, value);
        }
        else {
            var ctrl = $('[name='+key+']', form);
            switch(ctrl.prop("type")) {
                case "radio": case "checkbox":
                ctrl.each(function() {
                    $(this).prop("checked",value);
                });
                break;
                case "select-one":
                    ctrl.dropdown('set selected', value);
                    break;
                default:
                    ctrl.val(value);
            }
        }
    }.bind(this));
}