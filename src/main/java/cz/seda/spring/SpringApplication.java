package cz.seda.spring;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableScheduling
@EnableTransactionManagement
public class SpringApplication extends SpringBootServletInitializer
{

    @Override
    protected SpringApplicationBuilder configure (SpringApplicationBuilder builder) {
        return builder.sources(SpringApplication.class);
    }

    public static void main (String[] args) {

        org.springframework.boot.SpringApplication app =
                  new org.springframework.boot.SpringApplication(SpringApplication.class);
        app.run(args);
    }
}