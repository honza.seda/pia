package cz.seda.spring.controller;

import cz.seda.spring.model.Account;
import cz.seda.spring.model.Card;
import cz.seda.spring.model.User;
import cz.seda.spring.repository.CardRepository;
import cz.seda.spring.security.CustomUserPrincipal;
import cz.seda.spring.service.AccountService;
import cz.seda.spring.service.UserService;
import cz.seda.spring.service.interfaces.IAccountService;
import cz.seda.spring.util.enums.ECountry;
import cz.seda.spring.util.enums.EFlashMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import javax.validation.executable.ValidateOnExecution;
import java.util.Set;

/**
 * Controller for user related operations
 */
@Controller
@RequestMapping(path = "/user", name = "UserCtl")
public class UserController extends BaseController {
    private IAccountService accountService;

    @Autowired
    public UserController(UserService userService, AccountService accountService) {
        super(userService);
        this.accountService = accountService;
    }

    /**
     * View template for login page
     * @return user/login ModelAndView
     */
    @GetMapping(path = "/login", name = "login")
    public ModelAndView showlogin()
    {
        return new ModelAndView("user/login");
    }

    /**
     * Empty GET request mapping for logout mapping for Spring Security logoutRequestMatcher
     */
    @GetMapping(path = "/logout", name = "logout")
    public void logout(){
        // Empty request for logout mapping for Spring Security logoutRequestMatcher
    }

    /**
     * View template for user details page
     * @return user/detail ModelAndView
     */
    @GetMapping(path = "/detail", name = "detail")
    public ModelAndView showUserDetail() {
        ModelAndView modelAndView = new ModelAndView("user/detail");
        Account account = accountService.findAccountByUser(getLoggedUser());
        modelAndView.addObject("userDetail", getLoggedUser());
        modelAndView.addObject("userAccount", account);
        modelAndView.addObject("card", accountService.getAccountCard(account));
        return modelAndView;
    }

    /**
     * View template for user edit form
     * @param model model
     * @return user/edit ModelAndView
     */
    @GetMapping(path = "/edit", name = "edit")
    public ModelAndView showUserEdit(Model model) {
        ModelAndView modelAndView = new ModelAndView("user/edit");
        if(!model.containsAttribute("userDetail")) {
            modelAndView.addObject("userDetail", getLoggedUser());
        }
        modelAndView.addObject("countries", ECountry.values());
        return modelAndView;
    }

    /**
     * Handler for saving edited user details from form
     * @param user User entity from form
     * @param result BindingResult of User form values
     * @param ra redirect attributes
     * @return redirect to /user/detail ModelAndView on success, otherwise redirects to /user/edit ModelAndView
     */
    @PostMapping(path = "/submit-edit-details", name = "submitDetails")
    public ModelAndView submitDetails(@Valid @ModelAttribute("userDetail") User user, BindingResult result, RedirectAttributes ra)
    {
        if (result.hasErrors())
        {
            ra.addFlashAttribute("org.springframework.validation.BindingResult.userDetail", result);
            ra.addFlashAttribute("userDetail", user);
            addFlashMessage(ra, EFlashMessage.ERROR, "Při zpracování formuláře došlo k chybě. Zkontrolujte prosím zadané údaje.");
            return new ModelAndView("redirect:/user/edit");
        }
        user.setId(getLoggedUser().getId());
        User editedUser = userService.editUser(user);
        updatePrincipal(new CustomUserPrincipal(editedUser));

        addFlashMessage(ra, EFlashMessage.SUCCESS, "Kontaktní údaje byly upraveny");
        return new ModelAndView("redirect:/user/detail");
    }

    /**
     * GET request for Spring Security defaultSuccessUrl
     * @param redirectAttrs redirect attributes
     * @return redirect page based on logged in user role
     */
    @GetMapping("/handleLoginSuccess")
    public String handleLoginSuccess(RedirectAttributes redirectAttrs)
    {
        addFlashMessage(redirectAttrs, EFlashMessage.SUCCESS, "Nyní jste přihlášeni");
        Set<String> roles = AuthorityUtils.authorityListToSet(SecurityContextHolder.getContext().getAuthentication().getAuthorities());
        if (roles.contains("ROLE_ADMIN")) {
            return "redirect:/admin/";
        }
        else
        {
            return "redirect:/dashboard/";
        }
    }

    @PostMapping(path = "/submit-user-settings", name = "submitUserSettings")
    public ModelAndView submitUserSettings(@ModelAttribute("userAccount") Account accountSettings, @ModelAttribute("card") Card cardSettings, RedirectAttributes ra)
    {
        ModelAndView modelAndView = new ModelAndView("redirect:/user/detail");
        Account userAccount = accountService.findAccountByUser(getLoggedUser());
        accountService.updateUserSettings(userAccount, accountSettings, cardSettings);
        addFlashMessage(ra, EFlashMessage.SUCCESS, "Nastavení účtu bylo změněno.");
        return modelAndView;
    }

    /**
     * Updates current Security principal with new CustomUserPrincipal object
     * @param newPrincipal updated values of principal
     */
    private void updatePrincipal(CustomUserPrincipal newPrincipal)
    {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Authentication authentication = new UsernamePasswordAuthenticationToken(newPrincipal, auth.getCredentials(), auth.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
