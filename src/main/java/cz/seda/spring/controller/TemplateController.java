package cz.seda.spring.controller;

import cz.seda.spring.model.Account;
import cz.seda.spring.model.BankCode;
import cz.seda.spring.model.TransactionDetail;
import cz.seda.spring.model.TransactionTemplate;
import cz.seda.spring.repository.AccountRepository;
import cz.seda.spring.repository.BankCodeRepository;
import cz.seda.spring.repository.TransactionTemplateRepository;
import cz.seda.spring.service.TemplateService;
import cz.seda.spring.service.UserService;
import cz.seda.spring.service.interfaces.ITemplateService;
import cz.seda.spring.util.Paginator;
import cz.seda.spring.util.enums.EFlashMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

/**
 * Controller handling transaction template operations
 */
@Controller
@RequestMapping(path = "/template", name = "TemplateCtl")
public class TemplateController extends BaseController {
    private TransactionTemplateRepository transactionTemplateRepository;
    private AccountRepository accountRepository;
    private BankCodeRepository bankCodeRepository;
    private ITemplateService templateService;

    @Autowired
    public TemplateController(
            UserService userService,
            TransactionTemplateRepository transactionTemplateRepository,
            AccountRepository accountRepository,
            BankCodeRepository bankCodeRepository,
            TemplateService templateService
    ) {
        super(userService);
        this.transactionTemplateRepository = transactionTemplateRepository;
        this.accountRepository = accountRepository;
        this.bankCodeRepository = bankCodeRepository;
        this.templateService = templateService;
    }

    /**
     * View template for list of all transaction templates
     * @param page optional parameter of pagination page
     * @param limit optional parameter of pagination limit
     * @return template/templateList ModelAndView
     */
    @GetMapping(path = "/", name = "templates")
    public ModelAndView showTemplateList(@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer limit) {
        ModelAndView modelAndView = new ModelAndView("template/templateList");
        if(limit == null)
        {
            limit = Paginator.ALLOWED_LIMITS[0];
        }
        if(page == null)
        {
            page = 1;
        }
        Paginator paginator = new Paginator((page-1)*limit, limit, new Sort(Sort.Direction.ASC, "name"));
        Account account = accountRepository.findAccountByUser(getLoggedUser());
        List<TransactionTemplate> transactionTemplates = this.transactionTemplateRepository.findAllByAccount(account, paginator);
        paginator.setPageCount(transactionTemplateRepository.countByAccount(account));
        modelAndView.addObject("transactionTemplates", transactionTemplates);
        modelAndView.addObject("paginator", paginator);
        return modelAndView;
    }

    /**
     * View template for new transaction template form
     * @param model model
     * @return template/templateNew ModelAndView
     */
    @GetMapping(path = "/new", name = "newTemplate")
    public ModelAndView showNewTemplate(Model model) {
        ModelAndView modelAndView = new ModelAndView("template/templateNew");
        if(!model.containsAttribute("transactionTemplate")) {
            modelAndView.addObject("transactionTemplate", new TransactionTemplate());
        }
        List<BankCode> bankCodes = bankCodeRepository.findAll();

        modelAndView.addObject("bankCodes", bankCodes);
        return modelAndView;
    }

    /**
     * View template for editing transaction template form
     * @param templateId edited transaction template id
     * @param ra redirect attributes
     * @return template/templateEdit ModelAndView if template exists and user is owner of template, redirect to /template/ otherwise
     */
    @GetMapping(path = "/edit/{templateId}", name = "editTemplate")
    public ModelAndView showEditTemplate(@PathVariable("templateId") int templateId, RedirectAttributes ra) {
        TransactionTemplate transactionTemplate = this.transactionTemplateRepository.getById(templateId);
        ModelAndView modelAndView;
        if (transactionTemplate == null)
        {
            addFlashMessage(ra, EFlashMessage.ERROR, "Šablona nenalezena.");
            modelAndView = new ModelAndView("redirect:/template/");
        }
        else
        {
            if(transactionTemplate.getAccount() != accountRepository.findAccountByUser(getLoggedUser())){
                addFlashMessage(ra, EFlashMessage.ERROR, "Šablona nenalezena.");
                modelAndView = new ModelAndView("redirect:/template/");
            }
            else {
                List<BankCode> bankCodes = bankCodeRepository.findAll();

                modelAndView = new ModelAndView("template/templateEdit");
                modelAndView.addObject("bankCodes", bankCodes);
                modelAndView.addObject("transactionTemplate", transactionTemplate);
            }
        }
        return modelAndView;
    }

    /**
     * Handler for transaction edit form POST action
     * @param transactionTemplate TransactionTemplate entity from form
     * @param result BindingResult of TransactionTemplate form values
     * @param ra redirect attributes
     * @return redirect to /template/ on success, redirect back to form /template/edit/ on binding result errors
     */
    @PostMapping(path = "/template-edit-action", name = "templateEditAction")
    public String editTemplate(@Valid @ModelAttribute("transactionTemplate") TransactionTemplate transactionTemplate, BindingResult result, RedirectAttributes ra)
    {
        if (result.hasErrors())
        {
            ra.addFlashAttribute("org.springframework.validation.BindingResult.transactionTemplate", result);
            ra.addFlashAttribute("transactionTemplate", transactionTemplate);
            addFlashMessage(ra, EFlashMessage.ERROR, "Při zpracování formuláře došlo k chybě. Zkontrolujte prosím zadané údaje.");
            return "redirect:/template/edit/" + transactionTemplate.getId();
        }

        transactionTemplate.setAccount(accountRepository.findAccountByUser(getLoggedUser()));
        transactionTemplateRepository.save(transactionTemplate);
        addFlashMessage(ra, EFlashMessage.SUCCESS, "Šablona byla upravena");
        return "redirect:/template/";
    }

    /**
     * Get request for deleting transaction template by id
     * @param templateId transaction template id
     * @param ra redirect attributes
     * @return redirect:/template/ ModelAndView
     */
    @GetMapping(path = "/delete/{templateId}", name = "deleteTemplate")
    public ModelAndView deleteTemplate(@PathVariable("templateId") int templateId, RedirectAttributes ra) {
        TransactionTemplate transactionTemplate = transactionTemplateRepository.getById(templateId);
        ModelAndView modelAndView;
        if (transactionTemplate == null)
        {
            addFlashMessage(ra, EFlashMessage.ERROR, "Šablona nenalezena.");
            modelAndView = new ModelAndView("redirect:/template/");
        }
        else
        {
            transactionTemplateRepository.delete(transactionTemplate);
            addFlashMessage(ra, EFlashMessage.SUCCESS, "Šablona byla smazána.");
            modelAndView = new ModelAndView("redirect:/template/");
        }
        return modelAndView;
    }

    /**
     * Handler for new transaction template form POST action
     * @param transactionTemplate TransactionTemplate entity from form
     * @param result Binding result of TransactionTemplate form values
     * @param ra redirect attributes
     * @return redirect to /template/ on success, redirect back to form /template/new/ on binding result errors
     */
    @PostMapping(path = "/save-new-template", name = "saveNewTemplate")
    public String saveNewTemplate(@Valid @ModelAttribute("transactionTemplate") TransactionTemplate transactionTemplate, BindingResult result, RedirectAttributes ra)
    {
        if (result.hasErrors())
        {
            ra.addFlashAttribute("org.springframework.validation.BindingResult.transactionTemplate", result);
            ra.addFlashAttribute("transactionTemplate", transactionTemplate);
            addFlashMessage(ra, EFlashMessage.ERROR, "Při zpracování formuláře došlo k chybě. Zkontrolujte prosím zadané údaje.");
            return "redirect:/template/new";
        }

        Account account = accountRepository.findAccountByUser(getLoggedUser());
        transactionTemplate.setAccount(account);

        transactionTemplateRepository.save(transactionTemplate);
        addFlashMessage(ra, EFlashMessage.SUCCESS, "Nová šablona byla vytvořena");
        return "redirect:/template/";
    }

    /**
     * Get request for redirect to new transaction order with selected template as form default values
     * @param id transaction template id
     * @param ra redirect attributes
     * @return redirect to /transaction/order ModelAndView if template is found and user is owner.
     */
    @GetMapping(path = "/use/{id}", name = "useTemplate")
    public ModelAndView useTemplate(@PathVariable(name = "id") Integer id, RedirectAttributes ra){
        Account account = accountRepository.findAccountByUser(getLoggedUser());
        TransactionDetail detail = templateService.mapTemplateByIdToDetail(id, account);
        if (detail == null) {
            addFlashMessage(ra, EFlashMessage.ERROR, "Šablona nenalezena.");
            return new ModelAndView("redirect:/template/");
        }
        ModelAndView modelAndView = new ModelAndView("redirect:/transaction/order");
        ra.addFlashAttribute("transactionDetail", detail);
        return modelAndView;
    }
}
