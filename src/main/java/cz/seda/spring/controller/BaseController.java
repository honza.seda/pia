package cz.seda.spring.controller;

import cz.seda.spring.model.User;
import cz.seda.spring.service.UserService;
import cz.seda.spring.security.CustomUserPrincipal;
import cz.seda.spring.service.interfaces.IUserService;
import cz.seda.spring.util.FlashMessage;
import cz.seda.spring.util.enums.EFlashMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 * Abstract base controller sharing common attributes across controllers
 */
@Controller
abstract public class BaseController {
    protected IUserService userService;

    @Value("${pia.settings.bankCode}")
    private String bankCode;

    @Autowired
    public BaseController(UserService userService) {
        this.userService = userService;
    }

    /**
     * Retrieves logged in user principal
     * @return SecurityContext UserPrincipal
     */
    protected User getLoggedUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth instanceof AnonymousAuthenticationToken) {
            return null;
        }
        CustomUserPrincipal userPrincipal = (CustomUserPrincipal) auth.getPrincipal();
        return userPrincipal.getUser();
    }

    /**
     * Adds common model attribute for all extending controllers.
     * @param model model
     */
    @ModelAttribute
    public void addCommon(Model model){
        User user = getLoggedUser();
        if(user != null) {
            model.addAttribute("isLoggedIn", true);
            model.addAttribute("userName", user.getFirstName() + " " + user.getLastName());
        }
        else
        {
            model.addAttribute("isLoggedIn", false);
        }
        model.addAttribute("bankCode", bankCode);
    }

    /**
     * Common method for adding flash message to redirect attributes
     * @param ra redirect attributes
     * @param type type of flash message
     * @param message flash message text
     */
    void addFlashMessage(RedirectAttributes ra, EFlashMessage type, String message)
    {
        ra.addFlashAttribute("flashMessage", new FlashMessage(type, message));
    }
}
