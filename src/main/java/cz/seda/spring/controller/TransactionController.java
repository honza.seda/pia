package cz.seda.spring.controller;

import cz.seda.spring.exception.CaptchaException;
import cz.seda.spring.exception.TransactionException;
import cz.seda.spring.model.*;
import cz.seda.spring.repository.AccountRepository;
import cz.seda.spring.repository.BankCodeRepository;
import cz.seda.spring.repository.StandingTransactionSettingsRepository;
import cz.seda.spring.repository.TransactionTemplateRepository;
import cz.seda.spring.service.interfaces.ICaptchaService;
import cz.seda.spring.service.TransactionService;
import cz.seda.spring.service.UserService;
import cz.seda.spring.service.interfaces.ITransactionService;
import cz.seda.spring.util.Paginator;
import cz.seda.spring.util.enums.EFlashMessage;
import cz.seda.spring.util.enums.EStandingOrderFrequency;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * Controller handling transaction orders and standing transactions
 */
@Controller
@RequestMapping(path = "/transaction", name = "TransactionCtl")
public class TransactionController extends BaseController {
    private TransactionTemplateRepository transactionTemplateRepository;
    private AccountRepository accountRepository;
    private BankCodeRepository bankCodeRepository;
    private ICaptchaService captchaService;
    private ITransactionService transactionService;
    private StandingTransactionSettingsRepository standingTransactionSettingsRepository;

    @Autowired
    public TransactionController(
            UserService userService,
            TransactionTemplateRepository transactionTemplateRepository,
            AccountRepository accountRepository,
            BankCodeRepository bankCodeRepository,
            ICaptchaService captchaService,
            TransactionService transactionService,
            StandingTransactionSettingsRepository standingTransactionSettingsRepository
    ) {
        super(userService);
        this.transactionTemplateRepository = transactionTemplateRepository;
        this.accountRepository = accountRepository;
        this.bankCodeRepository = bankCodeRepository;
        this.captchaService = captchaService;
        this.transactionService = transactionService;
        this.standingTransactionSettingsRepository = standingTransactionSettingsRepository;
    }

    /**
     * Adds account information as common attributes for all mappings.
     * @param model model
     */
    @Override
    public void addCommon(Model model) {
        super.addCommon(model);
        model.addAttribute("account", accountRepository.findAccountByUser(getLoggedUser()));
    }

    /**
     * View template for new transaction order form.
     * @param model model
     * @return transaction/transactionOrder ModelAndView
     */
    @GetMapping(path = "/order", name = "order")
    public ModelAndView showTransactionOrder(Model model){
        ModelAndView modelAndView = new ModelAndView("transaction/transactionOrder");
        Account account = accountRepository.findAccountByUser(getLoggedUser());
        List<TransactionTemplate> templates = transactionTemplateRepository.findAllByAccount(account);
        List<BankCode> bankCodes = bankCodeRepository.findAll();

        modelAndView.addObject("minDate", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        modelAndView.addObject("captchaSite", captchaService.getReCaptchaSite());
        modelAndView.addObject("bankCodes", bankCodes);
        modelAndView.addObject("templates", templates);
        modelAndView.addObject("account", account);
        if(!model.containsAttribute("transactionDetail")) {
            TransactionDetail transactionDetail = new TransactionDetail();
            transactionDetail.setDueDate(LocalDate.now());
            modelAndView.addObject("transactionDetail", transactionDetail);
        }
        return modelAndView;
    }

    /**
     * View template for new standing transaction form.
     * @param model model
     * @return transaction/transactionStandingNew ModelAndView
     */
    @GetMapping(path = "/standing/new", name = "standingNew")
    public ModelAndView showNewStandingOrder(Model model){
        ModelAndView modelAndView = new ModelAndView("transaction/transactionStandingNew");
        Account account = accountRepository.findAccountByUser(getLoggedUser());
        List<BankCode> bankCodes = bankCodeRepository.findAll();

        modelAndView.addObject("account", account);
        modelAndView.addObject("minDate", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        modelAndView.addObject("bankCodes", bankCodes);
        modelAndView.addObject("frequency", EStandingOrderFrequency.values());

        LocalDate now = LocalDate.now();
        if(!model.containsAttribute("transactionDetail")){
            TransactionDetail transactionDetail = new TransactionDetail();
            transactionDetail.setDueDate(now);
            modelAndView.addObject("transactionDetail", transactionDetail);
        }
        if(!model.containsAttribute("standingTransactionSettings")){
            StandingTransactionSettings settings = new StandingTransactionSettings();
            settings.setStartDate(now);
            modelAndView.addObject("standingTransactionSettings", settings);
        }
        return modelAndView;
    }

    /**
     * View template for standing transaction edit form.
     * @param id id of standing transaction
     * @param model model
     * @param ra redirect attributes
     * @return transaction/transactionStandingEdit ModelAndView
     */
    @GetMapping(path = "/standing/edit/{id}", name = "standingEdit")
    public ModelAndView showEditStandingTransaction(@PathVariable(name = "id") Integer id, Model model, RedirectAttributes ra){
        Account account = accountRepository.findAccountByUser(getLoggedUser());
        TransactionDetail transactionDetail = transactionService.getTransactionDetail(account, id);
        if(transactionDetail == null)
        {
            ModelAndView modelAndView = new ModelAndView("redirect:/transaction/standing");
            addFlashMessage(ra, EFlashMessage.ERROR, "Trvalý příkaz nenalezen");
            return modelAndView;
        }
        StandingTransactionSettings settings = transactionDetail.getStandingTransactionSettings();
        if(settings == null)
        {
            ModelAndView modelAndView = new ModelAndView("redirect:/transaction/standing");
            addFlashMessage(ra, EFlashMessage.ERROR, "Trvalý příkaz nenalezen");
            return modelAndView;
        }
        ModelAndView modelAndView = new ModelAndView("transaction/transactionStandingEdit");
        List<BankCode> bankCodes = bankCodeRepository.findAll();
        modelAndView.addObject("account", account);
        modelAndView.addObject("minDate", LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        LocalDate minStartDate = LocalDate.now();
        if(settings.getPaidCount() > 1)
        {
            minStartDate = settings.getStartDate();
        }
        modelAndView.addObject("minStartDate", minStartDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));

        modelAndView.addObject("bankCodes", bankCodes);
        modelAndView.addObject("frequency", EStandingOrderFrequency.values());
        if(!model.containsAttribute("transactionDetail")){
            modelAndView.addObject("transactionDetail", transactionDetail);
        }
        if(!model.containsAttribute("standingTransactionSettings")){
            modelAndView.addObject("standingTransactionSettings", settings);
        }
        return modelAndView;
    }

    /**
     * Handler for standing transaction edit form POST action.
     * @param transactionDetail TransactionDetail entity from form
     * @param resultDetail BindingResult of TransactionDetail form values
     * @param standingTransactionSettings StandingTransactionSettings entity from form
     * @param resultStandingSettings BindingResult of StandingTransactionSettings form values
     * @param ra redirect attributes
     * @param request servlet request
     * @return redirect to /transaction/standing on success, redirect back to form /transaction/standing/edit/{id} on binding result errors
     */
    @PostMapping(path = "/proccess-edit-transaction-standing", name = "proccessEditTransactionStanding")
    public String proccessEditTransactionStanding(
            @Valid @ModelAttribute("transactionDetail") TransactionDetail transactionDetail,
            BindingResult resultDetail,
            @Valid @ModelAttribute("standingTransactionSettings") StandingTransactionSettings standingTransactionSettings,
            BindingResult resultStandingSettings,
            RedirectAttributes ra,
            HttpServletRequest request
    ) {
        if(standingTransactionSettings.getRepeats() == null && standingTransactionSettings.getEndDate() == null){
            resultStandingSettings.rejectValue("repeats", "error.standingTransactionSettings", "Musí být vyplněný buď datum ukončení nebo počet opakování");
        }
        if(resultDetail.hasErrors() || resultStandingSettings.hasErrors()) {
            ra.addFlashAttribute("org.springframework.validation.BindingResult.transactionDetail", resultDetail);
            ra.addFlashAttribute("transactionDetail", transactionDetail);
            ra.addFlashAttribute("org.springframework.validation.BindingResult.standingTransactionSettings", resultStandingSettings);
            ra.addFlashAttribute("standingTransactionSettings", standingTransactionSettings);
            addFlashMessage(ra, EFlashMessage.ERROR, "Při zpracování formuláře došlo k chybě. Zkontrolujte prosím zadané údaje.");
            return "redirect:/transaction/standing/edit/"+transactionDetail.getId().toString();
        }

        try{
            Account account = accountRepository.findAccountByUser(getLoggedUser());
            transactionService.editStandingTransaction(account, transactionDetail, standingTransactionSettings);
            addFlashMessage(ra, EFlashMessage.SUCCESS, "Trvalý příkaz byl upraven");
            return "redirect:/transaction/standing";
        }
        catch (TransactionException e)
        {
            ra.addFlashAttribute("org.springframework.validation.BindingResult.transactionDetail", resultDetail);
            ra.addFlashAttribute("transactionDetail", transactionDetail);
            ra.addFlashAttribute("org.springframework.validation.BindingResult.standingTransactionSettings", resultStandingSettings);
            ra.addFlashAttribute("standingTransactionSettings", standingTransactionSettings);
            addFlashMessage(ra, EFlashMessage.ERROR, e.getMessage());
            return "redirect:/transaction/standing/edit/"+transactionDetail.getId().toString();
        }
    }

    /**
     * View template for list of standing transactions.
     * @param page optional parameter of pagination page
     * @param limit optional parameter of pagination limit
     * @param model model
     * @return transaction/transactionStandingList ModelAndView
     */
    @GetMapping(path = "/standing", name = "standing")
    public ModelAndView showStandingOrder(@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer limit, Model model){
        ModelAndView modelAndView = new ModelAndView("transaction/transactionStandingList");
        Account account = accountRepository.findAccountByUser(getLoggedUser());
        if(limit == null)
        {
            limit = Paginator.ALLOWED_LIMITS[0];
        }
        if(page == null)
        {
            page = 1;
        }
        Sort sort = Sort.by(
                Sort.Order.desc("standingTransactionSettings.active"),
                Sort.Order.asc("standingTransactionSettings.name"));
        Paginator paginator = new Paginator((page-1)*limit, limit, sort);
        modelAndView.addObject("standingTransactionList", transactionService.getStandingOrders(account, paginator));
        paginator.setPageCount(transactionService.getStandingOrdersCount(account));
        modelAndView.addObject("standingTransactionList", transactionService.getStandingOrders(account, paginator));
        modelAndView.addObject("paginator", paginator);
        return modelAndView;
    }

    /**
     * Handler for saving new transaction order from form POST action. Validates and processes captcha response.
     * @param transactionDetail TransactionDetail entity from form
     * @param result Binding result of TransactionDetail form values
     * @param ra redirect attributes
     * @param request servlet request
     * @return redirect to /dashboard/ on success, redirect back to form /transaction/order on binding result errors
     */
    @PostMapping(path = "/proccess-transaction-order", name = "proccessTransactionOrder")
    public String proccessTransactionOrder(@Valid @ModelAttribute("transactionDetail") TransactionDetail transactionDetail, BindingResult result, RedirectAttributes ra, HttpServletRequest request)
    {
        String response = request.getParameter("g-recaptcha-response");
        try
        {
            captchaService.processResponse(response);
        }
        catch (CaptchaException e)
        {
            result.rejectValue("captcha", "error.transactionDetail", e.getMessage());
        }

        if (result.hasErrors())
        {
            ra.addFlashAttribute("org.springframework.validation.BindingResult.transactionDetail", result);
            ra.addFlashAttribute("transactionDetail", transactionDetail);
            addFlashMessage(ra, EFlashMessage.ERROR, "Při zpracování formuláře došlo k chybě. Zkontrolujte prosím zadané údaje.");
            return "redirect:/transaction/order";
        }

        try{
            Account account = accountRepository.findAccountByUser(getLoggedUser());
            transactionService.saveNewOrder(account, transactionDetail);
            addFlashMessage(ra, EFlashMessage.SUCCESS, "Příkaz k úhradě byl úspěšně zadán");
            return "redirect:/dashboard/";
        }
        catch (TransactionException e)
        {
            ra.addFlashAttribute("org.springframework.validation.BindingResult.transactionDetail", result);
            ra.addFlashAttribute("transaction", transactionDetail);
            addFlashMessage(ra, EFlashMessage.ERROR, e.getMessage());
            return "redirect:/transaction/order";
        }

    }

    /**
     * AJAX POST handler for retrieving TransactionTemplate entity by id in request body
     * @param templateId template id
     * @return TransactionTemplate JSON as ajax response body
     */
    @PostMapping(path = "/get-transaction-template", name = "getTemplate", consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody TransactionTemplate getTemplateForTransaction(@RequestBody Integer templateId)
    {
        return transactionTemplateRepository.getById(templateId);
    }

    /**
     * View template for transaction history list
     * @param page optional parameter of pagination page
     * @param limit optional parameter of pagination limit
     * @return transaction/transactionHistory ModelAndView
     */
    @GetMapping(path = "/history", name = "history")
    public ModelAndView showTransactionHistory(@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer limit){
        ModelAndView modelAndView = new ModelAndView("transaction/transactionHistory");
        Account account = accountRepository.findAccountByUser(getLoggedUser());
        if(limit == null)
        {
            limit = Paginator.ALLOWED_LIMITS[0];
        }
        if(page == null)
        {
            page = 1;
        }
        Paginator paginator = new Paginator((page-1)*limit, limit, new Sort(Sort.Direction.DESC, "processedTime"));
        List<TransactionQueue> transactions = transactionService.getTransactionHistory(account, paginator);
        paginator.setPageCount(transactionService.getTransactionHistoryCount(account));
        modelAndView.addObject("transactions", transactions);
        modelAndView.addObject("account", account);
        modelAndView.addObject("paginator", paginator);
        return modelAndView;
    }

    /**
     * View template for transaction order detail
     * @param id id of transaction
     * @param ra redirect attributes
     * @return transaction/transactionDetail ModelAndView if found, redirect to /transaction/history otherwise
     */
    @GetMapping(path = "/detail/{id}", name = "detail")
    public ModelAndView showTransactionOrder(@PathVariable(name = "id") Integer id, RedirectAttributes ra){
        Account account = accountRepository.findAccountByUser(getLoggedUser());
        TransactionQueue transactionQueue = transactionService.getTransactionQueueRecord(id, account);
        if(transactionQueue == null)
        {
            ModelAndView modelAndView = new ModelAndView("redirect:/transaction/history");
            addFlashMessage(ra, EFlashMessage.ERROR, "Transakce nenalezena");
            return modelAndView;
        }
        ModelAndView modelAndView = new ModelAndView("transaction/transactionDetail");
        modelAndView.addObject("transaction", transactionQueue);
        modelAndView.addObject("account", account);
        return modelAndView;
    }

    /**
     * Handler for saving new standing transaction from form POST action.
     * @param transactionDetail TransactionDetail entity from form
     * @param resultDetail BindingResult of TransactionDetail form values
     * @param standingTransactionSettings StandingTransactionSettings entity from form
     * @param resultStandingSettings BindingResult of StandingTransactionSettings form values
     * @param ra redirect attributes
     * @param request servlet request
     * @return redirect to /transaction/standing on success, redirect back to form /transaction/standing/new on binding result errors
     */
    @PostMapping(path = "/proccess-transaction-standing", name = "proccessTransactionStanding")
    public String proccessTransactionStanding(
            @Valid @ModelAttribute("transactionDetail") TransactionDetail transactionDetail,
            BindingResult resultDetail,
            @Valid @ModelAttribute("standingTransactionSettings") StandingTransactionSettings standingTransactionSettings,
            BindingResult resultStandingSettings,
            RedirectAttributes ra,
            HttpServletRequest request
    ) {
        if(standingTransactionSettings.getRepeats() == null && standingTransactionSettings.getEndDate() == null){
            resultStandingSettings.rejectValue("repeats", "error.standingTransactionSettings", "Musí být vyplněný buď datum ukončení nebo počet opakování");
        }
        if(resultDetail.hasErrors() || resultStandingSettings.hasErrors()) {
            ra.addFlashAttribute("org.springframework.validation.BindingResult.transactionDetail", resultDetail);
            ra.addFlashAttribute("transactionDetail", transactionDetail);
            ra.addFlashAttribute("org.springframework.validation.BindingResult.standingTransactionSettings", resultStandingSettings);
            ra.addFlashAttribute("standingTransactionSettings", standingTransactionSettings);
            addFlashMessage(ra, EFlashMessage.ERROR, "Při zpracování formuláře došlo k chybě. Zkontrolujte prosím zadané údaje.");
            return "redirect:/transaction/standing/new";
        }

        try{
            Account account = accountRepository.findAccountByUser(getLoggedUser());
            transactionService.saveNewStandingTransaction(account, transactionDetail, standingTransactionSettings);
            addFlashMessage(ra, EFlashMessage.SUCCESS, "Nový trvalý příkaz byl vytvořen");
            return "redirect:/transaction/standing";
        }
        catch (TransactionException e)
        {
            ra.addFlashAttribute("org.springframework.validation.BindingResult.transactionDetail", resultDetail);
            ra.addFlashAttribute("transactionDetail", transactionDetail);
            ra.addFlashAttribute("org.springframework.validation.BindingResult.standingTransactionSettings", resultStandingSettings);
            ra.addFlashAttribute("standingTransactionSettings", standingTransactionSettings);
            addFlashMessage(ra, EFlashMessage.ERROR, e.getMessage());
            return "redirect:/transaction/standing/new";
        }
    }

    /**
     * View template for standing transaction detail
     * @param id id of transaction
     * @param ra redirect attributes
     * @return transaction/transactionStandingDetail ModelAndView if found, redirect to /transaction/standing otherwise
     */
    @GetMapping(path = "/standing/detail/{id}", name = "standingDetail")
    public ModelAndView showStandingTransactionDetail(@PathVariable(name = "id") Integer id, RedirectAttributes ra){
        Account account = accountRepository.findAccountByUser(getLoggedUser());
        TransactionDetail transactionDetail = transactionService.getTransactionDetail(account, id);
        if(transactionDetail == null)
        {
            ModelAndView modelAndView = new ModelAndView("redirect:/transaction/standing");
            addFlashMessage(ra, EFlashMessage.ERROR, "Trvalý příkaz nenalezen");
            return modelAndView;
        }
        List<TransactionQueue> queueList = transactionService.getAllStandingTransactionProcessedPayments(transactionDetail);
        ModelAndView modelAndView = new ModelAndView("transaction/transactionStandingDetail");
        modelAndView.addObject("transactionDetail", transactionDetail);
        modelAndView.addObject("processedTransactions", queueList);
        modelAndView.addObject("account", account);
        return modelAndView;
    }

    /**
     * GET request for canceling a standing transaction by id
     * @param id id of transaction
     * @param ra redirect attributes
     * @return redirect to /transaction/standing ModelAndView
     */
    @GetMapping(path = "/standing/cancel/{id}", name = "standingCancel")
    public ModelAndView cancelStandingTransaction(@PathVariable(name = "id") Integer id, RedirectAttributes ra){
        Account account = accountRepository.findAccountByUser(getLoggedUser());
        TransactionDetail transactionDetail = transactionService.cancelStandingTransaction(account, id);
        ModelAndView modelAndView = new ModelAndView("redirect:/transaction/standing");
        if(transactionDetail == null)
        {
            addFlashMessage(ra, EFlashMessage.ERROR, "Trvalý příkaz nenalezen");
            return modelAndView;
        }
        addFlashMessage(ra, EFlashMessage.SUCCESS, "Trvalý příkaz byl zrušen");
        return modelAndView;
    }
}
