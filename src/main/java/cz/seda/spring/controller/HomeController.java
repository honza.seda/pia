package cz.seda.spring.controller;

import cz.seda.spring.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Landing page controller
 */
@Controller
@RequestMapping(path = "/", name = "HomeCtl")
public class HomeController extends BaseController
{
    @Autowired
    public HomeController(UserService userService) {
        super(userService);
    }

    /**
     * View template for landing page
     * @return home/default ModelAndView
     */
    @RequestMapping(path = "/", name = "homeDefault")
    public ModelAndView showDefault()
    {
        return new ModelAndView("home/default");
    }
}