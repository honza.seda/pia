package cz.seda.spring.controller;

import cz.seda.spring.security.CustomUserPrincipal;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;

/**
 * Controller for custom error request handling
 */
@Controller
public class CustomErrorController implements ErrorController {

    /**
     * Request method for determining which error page to show based on http status code
     * @param request servlet request
     * @return ModelAndView based on request status code
     */
    @RequestMapping(value = "/error", method = RequestMethod.GET)
    public ModelAndView handleError(HttpServletRequest request) {
        Object status = request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);

        String errorView = "error/error";
        if (status != null) {
            Integer statusCode = Integer.valueOf(status.toString());

            if(statusCode == HttpStatus.NOT_FOUND.value()) {
                errorView = "error/notFound";
            }
            else if(statusCode == HttpStatus.FORBIDDEN.value()){
                errorView = "error/accessDenied";
            }
            else if(statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) {
                errorView = "error/serverError";
            }
            else if(statusCode == HttpStatus.BAD_REQUEST.value()){
                errorView = "error/badRequest";
            }
        }
        ModelAndView modelAndView = new ModelAndView(errorView);

        modelAndView.addObject("userRole", getLoggedUserRole());
        return modelAndView;
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }

    /**
     * Retrieves logged in users role
     * @return string role
     */
    private String getLoggedUserRole() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth instanceof AnonymousAuthenticationToken) {
            return null;
        }
        CustomUserPrincipal userPrincipal = (CustomUserPrincipal) auth.getPrincipal();
        return userPrincipal.getUser().getRole();
    }
}
