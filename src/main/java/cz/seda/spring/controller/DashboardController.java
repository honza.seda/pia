package cz.seda.spring.controller;

import cz.seda.spring.model.Account;
import cz.seda.spring.repository.AccountRepository;
import cz.seda.spring.service.TransactionService;
import cz.seda.spring.service.UserService;
import cz.seda.spring.service.interfaces.ITransactionService;
import cz.seda.spring.util.Paginator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller for user overview
 */
@Controller
@RequestMapping(path = "/dashboard", name = "DashboardCtl")
public class DashboardController extends BaseController {
    private AccountRepository accountRepository;
    private ITransactionService transactionService;

    @Autowired
    public DashboardController(UserService userService, AccountRepository accountRepository, TransactionService transactionService) {
        super(userService);
        this.accountRepository = accountRepository;
        this.transactionService = transactionService;
    }

    /**
     * View template for user dashboard with account and transactions overview
     * @return dashboard/default ModelAndView
     */
    @GetMapping(path = "/", name = "dashboardDefault")
    public ModelAndView showDefault()
    {
        ModelAndView dashboard = new ModelAndView("dashboard/default");
        Account account = accountRepository.findAccountByUser(getLoggedUser());
        Pageable paginator = new Paginator(0, Paginator.OVERWIEV_TRANSACTION_COUNT, new Sort(Sort.Direction.DESC,"processedTime"));
        dashboard.addObject("recentTransactions", transactionService.getTransactionHistory(account, paginator));
        dashboard.addObject("waitingTransactions",
                transactionService.getWaitingOrders(
                    account,
                    new Paginator(
                            0,
                            Paginator.OVERWIEV_TRANSACTION_COUNT,
                            new Sort(Sort.Direction.ASC, "transactionDetail.dueDate")
                    )
                )
        );
        dashboard.addObject("account", account);
        return dashboard;
    }
}
