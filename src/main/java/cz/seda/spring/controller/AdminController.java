package cz.seda.spring.controller;

import cz.seda.spring.exception.CaptchaException;
import cz.seda.spring.model.BankCode;
import cz.seda.spring.service.*;
import cz.seda.spring.model.User;
import cz.seda.spring.service.interfaces.IBankCodeService;
import cz.seda.spring.service.interfaces.ICaptchaService;
import cz.seda.spring.service.interfaces.IMailService;
import cz.seda.spring.util.Paginator;
import cz.seda.spring.util.TimeFormatter;
import cz.seda.spring.util.enums.ECountry;
import cz.seda.spring.util.enums.EFlashMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * Controller for admin part of application
 */
@Controller
@RequestMapping(path = "/admin", name = "AdminCtl")
public class AdminController extends BaseController {
    private ICaptchaService captchaService;
    private IBankCodeService bankCodeService;
    private IMailService mailService;

    @Value("${pia.settings.transactionProcessRate}")
    private Long transactionProcessRate;
    @Value("${pia.settings.updateBankCodesRate}")
    private Long updateBankCodesRate;
    @Value("${pia.settings.bankCode}")
    private String bankCode;

    @Autowired
    public AdminController(
            UserService userService,
            AccountService accountService,
            ICaptchaService captchaService,
            BankCodeService bankCodeService,
            MailService mailService) {
        super(userService);
        this.captchaService = captchaService;
        this.bankCodeService = bankCodeService;
        this.mailService = mailService;
    }

    /**
     * Default admin view template.
     * @return admin/default ModelAndView
     */
    @GetMapping(path = "/", name = "defaultPage")
    public ModelAndView showDefault(){
        ModelAndView modelAndView = new ModelAndView("admin/default");
        modelAndView.addObject("bankCode", bankCode);
        modelAndView.addObject("transactionProcessRate", TimeFormatter.msToString(transactionProcessRate));
        modelAndView.addObject("updateBankCodesRate", TimeFormatter.msToString(updateBankCodesRate));
        return modelAndView;
    }

    /**
     * View template for user registration form.
     * @param model model
     * @return admin/registration ModelAndView
     */
    @GetMapping(path = "/new-registration", name = "registrationPage")
    public ModelAndView showRegistration(Model model){
        ModelAndView registration = new ModelAndView("admin/registration");

        registration.addObject("captchaSite", captchaService.getReCaptchaSite());
        if(!model.containsAttribute("user")) {
            User user = new User();
            user.setRole("ROLE_USER");
            registration.addObject("user", user);
        }
        registration.addObject("countries", ECountry.values());

        return registration;
    }

    /**
     * Handler for new user form action. Processes and validates captcha response.
     * @param user User entity from form
     * @param result BindingResult of User form values
     * @param ra redirect attributes
     * @param request servlet request
     * @return redirects to list of users on success, on error redirects back to form with error values.
     */
    @PostMapping(path = "/new-registration", name = "registerUser")
    public String registerUser(@Valid @ModelAttribute("user") User user, BindingResult result, RedirectAttributes ra, HttpServletRequest request)
    {
        String response = request.getParameter("g-recaptcha-response");
        try
        {
            captchaService.processResponse(response);
        }
        catch (CaptchaException e)
        {
            result.rejectValue("captcha", "error.user", e.getMessage());
        }

        if (result.hasErrors())
        {
            ra.addFlashAttribute("org.springframework.validation.BindingResult.user", result);
            ra.addFlashAttribute("user", user);
            addFlashMessage(ra, EFlashMessage.ERROR, "Při zpracování formuláře došlo k chybě. Zkontrolujte prosím zadané údaje.");
            return "redirect:/admin/new-registration";
        }

        if(userService.saveNewUser(user) != null)
        {
            addFlashMessage(ra, EFlashMessage.SUCCESS, "Uživatel byl vytvořen");
            return "redirect:/admin/user-list"; // redirect na seznam uživatelů
        }
        else
        {
            addFlashMessage(ra, EFlashMessage.ERROR, "Při ukládání došlo k chybě");
            return "redirect:/admin/new-registration";
        }
    }

    /**
     * View template for list of users.
     * @param page optional parameter of pagination page
     * @param limit optional parameter of pagination limit
     * @return admin/userList ModelAndView
     */
    @GetMapping(path = "/user-list", name = "userList")
    public ModelAndView showUserList(@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer limit){
        ModelAndView modelAndView = new ModelAndView("admin/userList");
        if(limit == null)
        {
            limit = Paginator.ALLOWED_LIMITS[0];
        }
        if(page == null)
        {
            page = 1;
        }
        Paginator paginator = new Paginator((page-1)*limit, limit, new Sort(Sort.Direction.ASC, "lastName"));
        modelAndView.addObject("users", userService.getAllBankUsers(paginator));
        paginator.setPageCount(userService.getUserCount());
        modelAndView.addObject("paginator", paginator);

        return modelAndView;
    }

    /**
     * View template for user editing form.
     * @param userId id of editing user
     * @param model model
     * @return admin/userEdit ModelAndView
     */
    @GetMapping(path = "/user-edit/{userId}", name = "userEdit")
    public ModelAndView showUserEdit(@PathVariable("userId") int userId, Model model){

        ModelAndView modelAndView = new ModelAndView("admin/userEdit");
        if(!model.containsAttribute("user")) {
            modelAndView.addObject("user", userService.findUserById(userId));
        }
        modelAndView.addObject("countries", ECountry.values());
        return modelAndView;
    }

    /**
     * Handler for user edit form POST action.
     * @param user User entity from form
     * @param result BindingResult of User form values
     * @param ra redirect attributes
     * @return redirects to list of users on success, on error redirects back to form with error values.
     */
    @PostMapping(path = "/user-edit-action", name = "userEditAction")
    public String editUser(@Valid @ModelAttribute("user") User user, BindingResult result, RedirectAttributes ra)
    {
        if (result.hasErrors())
        {
            ra.addFlashAttribute("org.springframework.validation.BindingResult.user", result);
            ra.addFlashAttribute("user", user);
            addFlashMessage(ra, EFlashMessage.ERROR, "Při zpracování formuláře došlo k chybě. Zkontrolujte prosím zadané údaje.");
            return "redirect:/admin/user-edit/" + user.getId();
        }
        userService.editUser(user);
        mailService.sendEditedUser(user);
        addFlashMessage(ra, EFlashMessage.SUCCESS, "Údaje uživatele byly upraveny");
        return "redirect:/admin/user-list"; // redirect na seznam uživatelů
    }

    /**
     * Get request for deleting user.
     * @param userId id of user to be deleted
     * @param ra redirect attributes
     * @return redirect:/admin/user-list ModelAndView
     */
    @GetMapping(path = "/user-delete/{userId}", name = "userDelete")
    public ModelAndView userDelete(@PathVariable("userId") int userId, RedirectAttributes ra){
        ModelAndView modelAndView = new ModelAndView("redirect:/admin/user-list");
        User user = userService.deleteUser(userId);
        if (user != null)
        {
            mailService.sendDeletedUser(user.getEmail());
            addFlashMessage(ra, EFlashMessage.SUCCESS, "Uživatelský účet byl smazán");
            return modelAndView;
        }
        else {
            addFlashMessage(ra, EFlashMessage.ERROR, "Uživatelský účet nenalezen");
            return modelAndView;
        }
    }

    /**
     * View template of all bank codes
     * @param page optional parameter of pagination page
     * @param limit optional parameter of pagination limit
     * @return admin/bankCodes ModelAndView
     */
    @GetMapping(path = "/bank-codes", name = "bankCodes")
    public ModelAndView showBankCodes(@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer limit)
    {
        ModelAndView modelAndView = new ModelAndView("admin/bankCodes");
        if(limit == null)
        {
            limit = Paginator.ALLOWED_LIMITS[0];
        }
        if(page == null)
        {
            page = 1;
        }
        Paginator paginator = new Paginator((page-1)*limit, limit, new Sort(Sort.Direction.ASC, "code"));
        List<BankCode> bankCodes = bankCodeService.findAll(paginator);
        modelAndView.addObject("codes", bankCodes);
        paginator.setPageCount(bankCodeService.countAll());
        modelAndView.addObject("paginator", paginator);

        return modelAndView;
    }

    /**
     * Get request to manually update bank codes from external source
     * @param ra redirect attributes
     * @return redirect:/admin/bank-codes ModelAndView
     */
    @GetMapping(path = "/update-codes", name = "updateBankCodes")
    public ModelAndView updateBankCodes(RedirectAttributes ra)
    {
        ModelAndView modelAndView = new ModelAndView("redirect:/admin/bank-codes");
        if(bankCodeService.updateBankCodesFromExternalSource())
        {
            addFlashMessage(ra, EFlashMessage.SUCCESS, "Seznam bankovních kódů byl aktualizován");
        }
        else
        {
            addFlashMessage(ra, EFlashMessage.ERROR, "Nepodařilo se stáhnout soubor s aktualizovanými kódy");
        }
        return modelAndView;
    }
}
