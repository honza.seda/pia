package cz.seda.spring.repository;

import cz.seda.spring.model.Account;
import cz.seda.spring.model.TransactionDetail;
import cz.seda.spring.model.TransactionQueue;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface TransactionQueueRepository extends JpaRepository<TransactionQueue, Integer> {
    List<TransactionQueue> findAllByScheduledTimeIsLessThanEqualAndProcessedTimeIsNull(LocalDateTime dateTime);

    /**
     * Finds all transactions of account in queue that have been processed
     * @param account Account entity
     * @param pageable pagination
     * @return List of TransactionQueue entities
     */
    @Query("SELECT transactionQueue " +
            "FROM TransactionQueue transactionQueue " +
            "INNER JOIN FETCH transactionQueue.transactionDetail transactionDetail " +
            "LEFT JOIN FETCH transactionDetail.account " +
            "WHERE (transactionQueue.transactionDetail.account = :account " +
            "OR transactionQueue.transactionDetail.targetAccount = :account) " +
            "AND transactionQueue.processedTime IS NOT NULL")
    List<TransactionQueue> findAllByAccount(@Param("account") Account account, Pageable pageable);

    /**
     * Gets count of all transactions of account in queue that have been processed
     * @param account Account entity
     * @return Integer count
     */
    @Query("SELECT count (transactionDetail) " +
            "FROM TransactionQueue transactionQueue " +
            "INNER JOIN transactionQueue.transactionDetail transactionDetail " +
            "LEFT JOIN transactionDetail.account " +
            "WHERE (transactionQueue.transactionDetail.account = :account " +
            "OR transactionQueue.transactionDetail.targetAccount = :account) " +
            "AND transactionQueue.processedTime IS NOT NULL")
    Integer getCountByAccount(@Param("account") Account account);

    /**
     * Finds transactions of account in queue that are waiting to be processed
     * @param account Account entity
     * @param pageable pagination
     * @return List of TransactionQueue entities
     */
    @Query("SELECT transactionQueue " +
            "FROM TransactionQueue transactionQueue " +
            "INNER JOIN FETCH transactionQueue.transactionDetail transactionDetail " +
            "INNER JOIN FETCH transactionDetail.account " +
            "WHERE transactionQueue.transactionDetail.account = :account " +
            "AND transactionQueue.processedTime IS NULL " +
            "AND transactionDetail.standingTransactionSettings IS NULL")
    List<TransactionQueue> findWaitingOrders(@Param("account") Account account, Pageable pageable);

    /**
     * finds all records in queue for transaction detail that have already been processed, orders them by processed time descending
     * @param transactionDetail TransactionDetail entity
     * @return List of TransactionQueue entities
     */
    List<TransactionQueue> findAllByTransactionDetailAndProcessedTimeIsNotNullOrderByProcessedTimeDesc(TransactionDetail transactionDetail);

    /**
     * finds all records in queue for transaction detail that are waiting to be processed
     * @param transactionDetail TransactionDetail entity
     * @return List of TransactionQueue entities
     */
    TransactionQueue findByTransactionDetailAndProcessedTimeIsNull(TransactionDetail transactionDetail);
}
