package cz.seda.spring.repository;

import cz.seda.spring.model.StandingTransactionSettings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StandingTransactionSettingsRepository extends JpaRepository<StandingTransactionSettings, Integer> {
    /**
     * finds all standing transaction settings
     * @return List of StandingTransactionSettings entities
     */
    List<StandingTransactionSettings> findAll();
}
