package cz.seda.spring.repository;

import cz.seda.spring.model.BankCode;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BankCodeRepository extends JpaRepository<BankCode, Integer> {
    /**
     * finds all bank codes in database
     * @param pageable Pageable for query paging
     * @return List of BankCode entities
     */
    List<BankCode> findAllBy(Pageable pageable);

    /**
     * returns count of all bank codes in database
     * @return integer count
     */
    Integer countAllBy();
}
