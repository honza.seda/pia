package cz.seda.spring.repository;

import cz.seda.spring.model.Account;
import cz.seda.spring.model.TransactionDetail;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionDetailRepository extends JpaRepository<TransactionDetail, Integer> {
    List<TransactionDetail> findAllByAccountOrderByDueDateDesc(Account account);

    /**
     * Finds all standing transactions of account
     * @param account Account entity
     * @param pageable pagination
     * @return List of TransactionDetail entities
     */
    @Query("SELECT transactionDetail " +
            "FROM TransactionDetail transactionDetail " +
            "INNER JOIN FETCH transactionDetail.standingTransactionSettings standingTransactionSettings " +
            "WHERE transactionDetail.account = :account " +
            "AND transactionDetail.standingTransactionSettings IS NOT NULL")
    List<TransactionDetail> findAllStandingTransactions(@Param("account") Account account, Pageable pageable);

    /**
     * Gets count of all standing transactions of account
     * @param account Account entity
     * @return Integer count
     */
    @Query("SELECT count(transactionDetail) " +
            "FROM TransactionDetail transactionDetail " +
            "INNER JOIN transactionDetail.standingTransactionSettings standingTransactionSettings " +
            "WHERE transactionDetail.account = :account " +
            "AND transactionDetail.standingTransactionSettings IS NOT NULL")
    Integer countStandingTransactions(@Param("account") Account account);
}
