package cz.seda.spring.repository;

import cz.seda.spring.model.Account;
import cz.seda.spring.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository  extends JpaRepository<Account, Integer> {
    /**
     * finds account by User entity
     * @param user User entity
     * @return Account entity
     */
    Account findAccountByUser(User user);

    /**
     * finds account by account number
     * @param accountNumber string with account number
     * @return Account entity
     */
    Account findByAccountNumber(String accountNumber);
}
