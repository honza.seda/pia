package cz.seda.spring.repository;

import cz.seda.spring.model.Account;
import cz.seda.spring.model.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CardRepository extends JpaRepository<Card, Integer> {
    /**
     * finds Card entity by Account entity
     * @param account Account entity
     * @return Card entity
     */
    Card findByAccount(Account account);
}
