package cz.seda.spring.repository;

import cz.seda.spring.model.Account;
import cz.seda.spring.model.TransactionTemplate;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TransactionTemplateRepository extends JpaRepository<TransactionTemplate, Integer> {
    /**
     * finds all templates for account
     * @param account Account entity
     * @return List of TransactionTemplate entities
     */
    List<TransactionTemplate> findAllByAccount(Account account);

    /**
     * finds all templates for account with result paginating
     * @param account Account entity
     * @param pageable Pageable for query paging
     * @return List of TransactionTemplate entities
     */
    List<TransactionTemplate> findAllByAccount(Account account, Pageable pageable);

    /**
     * count of all templates for account
     * @param account Account entity
     * @return integer count
     */
    Integer countByAccount(Account account);

    /**
     * get specific template by id
     * @param id template id
     * @return TransactionTemplate entity
     */
    TransactionTemplate getById(Integer id);
}
