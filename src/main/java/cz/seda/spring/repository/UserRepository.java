package cz.seda.spring.repository;

import cz.seda.spring.model.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer>
{
    /**
     * finds user by login string
     * @param login login
     * @return User entity
     */
    User findUserByLogin(String login);

    /**
     * finds user by id
     * @param id integer id
     * @return User entity
     */
    User findUserById(int id);

    /**
     * finds users with specified role and deleted flag
     * @param role user role
     * @param deleted user deleted flag
     * @param pageable Pageable for query paging
     * @return List of User entities
     */
    List<User> findAllByRoleEqualsAndDeleted(String role, boolean deleted, Pageable pageable);

    /**
     * count of users with specified role and deleted flag
     * @param role user role
     * @param deleted user deleted flag
     * @return Integer count
     */
    Integer countByRoleEqualsAndDeleted(String role, boolean deleted);
}
