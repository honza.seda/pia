package cz.seda.spring.exception;

/**
 * Runtime exception for any transaction related exceptions
 */
public class TransactionException extends RuntimeException {
    public TransactionException(final String message) {
        super(message);
    }
}

