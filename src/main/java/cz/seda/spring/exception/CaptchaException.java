package cz.seda.spring.exception;

/**
 * Runtime exceptions for reCaptcha validation exceptions
 */
public class CaptchaException extends RuntimeException  {
    private static final long serialVersionUID = 5861310537366287163L;

    public CaptchaException() {
        super();
    }

    public CaptchaException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public CaptchaException(final String message) {
        super(message);
    }

    public CaptchaException(final Throwable cause) {
        super(cause);
    }
}
