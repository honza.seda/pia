package cz.seda.spring.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * BankCode entity
 */
@Entity
@Table(name = "bank_code")
public class BankCode {
    @Id
    @Column(name = "code")
    private String code;

    @Column(name = "name", nullable = false, length = 100)
    @NotNull
    private String name;

    @Column(name = "swift", length = 11)
    private String swift;

    public BankCode() {
    }

    public BankCode(String code, @NotNull String name, String swift) {
        this.code = code;
        this.name = name;
        this.swift = swift;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameForSelect() {
        return this.code + " / " + this.name;
    }

    public String getSwift() {
        return swift;
    }

    public void setSwift(String swift) {
        this.swift = swift;
    }
}
