package cz.seda.spring.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Card entity
 */
@Entity
@Table(name = "card")
public class Card {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "card_number", nullable = false, length = 16)
    @NotNull
    @Size(min = 16, max = 16, message = "Číslo karty musí mít {max} znaků.")
    private String cardNumber;

    @Column(name = "expiration_month", nullable = false, length = 2)
    @NotNull
    private Integer expirationMonth;

    @Column(name = "expiration_year", nullable = false, length = 4)
    @NotNull
    private Integer expirationYear;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "account_id")
    private Account account;

    @Column(name = "pin", nullable = false, length = 10)
    @NotNull
    private Integer pin;

    @Column(name = "international_payments")
    private boolean internationalPayments;

    public Card() {
    }

    public Card(
            @NotNull @Size(min = 16, max = 16, message = "Číslo karty musí mít {max} znaků.") String cardNumber,
            @NotNull Integer expirationMonth,
            @NotNull Integer expirationYear,
            @NotNull Account account,
            @NotNull Integer pin
    ) {
        this.cardNumber = cardNumber;
        this.expirationMonth = expirationMonth;
        this.expirationYear = expirationYear;
        this.account = account;
        this.pin = pin;
    }

    public Integer getId() {
        return id;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Integer getExpirationMonth() {
        return expirationMonth;
    }

    public void setExpirationMonth(Integer expirationMonth) {
        this.expirationMonth = expirationMonth;
    }

    public Integer getExpirationYear() {
        return expirationYear;
    }

    public void setExpirationYear(Integer expirationYear) {
        this.expirationYear = expirationYear;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Integer getPin() {
        return pin;
    }

    public void setPin(Integer pin) {
        this.pin = pin;
    }

    public boolean isInternationalPayments() {
        return internationalPayments;
    }

    public void setInternationalPayments(boolean internationalPayments) {
        this.internationalPayments = internationalPayments;
    }
}
