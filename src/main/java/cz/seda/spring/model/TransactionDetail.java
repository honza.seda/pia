package cz.seda.spring.model;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.Nullable;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.websocket.OnError;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

/**
 * TransactionDetail entity
 */
@Entity
@Table(name = "transaction_detail")
public class TransactionDetail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;

    @Column(name = "sender_account_number", nullable = false, length = 45)
    @Pattern(regexp = "^(?:\\d+-?\\d+)$", message = "Číslo účtu může obsahovat pouze číslice a případně znak \"-\" pro odddělení předčíslí")
    @Size(min = 2, max = 45, message = "Číslo účtu musí být v rozmezí {min} - {max} znaků.")
    private String senderAccountNumber;

    @Column(name = "sender_bank_code", nullable = false, length = 4)
    private String senderBankCode;

    @Column(name = "amount", nullable = false, length = 45)
    @NotNull(message = "Částka musí být vyplněna")
    @Min(value = 0, message = "Částka musí být kladná")
    private Double amount;

    @Column(name = "target_account_number", nullable = false, length = 45)
    @NotNull(message = "Nebylo zadáno číslo cílového účtu")
    @Pattern(regexp = "^(?:\\d+-?\\d+)$", message = "Číslo účtu může obsahovat pouze číslice a případně znak \"-\" pro odddělení předčíslí")
    @Size(min = 2, max = 45, message = "Číslo účtu musí být v rozmezí {min} - {max} znaků.")
    private String targetAccountNumber;

    @Column(name = "target_bank_code", nullable = false, length = 4)
    @NotNull(message = "Musíte zvolit kód banky cílového účtu")
    @Pattern(regexp = "^[\\d]{1,4}$", message = "Kód banky musí být číselný")
    private String targetBankCode;

    @ManyToOne
    @JoinColumn(name = "target_account_id")
    private Account targetAccount;

    @Column(name = "due_date", nullable = false)
    @NotNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dueDate;

    @Column(name = "ks", length = 11)
    private Long ks;

    @Column(name = "vs", length = 11)
    private Long vs;

    @Column(name = "ss", length = 11)
    private Long ss;

    @Column(name = "message", length = 200)
    @Size(max = 200, message = "Zpráva může mít maximálně {max} znaků.")
    private String message;

    @ManyToOne
    @JoinColumn(name = "standing_transaction_settings_id")
    private StandingTransactionSettings standingTransactionSettings;

    @OneToMany(mappedBy = "transactionDetail")
    private List<TransactionQueue> transactionQueue;

    // Dummy field for storing bindingResult captcha error
    @Transient
    private boolean captcha;

    @Transient
    @Pattern(regexp = "^(?:[\\d]{2}\\:[\\d]{2}|)$", message = "Čas splatnosti musí mít formát \"hh:mm\"")
    @Nullable
    private String dueTime;

    public TransactionDetail() {
    }

    public TransactionDetail(
            @NotNull(message = "Částka musí být vyplněna") Double amount,
            @NotNull(message = "Nebylo zadáno číslo cílového účtu") @Pattern(regexp = "^(?:\\d+-?\\d+)$", message = "Číslo účtu může obsahovat pouze číslice a případně znak \"-\" pro odddělení předčíslí") @Size(min = 2, max = 45, message = "Číslo účtu musí být v rozmezí {min} - {max} znaků.") String targetAccountNumber,
            @NotNull(message = "Musíte zvolit kód banky cílového účtu") @Pattern(regexp = "^[\\d]{1,4}$", message = "Kód banky musí být číselný") String targetBankCode,
            @NotNull LocalDate dueDate,
            Long ks,
            Long vs,
            Long ss,
            @Size(max = 200, message = "Zpráva může mít maximálně {max} znaků.") String message
    ) {
        this.amount = amount;
        this.targetAccountNumber = targetAccountNumber;
        this.targetBankCode = targetBankCode;
        this.dueDate = dueDate;
        this.ks = ks;
        this.vs = vs;
        this.ss = ss;
        this.message = message;
    }

    public TransactionDetail(
            Account account,
            @NotNull @Size(min = 2, max = 45, message = "Číslo účtu musí být v rozmezí {min} - {max} znaků.") String senderAccountNumber,
            @NotNull String senderBankCode,
            @NotNull Double amount,
            @NotNull @Size(min = 2, max = 45, message = "Číslo účtu musí být v rozmezí {min} - {max} znaků.") String targetAccountNumber,
            @NotNull String targetBankCode,
            Account targetAccount,
            @NotNull LocalDate dueDate,
            Long ks,
            Long vs,
            Long ss,
            @Size(max = 200, message = "Zpráva může mít maximálně {max} znaků.") String message
    ) {
        this.account = account;
        this.amount = amount;
        this.senderAccountNumber = senderAccountNumber;
        this.senderBankCode = senderBankCode;
        this.targetAccountNumber = targetAccountNumber;
        this.targetBankCode = targetBankCode;
        this.targetAccount = targetAccount;
        this.dueDate = dueDate;
        this.ks = ks;
        this.vs = vs;
        this.ss = ss;
        this.message = message;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getSenderAccountNumber() {
        return senderAccountNumber;
    }

    public void setSenderAccountNumber(String senderAccountNumber) {
        this.senderAccountNumber = senderAccountNumber;
    }

    public String getSenderBankCode() {
        return senderBankCode;
    }

    public void setSenderBankCode(String senderBankCode) {
        this.senderBankCode = senderBankCode;
    }

    public String getTargetAccountNumber() {
        return targetAccountNumber;
    }

    public void setTargetAccountNumber(String targetAccountNumber) {
        this.targetAccountNumber = targetAccountNumber;
    }

    public String getTargetBankCode() {
        return targetBankCode;
    }

    public void setTargetBankCode(String targetBankCode) {
        this.targetBankCode = targetBankCode;
    }

    public Account getTargetAccount() {
        return targetAccount;
    }

    public void setTargetAccount(Account targetAccount) {
        this.targetAccount = targetAccount;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public Long getKs() {
        return ks;
    }

    public void setKs(Long ks) {
        this.ks = ks;
    }

    public Long getVs() {
        return vs;
    }

    public void setVs(Long vs) {
        this.vs = vs;
    }

    public Long getSs() {
        return ss;
    }

    public void setSs(Long ss) {
        this.ss = ss;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isCaptcha() {
        return captcha;
    }

    public void setCaptcha(boolean captcha) {
        this.captcha = captcha;
    }

    public String getDueTime() {
        return dueTime;
    }

    public void setDueTime(String dueTime) {
        this.dueTime = dueTime;
    }

    public StandingTransactionSettings getStandingTransactionSettings() {
        return standingTransactionSettings;
    }

    public void setStandingTransactionSettings(StandingTransactionSettings standingTransactionSettings) {
        this.standingTransactionSettings = standingTransactionSettings;
    }
}
