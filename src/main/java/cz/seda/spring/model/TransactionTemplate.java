package cz.seda.spring.model;


import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * TransactionTemplate entity
 */
@Entity
@Table(name = "transaction_template")
public class TransactionTemplate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "account_id")
    private Account account;

    @Column(name = "name", nullable = false, length = 100)
    @NotNull(message = "Název šablony nesmí být prázdný")
    @Size(min = 1, max = 100, message = "Název šablony musí být v rozmezí {min} - {max} znaků.")
    private String name;

    @Column(name = "amount", nullable = false, length = 45)
    @NotNull(message = "Částka musí být vyplněna")
    @Min(value = 0, message = "Částka musí být kladná")
    private Double amount;

    @Column(name = "account_number", nullable = false, length = 45)
    @NotNull(message = "Nebylo zadáno číslo cílového účtu")
    @Pattern(regexp = "^(?:\\d+-?\\d+)$", message = "Číslo účtu může obsahovat pouze číslice a případně znak \"-\" pro odddělení předčíslí")
    @Size(min = 2, max = 45, message = "Číslo účtu musí být v rozmezí {min} - {max} znaků.")
    private String targetAccountNumber;

    @Column(name = "bank_code", nullable = false, length = 4)
    @NotNull(message = "Musíte zvolit kód banky cílového účtu")
    @Pattern(regexp = "^[\\d]{1,4}$", message = "Kód banky musí být číselný")
    private String targetBankCode;

    @Column(name = "ks", length = 11)
    private Long ks;

    @Column(name = "vs", length = 11)
    private Long vs;

    @Column(name = "ss", length = 11)
    private Long ss;

    @Column(name = "message", length = 200)
    @Size(max = 200, message = "Zpráva může mít maximálně {max} znaků.")
    private String message;

    public TransactionTemplate() {
    }

    public TransactionTemplate(
            @NotNull @Size(min = 1, max = 100, message = "Název šablony musí být v rozmezí {min} - {max} znaků.") String name,
            Account account,
            @NotNull Double amount,
            @NotNull @Size(min = 2, max = 45, message = "Číslo účtu musí být v rozmezí {min} - {max} znaků.") String targetAccountNumber,
            @NotNull String targetBankCode,
            Long ks,
            Long vs,
            Long ss,
            @Size(max = 200, message = "Zpráva může mít maximálně {max} znaků.") String message
    ) {
        this.name = name;
        this.account = account;
        this.amount = amount;
        this.targetAccountNumber = targetAccountNumber;
        this.targetBankCode = targetBankCode;
        this.ks = ks;
        this.vs = vs;
        this.ss = ss;
        this.message = message;
    }

    public TransactionTemplate(
            @NotNull(message = "Částka musí být vyplněna") Double amount,
            @NotNull(message = "Nebylo zadáno číslo cílového účtu") @Pattern(regexp = "^(?:\\d+-?\\d+)$", message = "Číslo účtu může obsahovat pouze číslice a případně znak \"-\" pro odddělení předčíslí") @Size(min = 2, max = 45, message = "Číslo účtu musí být v rozmezí {min} - {max} znaků.") String targetAccountNumber,
            @NotNull(message = "Musíte zvolit kód banky cílového účtu") @Pattern(regexp = "^[\\d]{1,4}$", message = "Kód banky musí být číselný") String targetBankCode,
            Long ks,
            Long vs,
            Long ss,
            @Size(max = 200, message = "Zpráva může mít maximálně {max} znaků.") String message
    ) {
        this.amount = amount;
        this.targetAccountNumber = targetAccountNumber;
        this.targetBankCode = targetBankCode;
        this.ks = ks;
        this.vs = vs;
        this.ss = ss;
        this.message = message;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getTargetAccountNumber() {
        return targetAccountNumber;
    }

    public void setTargetAccountNumber(String targetAccountNumber) {
        this.targetAccountNumber = targetAccountNumber;
    }

    public String getTargetBankCode() {
        return targetBankCode;
    }

    public void setTargetBankCode(String targetBankCode) {
        this.targetBankCode = targetBankCode;
    }

    public Long getKs() {
        return ks;
    }

    public void setKs(Long ks) {
        this.ks = ks;
    }

    public Long getVs() {
        return vs;
    }

    public void setVs(Long vs) {
        this.vs = vs;
    }

    public Long getSs() {
        return ss;
    }

    public void setSs(Long ss) {
        this.ss = ss;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
