package cz.seda.spring.model;

import cz.seda.spring.util.enums.ECountry;

import javax.persistence.*;
import javax.validation.constraints.*;

/**
 * User entity
 */
@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "login", nullable = false, length = 8, unique = true)
    private String login;

    @Column(name = "password", nullable = false)
    private String password;

    @Column(name = "firstname", nullable = false, length = 45)
    @NotNull(message = "Jméno je povinný údaj")
    @Size(min = 2, max = 45, message = "Jméno musí být v rozmezí {min} - {max} znaků")
    private String firstName;

    @Column(name = "lastname", nullable = false, length = 45)
    @NotNull(message = "Příjmení je povinný údaj")
    @Size(min = 2, max = 45, message = "Příjmení musí být v rozmezí {min} - {max} znaků")
    private String lastName;

    @Column(name = "pin", nullable = false, length = 10)
    @NotNull(message = "Rodné číslo je povinný údaj")
    @Pattern(regexp = "^(?:[0-9]{10})$", message = "Rodné číslo může obsahovat pouze číslice, číslo zadávejte bez lomítka (10 číslic)")
    private String pin;

    @Column(name = "street", length = 100)
    @Size(max = 100, message = "Ulice může mít maximálně {max} znaků")
    private String street;

    @Column(name = "house_number", length = 11)
    @Pattern(regexp = "^(?:[0-9]{0,5}|)$", message = "Číslo popisné musí být číslo, maximálně 5 číslic")
    private String houseNumber;

    @Column(name = "city", length = 100)
    @Size(max = 100, message = "Město může mít maximálně {max} znaků")
    private String city;

    @Column(name = "country")
    @Enumerated(EnumType.STRING)
    private ECountry country;

    @Column(name = "zip")
    @Pattern(regexp = "^(?:[0-9]{5}|)$", message = "PSČ musí být pětimístné číslo (bez mezery)")
    private String zip;

    @Column(name = "email", nullable = false, length = 65, unique = true)
    @NotNull(message = "Email je povinný údaj")
    @Size(min = 5, max = 65, message = "Email musí být v rozmezí {min} - {max} znaků")
    @Email(message = "Neplatný formát emailu")
    private String email;

    @Column(name = "phone", length = 13)
    @Pattern(regexp = "^(?:[+]?[\\d]{9,12}|)$", message = "Telefonní číslo může obsahovat pouze číslice (min 9 číslic), případně znak \"+\" pro předčíslí")
    @Size(max = 13, message = "Telefon může mít maimálně {max} znaků")
    private String phone;

    @Column(name = "role", nullable = false)
    @NotNull
    private String role = "ROLE_USER";

    @Column(name = "deleted")
    private boolean deleted = false;

    // Dummy field for storing bindingResult captcha error
    @Transient
    private boolean captcha;

    public User(){}

    public User(
            String login,
            String password,
            @NotNull @Size(min = 2, max = 45, message = "Jméno musí být v rozmezí {min} - {max} znaků") String firstName,
            @NotNull @Size(min = 2, max = 45, message = "Příjmení musí být v rozmezí {min} - {max} znaků") String lastName,
            @NotNull @Pattern(regexp = "^(?:[0-9]{10})$", message = "Rodné číslo může obsahovat pouze číslice, číslo zadávejte bez lomítka (10 číslic)") String pin,
            @Size(max = 100, message = "Ulice může mít maximálně {max} znaků") String street,
            @Pattern(regexp = "^(?:[0-9]{0,5}|)$", message = "Číslo popisné musí být číslo, maximálně 5 číslic") String houseNumber,
            @Size(max = 100, message = "Město může mít maximálně {max} znaků") String city,
            ECountry country,
            @Pattern(regexp = "^(?:[0-9]{5}|)$", message = "PSČ musí být pětimístné číslo (bez mezery)") String zip,
            @NotNull @Size(min = 5, max = 65, message = "Email musí být v rozmezí {min} - {max} znaků") String email,
            @Size(max = 13, message = "Telefon může mít maimálně {max} znaků") String phone,
            @NotNull String role
    ) {
        this.login = login;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.pin = pin;
        this.street = street;
        this.houseNumber = houseNumber;
        this.city = city;
        this.country = country;
        this.zip = zip;
        this.email = email;
        this.phone = phone;
        this.role = role;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public ECountry getCountry() {
        return country;
    }

    public void setCountry(ECountry country) {
        this.country = country;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public boolean isCaptcha() {
        return captcha;
    }

    public void setCaptcha(boolean captcha) {
        this.captcha = captcha;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
