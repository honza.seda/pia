package cz.seda.spring.model;

import cz.seda.spring.util.enums.EStandingOrderFrequency;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * StandingTransactionSettings entity
 */
@Entity
@Table(name = "standing_transaction_settings")
public class StandingTransactionSettings {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name", nullable = false, length = 100)
    @NotNull(message = "Název trvalého příkazu nesmí být prázdný")
    @Size(min = 2, max = 45, message = "Název trvalého příkazu musí být v rozmezí {min} - {max} znaků.")
    private String name;

    @Column(name = "start_date", nullable = false)
    @NotNull(message = "Datum první platby musí být vyplněn")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @Column(name = "end_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    @Column(name = "frequency", nullable = false)
    @Enumerated(EnumType.STRING)
    @NotNull(message = "Frekvence plateb musí být vyplněna")
    private EStandingOrderFrequency frequency;

    @Column(name = "last_transaction")
    private LocalDateTime lastTransaction;

    @Column(name = "repeats")
    @Min(value = 2, message = "Počet opakování musí být alespoň 2")
    private Integer repeats;

    @Column(name = "paid_count")
    private Integer paidCount;

    @Column(name = "active")
    private boolean active;

    public StandingTransactionSettings() {
    }

    public StandingTransactionSettings(
            @NotNull @Size(min = 2, max = 45, message = "Název trvalého příkazu musí být v rozmezí {min} - {max} znaků.") String name,
            @NotNull LocalDate startDate,
            @NotNull LocalDate endDate,
            EStandingOrderFrequency frequency,
            Integer repeats
    ) {
        this.name = name;
        this.startDate = startDate;
        this.endDate = endDate;
        this.frequency = frequency;
        this.repeats = repeats;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public EStandingOrderFrequency getFrequency() {
        return frequency;
    }

    public void setFrequency(EStandingOrderFrequency frequency) {
        this.frequency = frequency;
    }

    public Integer getRepeats() {
        return repeats;
    }

    public void setRepeats(Integer repeats) {
        this.repeats = repeats;
    }

    public LocalDateTime getLastTransaction() {
        return lastTransaction;
    }

    public void setLastTransaction(LocalDateTime lastTransaction) {
        this.lastTransaction = lastTransaction;
    }

    public Integer getPaidCount() {
        return paidCount;
    }

    public void setPaidCount(Integer paidCount) {
        this.paidCount = paidCount;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
