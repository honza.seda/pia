package cz.seda.spring.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Account entity
 */
@Entity
@Table(name = "account")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @NotNull
    private User user;

    @Column(name = "balance", nullable = false)
    @NotNull
    private Double balance;

    @Column(name = "available_balance", nullable = false)
    @NotNull
    private Double availableBalance;

    @Column(name = "account_number", nullable = false, length = 45)
    @NotNull
    private String accountNumber;

    @Column(name = "currency", nullable = false, length = 3)
    @NotNull
    @Size(min = 2, max = 3, message = "Neplatná měna.")
    private String currency;

    @Column(name = "send_notifications")
    private boolean sendNotifications;

    @OneToMany(mappedBy = "account")
    private List<TransactionDetail> transactionDetails;

    public Account() {
    }

    public Account(@NotNull User user, @NotNull Double balance, @NotNull Double availableBalance, String accountNumber, String currency, boolean sendNotifications) {
        this.user = user;
        this.balance = balance;
        this.availableBalance = availableBalance;
        this.accountNumber = accountNumber;
        this.currency = currency;
        this.sendNotifications = sendNotifications;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public Double getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(Double availableBalance) {
        this.availableBalance = availableBalance;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public boolean isSendNotifications() {
        return sendNotifications;
    }

    public void setSendNotifications(boolean sendNotifications) {
        this.sendNotifications = sendNotifications;
    }
}
