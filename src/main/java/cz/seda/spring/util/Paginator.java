package cz.seda.spring.util;


import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.io.Serializable;

/**
 * Implementation of spring Data Pageable to paginate and sort JPA queries
 */
public class Paginator implements Pageable, Serializable {
    private int limit;
    private long offset;
    private final Sort sort;
    private int pageCount, itemCount;

    private int startingPage, endingPage;

    public static int[] ALLOWED_LIMITS = {10, 25, 50, 100};
    public static final int OVERWIEV_TRANSACTION_COUNT = 5;

    /**
     * Implementation of spring Data Pageable to paginate and sort JPA queries
     * @param offset query offset
     * @param limit query limit
     * @param sort query sort
     */
    public Paginator(long offset, int limit, Sort sort) {
        if (offset < 0) {
            offset = 0;
        }
        this.offset = offset;

        if(limit == OVERWIEV_TRANSACTION_COUNT) {
            this.limit = limit;
        }
        else
        {
            for (int l : ALLOWED_LIMITS) {
                if (l == limit) {
                    this.limit = limit;
                }
            }
        }
        if(this.limit == 0){
            this.limit = ALLOWED_LIMITS[0];
        }

        this.sort = sort;
    }

    public Paginator(long offset, int limit) {
        this(offset, limit, new Sort(Sort.Direction.ASC,"id"));
    }

    @Override
    public int getPageNumber() {
        return 1 + (int) offset / limit;
    }

    @Override
    public int getPageSize() {
        return limit;
    }

    @Override
    public long getOffset() {
        return offset;
    }

    @Override
    public Sort getSort() {
        return sort;
    }

    @Override
    public Pageable next() {
        return hasNext() ? new Paginator(getOffset() + getPageSize(), getPageSize(), getSort()) : this;
    }

    public Paginator previous() {
        return hasPrevious() ? new Paginator(getOffset() - getPageSize(), getPageSize(), getSort()) : this;
    }


    @Override
    public Pageable previousOrFirst() {
        return hasPrevious() ? previous() : first();
    }

    @Override
    public Pageable first() {
        return new Paginator(0, getPageSize(), getSort());
    }

    @Override
    public boolean hasPrevious() {
        return offset >= limit;
    }

    public boolean hasNext() {
        return getPageNumber() < getPageCount();
    }

    public Paginator last() {
        return new Paginator((getPageCount() - 1) * getPageSize() , getPageSize(), getSort());
    }

    public int getPageCount() {
        return pageCount;
    }

    public int getItemCount() {
        return itemCount;
    }

    /**
     * Sets maximum possible page to be displayed and total number of items
     * @param itemCount total count
     */
    public void setPageCount(int itemCount) {
        this.itemCount = itemCount;
        this.pageCount = (int) Math.ceil(itemCount / (double) getPageSize());
        setPagesList();
    }

    public int[] getAllowedLimits()
    {
        return ALLOWED_LIMITS;
    }

    /**
     * Sets starting and ending page number to be displayed in template
     */
    private void setPagesList(){
        int offset = 2;

        if (getPageNumber() + offset >= getPageCount()) {
            this.startingPage = Math.max(1, getPageCount() - 2*offset);
            this.endingPage = getPageCount();
        } else {
            this.startingPage= Math.max(1, getPageNumber() - offset);
            this.endingPage = Math.min(startingPage + 2*offset, getPageCount());
        }
    }

    public int getStartingPage() {
        return startingPage;
    }

    public int getEndingPage() {
        return endingPage;
    }
}
