package cz.seda.spring.util;

/**
 * String time formatter helper class
 */
public class TimeFormatter {
    /**
     * Outputs milliseconds as a more readable form
     * @param ms String milliseconds
     * @return Formatted time string
     */
    public static String msToString(long ms) {
        long totalSecs = ms/1000;
        long hours = (totalSecs / 3600);
        long days = (hours / 24);
        long mins = (totalSecs / 60) % 60;
        long secs = totalSecs % 60;
        String minsString = (mins == 0)
                ? "00"
                : ((mins < 10)
                ? "0" + mins
                : "" + mins);
        String secsString = (secs == 0)
                ? "00"
                : ((secs < 10)
                ? "0" + secs
                : "" + secs);
        if (days > 0)
            return days + "d " + hours % 24 + "h " + minsString + "m " + secsString + "s";
        else if (hours > 0)
            return hours + "h " + minsString + "m " + secsString + "s";
        else if (mins > 0)
            return mins + "m " + secsString + "s";
        else return secsString + "s";
    }
}
