package cz.seda.spring.util;

import cz.seda.spring.exception.TransactionException;
import cz.seda.spring.model.Account;
import cz.seda.spring.model.StandingTransactionSettings;
import cz.seda.spring.model.TransactionDetail;
import cz.seda.spring.model.TransactionQueue;
import cz.seda.spring.repository.AccountRepository;
import cz.seda.spring.repository.TransactionQueueRepository;
import cz.seda.spring.service.BankCodeService;
import cz.seda.spring.service.MailService;
import cz.seda.spring.service.TransactionService;
import cz.seda.spring.service.interfaces.IBankCodeService;
import cz.seda.spring.service.interfaces.IMailService;
import cz.seda.spring.service.interfaces.ITransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.transaction.TransactionalException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Scheduled tasks executed automatically inside application
 */
@Component
public class ScheduledTasks {
    private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);

    private TransactionQueueRepository transactionQueueRepository;
    private AccountRepository accountRepository;
    private IBankCodeService bankCodeService;
    private IMailService mailService;
    private ITransactionService transactionService;

    @Autowired
    public ScheduledTasks(
            TransactionQueueRepository transactionQueueRepository,
            AccountRepository accountRepository,
            BankCodeService bankCodeService,
            MailService mailService,
            TransactionService transactionService
    ) {
        this.transactionQueueRepository = transactionQueueRepository;
        this.accountRepository = accountRepository;
        this.bankCodeService = bankCodeService;
        this.mailService = mailService;
        this.transactionService = transactionService;
    }

    /**
     * Cycles through transaction queue and processes those transactions that have scheduledTime lower
     * than the time of scheduled task execution. Calls method to send email notification about processed payments.
     */
    @Scheduled(initialDelay=0, fixedRateString="${pia.settings.transactionProcessRate}")
    public void processTransactionQueue() {
        LocalDateTime runDateTime = LocalDateTime.now();
        System.out.println(runDateTime + " - Running cz.seda.spring.util.ScheduledTasks.processTransactionQueue");
        List<TransactionQueue> queue = transactionQueueRepository.findAllByScheduledTimeIsLessThanEqualAndProcessedTimeIsNull(runDateTime);
        for (TransactionQueue transaction: queue) {
            ArrayList<String> mailNotificationRecipients = new ArrayList<>();
            transaction.setProcessedTime(runDateTime);
            TransactionDetail detail = transaction.getTransactionDetail();
            Account account = detail.getAccount();
            StandingTransactionSettings standingTransactionSettings = detail.getStandingTransactionSettings();
            if (account != null) {
                account.setBalance(account.getBalance() - detail.getAmount());
                if(standingTransactionSettings != null) {
                    account.setAvailableBalance(account.getAvailableBalance() - detail.getAmount());
                }
                if (account.isSendNotifications()) {
                    mailNotificationRecipients.add(account.getUser().getEmail());
                }
            }

            Account targetAccount = detail.getTargetAccount();
            if(targetAccount != null)
            {
                targetAccount.setBalance(targetAccount.getBalance() + detail.getAmount());
                targetAccount.setAvailableBalance(targetAccount.getAvailableBalance() + detail.getAmount());
                accountRepository.save(targetAccount);
                if (targetAccount.isSendNotifications())
                {
                    mailNotificationRecipients.add(targetAccount.getUser().getEmail());
                }
            }

            if(standingTransactionSettings != null)
            {
                try {
                    transactionService.addNewStandingTransactionPaymentQueue(account, detail);
                }
                catch (TransactionException e)
                {
                    if(account != null) {
                        mailService.sendStandingTransactionLowBalance(account.getUser().getEmail(), detail.getStandingTransactionSettings().getName());
                    }
                }
            }

            accountRepository.save(account);
            transactionQueueRepository.save(transaction);
            if(!mailNotificationRecipients.isEmpty()) {
                mailService.sendPaymentProcessedInfo(String.join(",", mailNotificationRecipients), detail);
            }
        }
    }

    /**
     * Scheduled task to automatically call an update of bank codes from external source
     */
    @Scheduled(initialDelayString = "${pia.settings.updateBankCodesRate}", fixedRateString = "${pia.settings.updateBankCodesRate}")
    public void updateBankCodes() {
        LocalDateTime runDateTime = LocalDateTime.now();
        System.out.println(runDateTime + " - Running cz.seda.spring.util.ScheduledTasks.updateBankCodes");
        bankCodeService.updateBankCodesFromExternalSource();
    }
}
