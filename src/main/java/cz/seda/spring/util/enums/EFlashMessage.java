package cz.seda.spring.util.enums;

/**
 * Enum for flash message types
 */
public enum EFlashMessage {
    ERROR("negative",EFlashIcon.ERROR,"Operace byla neúspěšná"),
    SUCCESS("blue",EFlashIcon.SUCCESS,"Operace proběhla úspěšně"),
    INFO("yellow",EFlashIcon.INFO,"Informace o operaci");

    private String type;
    private EFlashIcon icon;
    private String messageHeader;

    EFlashMessage(String type, EFlashIcon icon, String messageHeader)
    {
        this.type = type;
        this.icon = icon;
        this.messageHeader = messageHeader;
    }

    public String getType()
    {
        return type;
    }

    public EFlashIcon getIcon() {
        return icon;
    }

    public String getMessageHeader() {
        return messageHeader;
    }
}
