package cz.seda.spring.util.enums;

/**
 * Enum with possible flash message icons
 */
public enum EFlashIcon {
    SUCCESS("info"),
    ERROR("exclamation"),
    INFO("question");

    private String icon;

    EFlashIcon(String icon)
    {
        this.icon = icon;
    }

    public String getIcon()
    {
        return icon;
    }
}
