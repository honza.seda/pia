package cz.seda.spring.util.enums;

/**
 * Enum for possible standing transaction settings frequency values
 */
public enum EStandingOrderFrequency {
    min("Minuta"),
    day("Den"),
    week("Týden"),
    month("Měsíc");

    private final String labelName;

    private EStandingOrderFrequency(String labelName) {
        this.labelName = labelName;
    }

    public String getLabelName() {
        return labelName;
    }
}
