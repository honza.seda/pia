package cz.seda.spring.util.enums;

/**
 * Enum for user country values
 */
public enum ECountry {
    cz("Česká republika"),
    sk("Slovensko");

    private final String labelName;

    private ECountry(String labelName) {
        this.labelName = labelName;
    }

    public String getLabelName() {
        return labelName;
    }
}
