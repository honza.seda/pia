package cz.seda.spring.util;

import cz.seda.spring.util.enums.EFlashIcon;
import cz.seda.spring.util.enums.EFlashMessage;

/**
 * Object for storing active flash message to be displayed
 */
public class FlashMessage {
    private EFlashMessage type;
    private String message;

    public FlashMessage(EFlashMessage type, String message) {
        this.type = type;
        this.message = message;
    }

    public String getType() {
        return type.getType();
    }

    public String getIcon() {
        return type.getIcon().getIcon();
    }

    public String getMessageHeader() {
        return type.getMessageHeader();
    }

    public String getMessage() {
        return message;
    }

}
