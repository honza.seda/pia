package cz.seda.spring.service;

import cz.seda.spring.model.Account;
import cz.seda.spring.model.TransactionDetail;
import cz.seda.spring.model.TransactionTemplate;
import cz.seda.spring.repository.TransactionTemplateRepository;
import cz.seda.spring.service.interfaces.ITemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service("templateService")
public class TemplateService implements ITemplateService {
    private TransactionTemplateRepository transactionTemplateRepository;

    @Autowired
    public TemplateService(TransactionTemplateRepository transactionTemplateRepository) {
        this.transactionTemplateRepository = transactionTemplateRepository;
    }

    @Override
    public TransactionDetail mapTemplateByIdToDetail(Integer templateId, Account account) {
        TransactionTemplate template = transactionTemplateRepository.findById(templateId).orElse(null);
        if(template != null){
            if (template.getAccount() == account) {
                return new TransactionDetail(
                        template.getAmount(),
                        template.getTargetAccountNumber(),
                        template.getTargetBankCode(),
                        LocalDate.now(),
                        template.getKs(),
                        template.getVs(),
                        template.getSs(),
                        template.getMessage()
                );
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
