package cz.seda.spring.service;

import cz.seda.spring.exception.TransactionException;
import cz.seda.spring.model.Account;
import cz.seda.spring.model.StandingTransactionSettings;
import cz.seda.spring.model.TransactionDetail;
import cz.seda.spring.model.TransactionQueue;
import cz.seda.spring.repository.StandingTransactionSettingsRepository;
import cz.seda.spring.repository.TransactionDetailRepository;
import cz.seda.spring.repository.TransactionQueueRepository;
import cz.seda.spring.service.interfaces.IAccountService;
import cz.seda.spring.service.interfaces.ITransactionService;
import cz.seda.spring.util.Paginator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Service("transactionService")
public class TransactionService implements ITransactionService {
    private TransactionDetailRepository transactionDetailRepository;
    private TransactionQueueRepository transactionQueueRepository;
    private IAccountService accountService;
    private StandingTransactionSettingsRepository standingTransactionSettingsRepository;

    @Autowired
    public TransactionService(
            TransactionDetailRepository transactionDetailRepository,
            TransactionQueueRepository transactionQueueRepository,
            AccountService accountService,
            StandingTransactionSettingsRepository standingTransactionSettingsRepository
    ) {
        this.transactionDetailRepository = transactionDetailRepository;
        this.transactionQueueRepository = transactionQueueRepository;
        this.accountService = accountService;
        this.standingTransactionSettingsRepository = standingTransactionSettingsRepository;
    }

    @Override
    public void saveNewOrder(Account account, TransactionDetail transactionDetail)
    {
        if(account.getAvailableBalance() < transactionDetail.getAmount())
        {
            throw new TransactionException("Pro zadání příkazu nemáte dostatečný zůstatek na účtě.");
        }
        if(transactionDetail.getAmount() <= 0) {
            throw new TransactionException("Částka úhrady musí být kladná");
        }

        transactionDetail.setAccount(account);
        transactionDetail.setSenderAccountNumber(account.getAccountNumber());
        transactionDetail.setSenderBankCode(accountService.getBankCode());
        if(transactionDetail.getTargetBankCode().equals(accountService.getBankCode())) {
            Account targetAccount = accountService.findAccountByAccountNumber(transactionDetail.getTargetAccountNumber());
            if (targetAccount != null) {
                transactionDetail.setTargetAccount(targetAccount);
            }
        }
        LocalDateTime processDateTime = randomProcessDateTime(transactionDetail);
        transactionDetail = transactionDetailRepository.save(transactionDetail);

        TransactionQueue transactionQueue = new TransactionQueue(transactionDetail, processDateTime, null);
        transactionQueueRepository.save(transactionQueue);
        account.setAvailableBalance(account.getAvailableBalance() - transactionDetail.getAmount());
        accountService.save(account);
    }

    @Override
    public void saveNewStandingTransaction(
            Account account,
            TransactionDetail transactionDetail,
            StandingTransactionSettings standingTransactionSettings
    ) {
        if(account.getAvailableBalance() < transactionDetail.getAmount())
        {
            throw new TransactionException("Pro zadání příkazu nemáte dostatečný zůstatek na účtě.");
        }
        if(transactionDetail.getAmount() <= 0) {
            throw new TransactionException("Částka úhrady musí být kladná");
        }

        transactionDetail.setAccount(account);
        transactionDetail.setSenderAccountNumber(account.getAccountNumber());
        transactionDetail.setSenderBankCode(accountService.getBankCode());
        if(transactionDetail.getTargetBankCode().equals(accountService.getBankCode())) {
            Account targetAccount = accountService.findAccountByAccountNumber(transactionDetail.getTargetAccountNumber());
            if (targetAccount != null) {
                transactionDetail.setTargetAccount(targetAccount);
            }
        }

        standingTransactionSettings.setActive(true);
        standingTransactionSettings.setPaidCount(0);
        validateStandingOrderSettingsDate(standingTransactionSettings);

        StandingTransactionSettings settings = standingTransactionSettingsRepository.save(standingTransactionSettings);
        transactionDetail.setStandingTransactionSettings(settings);
        transactionDetailRepository.save(transactionDetail);
        addNewStandingTransactionPaymentQueue(account, transactionDetail);
    }

    /**
     * Validates if inserted end date allow for at least 2 repeats of transaction
     * based on transaction repeat frequency
     * @param standingTransactionSettings StandingTransactionSettings entity
     * @throws TransactionException
     */
    private void validateStandingOrderSettingsDate(StandingTransactionSettings standingTransactionSettings) throws TransactionException {
        if(standingTransactionSettings.getEndDate() != null) {
            switch (standingTransactionSettings.getFrequency()){
                case min:
                    if(standingTransactionSettings.getEndDate().isBefore(standingTransactionSettings.getStartDate()))
                    {
                        throw new TransactionException("Datum ukončení musí být po datu první platby");
                    }
                    break;
                case day:
                    if(standingTransactionSettings.getEndDate().isBefore(standingTransactionSettings.getStartDate().plusDays(1)))
                    {
                        throw new TransactionException("Datum ukončení musí být nejméně následující den po datu první platby");
                    }
                    break;
                case week:
                    if(standingTransactionSettings.getEndDate().isBefore(standingTransactionSettings.getStartDate().plusWeeks(1)))
                    {
                        throw new TransactionException("Datum ukončení musí být nejméně týden po datu první platby");
                    }
                    break;
                case month:
                    if(standingTransactionSettings.getEndDate().isBefore(standingTransactionSettings.getStartDate().plusMonths(1)))
                    {
                        throw new TransactionException("Datum ukončení musí být nejméně měsíc po datu první platby");
                    }
                    break;
            }
        }
    }

    @Override
    public void addNewStandingTransactionPaymentQueue(Account account, TransactionDetail transactionDetail){
        StandingTransactionSettings settings = transactionDetail.getStandingTransactionSettings();
        if(account.getAvailableBalance() < transactionDetail.getAmount())
        {
            settings.setActive(false);
            standingTransactionSettingsRepository.save(settings);
            throw new TransactionException("Pro zadání příkazu nemáte dostatečný zůstatek na účtě.");
        }
        if(settings.getRepeats() != null) {
            if(settings.getPaidCount() >= settings.getRepeats()){
                settings.setActive(false);
                standingTransactionSettingsRepository.save(settings);
                return;
            }
        }
        LocalDateTime baseTime = settings.getLastTransaction() == null ? LocalDateTime.of(settings.getStartDate(), LocalTime.now()) : settings.getLastTransaction();
        LocalDateTime processTime = baseTime;
        if(settings.getLastTransaction() != null) {
            switch (settings.getFrequency()) {
                case min:
                    processTime = baseTime.plusMinutes(1);
                    break;
                case day:
                    processTime = baseTime.plusDays(1);
                    break;
                case week:
                    processTime = baseTime.plusWeeks(1);
                    break;
                case month:
                    processTime = baseTime.plusMonths(1);
                    break;
            }
        }
        if(settings.getEndDate() != null) {
            if(processTime.isAfter(LocalDateTime.of(settings.getEndDate(),LocalTime.of(23,59))))
            {
                settings.setActive(false);
                standingTransactionSettingsRepository.save(settings);
                return;
            }
        }
        TransactionQueue queue = new TransactionQueue(transactionDetail, processTime, null);
        transactionQueueRepository.save(queue);
        settings.setLastTransaction(processTime);
        settings.setPaidCount(settings.getPaidCount()+1);
        standingTransactionSettingsRepository.save(settings);
    }

    @Override
    public void editStandingTransaction(Account account, TransactionDetail transactionDetail, StandingTransactionSettings standingTransactionSettings)
    {
        if(account.getAvailableBalance() < transactionDetail.getAmount())
        {
            throw new TransactionException("Pro zadání příkazu nemáte dostatečný zůstatek na účtě.");
        }
        if(transactionDetail.getAmount() <= 0) {
            throw new TransactionException("Částka úhrady musí být kladná");
        }

        TransactionDetail oldTransaction = getTransactionDetail(account, transactionDetail.getId());
        if(oldTransaction == null) {
            throw new TransactionException("Transakce nenalezena");
        }
        StandingTransactionSettings oldSettings = oldTransaction.getStandingTransactionSettings();

        standingTransactionSettings.setPaidCount(oldSettings.getPaidCount());
        standingTransactionSettings.setId(oldSettings.getId());
        standingTransactionSettings.setActive(oldSettings.isActive());
        boolean updateFirst = false;
        if(standingTransactionSettings.getPaidCount() == 1){
            standingTransactionSettings.setPaidCount(0);
            updateFirst = true;
        }
        else{
            standingTransactionSettings.setLastTransaction(oldSettings.getLastTransaction());
        }
        standingTransactionSettings = standingTransactionSettingsRepository.save(standingTransactionSettings);

        transactionDetail.setAccount(oldTransaction.getAccount());
        transactionDetail.setSenderAccountNumber(oldTransaction.getSenderAccountNumber());
        transactionDetail.setSenderBankCode(oldTransaction.getSenderBankCode());
        transactionDetail.setAccount(account);
        transactionDetail.setTargetAccount(oldTransaction.getTargetAccount());
        transactionDetail.setStandingTransactionSettings(standingTransactionSettings);
        transactionDetailRepository.save(transactionDetail);

        if(updateFirst){
            TransactionQueue transactionQueue = transactionQueueRepository.findByTransactionDetailAndProcessedTimeIsNull(transactionDetail);
            transactionQueueRepository.delete(transactionQueue);
            addNewStandingTransactionPaymentQueue(account,transactionDetail);
        }
    }

    @Override
    public List<TransactionDetail> getStandingOrders(Account account, Paginator paginator)
    {
        return transactionDetailRepository.findAllStandingTransactions(account, paginator);
    }

    @Override
    public Integer getStandingOrdersCount(Account account)
    {
        return transactionDetailRepository.countStandingTransactions(account);
    }

    @Override
    public List<TransactionQueue> getTransactionHistory(Account account, Pageable pageable)
    {
        List<TransactionQueue> transactionQueueList = transactionQueueRepository.findAllByAccount(account, pageable);
        for (TransactionQueue transactionQueue : transactionQueueList)
        {
            if(transactionQueue.getTransactionDetail().getAccount() == account)
            {
                transactionQueue.setOutgoing(true);
            }
        }
        return transactionQueueList;
    }

    @Override
    public Integer getTransactionHistoryCount(Account account)
    {
        return transactionQueueRepository.getCountByAccount(account);
    }

    @Override
    public TransactionQueue getTransactionQueueRecord(Integer id, Account account)
    {
        TransactionQueue transactionQueue = transactionQueueRepository.findById(id).orElse(null);
        if(transactionQueue != null) {
            if (transactionQueue.getTransactionDetail().getAccount() == account) {
                transactionQueue.setOutgoing(true);
            } else if (transactionQueue.getTransactionDetail().getTargetAccount() != account) {
                transactionQueue = null;
            }
            return transactionQueue;
        } else {
            return null;
        }
    }

    @Override
    public List<TransactionQueue> getWaitingOrders(Account account, Paginator paginator)
    {
        return transactionQueueRepository.findWaitingOrders(account, paginator);
    }

    @Override
    public TransactionDetail getTransactionDetail(Account account, Integer id)
    {
        TransactionDetail transactionDetail = transactionDetailRepository.findById(id).orElse(null);
        if(transactionDetail != null) {
            if (transactionDetail.getAccount() == account) {
                return transactionDetail;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public TransactionDetail cancelStandingTransaction(Account account, Integer id)
    {
        TransactionDetail transactionDetail = transactionDetailRepository.findById(id).orElse(null);
        if(transactionDetail != null) {
            if (transactionDetail.getAccount() == account) {
                transactionDetail.getStandingTransactionSettings().setActive(false);
                standingTransactionSettingsRepository.save(transactionDetail.getStandingTransactionSettings());
                TransactionQueue unprocessedPayment = transactionQueueRepository.findByTransactionDetailAndProcessedTimeIsNull(transactionDetail);
                if (unprocessedPayment != null) {
                    transactionQueueRepository.delete(unprocessedPayment);
                }
                return transactionDetail;
            } else {
                return null;
            }
        }
        else {
            return null;
        }
    }

    @Override
    public List<TransactionQueue> getAllStandingTransactionProcessedPayments(TransactionDetail transactionDetail)
    {
        return transactionQueueRepository.findAllByTransactionDetailAndProcessedTimeIsNotNullOrderByProcessedTimeDesc(transactionDetail);
    }

    /**
     * Generates random time stamp between current datetime and transactions due date(time)
     * @param transactionDetail TransactionDetail entity
     * @return generated time stamp
     */
    private LocalDateTime randomProcessDateTime(TransactionDetail transactionDetail) {
        if(transactionDetail.getDueTime() != null)
        {
            if (transactionDetail.getDueTime().isEmpty()) {
                transactionDetail.setDueTime("23:59");
            }
        }
        else
        {
            transactionDetail.setDueTime("23:59");
        }
        LocalDateTime to = LocalDateTime.of(transactionDetail.getDueDate(), LocalTime.parse(transactionDetail.getDueTime(), DateTimeFormatter.ISO_TIME));
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime greaterThanNow = now.plusMinutes(3);
        if(to.isBefore(greaterThanNow)){

            to = LocalDateTime.of(greaterThanNow.toLocalDate(), greaterThanNow.toLocalTime());
        }
        long epochNow = now.toEpochSecond(ZoneOffset.UTC);
        long epochTo = to.toEpochSecond(ZoneOffset.UTC);
        long randomEpochSeconds = ThreadLocalRandom.current().nextLong(epochNow, epochTo);
        LocalDateTime assignedProccessDateTime = LocalDateTime.ofEpochSecond(randomEpochSeconds, 0, ZoneOffset.UTC);
        return assignedProccessDateTime;
    }
}
