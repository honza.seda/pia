package cz.seda.spring.service;

import cz.seda.spring.model.BankCode;
import cz.seda.spring.repository.BankCodeRepository;
import cz.seda.spring.service.interfaces.IBankCodeService;
import cz.seda.spring.util.Paginator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.List;

@Service("bankCodeService")
public class BankCodeService implements IBankCodeService {
    private BankCodeRepository bankCodeRepository;

    @Value("${pia.settings.bankCodeSource.url}")
    private String bankCodeSourceUrl;
    @Value("${pia.settings.bankCodeSource.separator}")
    private String bankCodeSourceSeparator;
    @Value("${pia.settings.bankCodeSource.encoding}")
    private String bankCodeSourceEncoding;

    @Autowired
    public BankCodeService(BankCodeRepository bankCodeRepository) {
        this.bankCodeRepository = bankCodeRepository;
    }

    @Override
    public List<BankCode> findAll(Paginator paginator){
        return bankCodeRepository.findAllBy(paginator);
    }

    @Override
    public Integer countAll()
    {
        return bankCodeRepository.countAllBy();
    }

    @Override
    public boolean updateBankCodesFromExternalSource()
    {
        try {
            URL url = new URL(bankCodeSourceUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            int status = con.getResponseCode();

            if (status == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = con.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(inputStream, Charset.forName(bankCodeSourceEncoding)));
                br.readLine(); //skip header
                String line;
                while ((line = br.readLine()) != null) {
                    String[] bank = line.split(bankCodeSourceSeparator);
                    String name = new String(bank[1].getBytes(bankCodeSourceEncoding), bankCodeSourceEncoding);
                    bankCodeRepository.save(
                            new BankCode(
                                    bank[0],
                                    name,
                                    bank[2]
                            )
                    );
                }
                inputStream.close();
                br.close();
                return true;
            }
            else {
                return false;
            }
        } catch (IOException e) {
            return false;
        }

    }
}
