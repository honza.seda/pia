package cz.seda.spring.service;

import cz.seda.spring.model.User;
import cz.seda.spring.repository.UserRepository;
import cz.seda.spring.service.interfaces.IAccountService;
import cz.seda.spring.service.interfaces.IMailService;
import cz.seda.spring.service.interfaces.IUserService;
import cz.seda.spring.util.Paginator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.Normalizer;
import java.util.List;
import java.util.Random;

@Service("userService")
public class UserService implements IUserService {
    private UserRepository userRepository;
    private IAccountService accountService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private IMailService mailService;

    @Autowired
    public UserService(
            UserRepository userRepository,
            BCryptPasswordEncoder bCryptPasswordEncoder,
            AccountService accountService,
            MailService mailService
    ) {
        this.userRepository = userRepository;
        this.accountService = accountService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.mailService = mailService;
    }

    @Override
    public User findUserByLogin(String login)
    {
        return userRepository.findUserByLogin(login);
    }

    @Override
    public User findUserById(Integer id)
    {
        return userRepository.findUserById(id);
    }

    @Override
    public User saveNewUser(User user)
    {
        user.setRole("ROLE_USER");
        String password = generateCredentials(user);
        User savedUser = saveUser(user);
        if(savedUser == null)
        {
            return null;
        }
        if(accountService.createAccountAndCardForUser(user) == null){
            return null;
        }
        mailService.sendRegistrationMail(savedUser, password);
        return savedUser;
    }

    @Override
    public User saveUser(User user)
    {
        return userRepository.save(user);
    }

    @Override
    public User deleteUser(Integer userId)
    {
        User user = findUserById(userId);
        if (user == null)
        {
            return null;
        }
        user.setDeleted(true);
        return userRepository.save(user);
    }

    @Override
    public User editUser(User user)
    {
        User oldUser = userRepository.findUserById(user.getId());
        if(oldUser == null) {
            return null;
        }
        user.setRole(oldUser.getRole());
        user.setLogin(oldUser.getLogin());
        user.setPassword(oldUser.getPassword());

        return userRepository.save(user);
    }

    @Override
    public List<User> getAllBankUsers(Paginator paginator) {
        return userRepository.findAllByRoleEqualsAndDeleted("ROLE_USER", false, paginator);
    }

    @Override
    public Integer getUserCount() {
        return userRepository.countByRoleEqualsAndDeleted("ROLE_USER", false);
    }

    /**
     * Generates login credentials
     * Login: first two characters of first name + first two characters of last name + random four digit number (including leading zeros)
     * Password: random four digit number (including leading zeros)
     * @param user
     */
    private String generateCredentials(User user)
    {
        Random r = new Random();
        String generatedUsername;
        String generatedPassword;

        do {
            int randomLoginSeed = r.nextInt(9999);
            generatedUsername = user.getFirstName().substring(0, 2)
                            + user.getLastName().substring(0, 2)
                            + String.format("%04d", randomLoginSeed);

            // Strip generated username of accented characters
            generatedUsername = Normalizer.normalize(generatedUsername, Normalizer.Form.NFD).replaceAll("[^\\p{ASCII}]", "");
            generatedUsername = generatedUsername.toLowerCase();
        } while (findUserByLogin(generatedUsername) != null);

        int randomPasswordSeed = r.nextInt(9999);
        generatedPassword = String.format("%04d", randomPasswordSeed);

        user.setLogin(generatedUsername);
        user.setPassword(bCryptPasswordEncoder.encode(generatedPassword));
        return generatedPassword;
    }
}
