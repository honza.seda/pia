package cz.seda.spring.service;

import cz.seda.spring.model.Account;
import cz.seda.spring.model.Card;
import cz.seda.spring.model.User;
import cz.seda.spring.repository.AccountRepository;
import cz.seda.spring.repository.CardRepository;
import cz.seda.spring.service.interfaces.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Random;

@Service("accountService")
public class AccountService implements IAccountService {
    private AccountRepository accountRepository;
    private CardRepository cardRepository;

    @Value("${pia.settings.bankCode}")
    String bankCode;

    @Autowired
    public AccountService(AccountRepository accountRepository, CardRepository cardRepository) {
        this.accountRepository = accountRepository;
        this.cardRepository = cardRepository;
    }

    public String getBankCode(){
        return bankCode;
    }

    public boolean save(Account account){
        return accountRepository.save(account) != null;
    }

    @Override
    public Account createAccountAndCardForUser(User user){
        Random r = new Random();
        StringBuilder generatedAccountNumber = new StringBuilder();
        StringBuilder generatedCardNumber = new StringBuilder();

        //generate account
        for (int i = 0; i < 10; i++)
        {
            int rangeModifier = i == 0 ? 1 : 0;
            generatedAccountNumber.append((r.nextInt(10 - rangeModifier) + rangeModifier));
        }

        Account account = new Account(
                user,
                0.0,
                0.0,
                generatedAccountNumber.toString(),
                "CZK",
                true
        );
        account = accountRepository.save(account);

        //generate card
        LocalDate now = LocalDate.now();
        for (int i = 0; i < 16; i++)
        {
            int rangeModifier = i == 0 ? 1 : 0;
            generatedCardNumber.append((r.nextInt(10 - rangeModifier) + rangeModifier));
        }
        Integer generatedPin = r.nextInt(9999);
        Card card = new Card(
                generatedCardNumber.toString(),
                now.getMonthValue(),
                now.plusYears(5).getYear(),
                account,
                generatedPin
        );
        cardRepository.save(card);

        return account;
    }

    @Override
    public Card getAccountCard(Account account) {
        return cardRepository.findByAccount(account);
    }

    @Override
    public Account findAccountByAccountNumber(String accountNumber)
    {
        return accountRepository.findByAccountNumber(accountNumber);
    }

    @Override
    public Account findAccountByUser(User user)
    {
        return accountRepository.findAccountByUser(user);
    }

    @Override
    public void updateUserSettings(Account account, Account accountSettings, Card cardSettings)
    {
        account.setSendNotifications(accountSettings.isSendNotifications());
        save(account);
        Card card = cardRepository.findByAccount(account);
        card.setInternationalPayments(cardSettings.isInternationalPayments());
        cardRepository.save(card);
    }
}
