package cz.seda.spring.service;

import cz.seda.spring.model.User;
import cz.seda.spring.security.CustomUserPrincipal;
import cz.seda.spring.service.interfaces.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private IUserService userService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user = userService.findUserByLogin(s);
        if (user == null) {
            throw new UsernameNotFoundException(s);
        }
        return new CustomUserPrincipal(user);
    }
}
