package cz.seda.spring.service.interfaces;

/**
 * Captcha server validation
 */
public interface ICaptchaService {

    /**
     * Validates captcha response.
     * @param response JSON response
     */
    void processResponse(final String response);

    /**
     * Retrieves captcha site key.
     * @return String key
     */
    String getReCaptchaSite();

    /**
     * Retrieves captcha secret key.
     * @return String key
     */
    String getReCaptchaSecret();
}
