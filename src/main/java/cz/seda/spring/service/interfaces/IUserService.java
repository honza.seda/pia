package cz.seda.spring.service.interfaces;

import cz.seda.spring.model.User;
import cz.seda.spring.util.Paginator;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface IUserService {

    /**
     * Finds user in repository by login
     * @param login String login
     * @return User entity
     */
    User findUserByLogin(String login);

    /**
     * Finds user in repository by id
     * @param id Integer id
     * @return User entity
     */
    User findUserById(Integer id);

    /**
     * Saves new user (front end) into database. Encrypts user password and generates unique login.
     * Creates new Account and Card for user.
     * @param user User entity
     * @return boolean of result
     */
    User saveNewUser(User user);

    /**
     * Calls repository function to save user into database.
     * @param user User entity
     * @return saved User entity
     */
    User saveUser(User user);

    /**
     * Deletes user by setting the 'deleted' flag of user to true.
     * @param userId Integer of user id
     * @return updated User entity
     */
    User deleteUser(Integer userId);

    /**
     * Updates User entity
     * @param user User entity
     */
    User editUser(User user);

    /**
     * Retrieves all front-end users from repository
     * @param paginator Pageable for query paging
     * @return List of retrieved users
     */
    List<User> getAllBankUsers(Paginator paginator);

    /**
     * Retrieves count of all front-end users from repository
     * @return Integer count
     */
    Integer getUserCount();
}
