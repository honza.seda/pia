package cz.seda.spring.service.interfaces;

import cz.seda.spring.model.TransactionDetail;
import cz.seda.spring.model.User;

public interface IMailService {

    /**
     * Sends email to specified recipient.
     * @param recipient recipients email address as string
     * @param subject email subject string
     * @param content content html as string
     * @return boolean result of send operation
     */
    boolean prepareAndSend(String recipient, String subject, String content);

    /**
     * Retrieves email string representing mail sender.
     * @return String mail address
     */
    String getFromMail();

    /**
     * Returns email html template file from resources as string.
     * @param templateName name of template file
     * @return string containing html of template
     */
    String loadEmailTemplate(String templateName);

    /**
     * Sends email with registered user inforamtion
     * @param user registered user entity
     * @param password non-encoded password
     * @return prepareAndSend() return
     */
    boolean sendRegistrationMail(User user, String password);

    /**
     * Sends notification about payment being processed in queue
     * @param recipients String of recipients (delimited by ',')
     * @param transactionDetail processed transaction detail
     * @return prepareAndSend() return
     */
    boolean sendPaymentProcessedInfo(String recipients, TransactionDetail transactionDetail);

    /**
     * Sends updated user info on edit by admin
     * @param user updated User entity
     * @return prepareAndSend() return
     */
    boolean sendEditedUser(User user);

    /**
     * Sends notification about deleted user
     * @param mail recipient email
     * @return prepareAndSend() return
     */
    boolean sendDeletedUser(String mail);

    /**
     * Sends notification of standing transaction cancellation due to low account balance
     * @param mail recipient mail
     * @param standingTransactionName standing transaction name string
     * @return prepareAndSend() return
     */
    boolean sendStandingTransactionLowBalance(String mail, String standingTransactionName);
}
