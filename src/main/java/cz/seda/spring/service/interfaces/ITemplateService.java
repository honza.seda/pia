package cz.seda.spring.service.interfaces;

import cz.seda.spring.model.Account;
import cz.seda.spring.model.TransactionDetail;

public interface ITemplateService {

    /**
     * Maps a TransactionTemplate entity onto a TransactionDetail entity.
     * @param templateId id of template
     * @param account account entity to which template is assigned
     * @return TransactionDetail entity
     */
    TransactionDetail mapTemplateByIdToDetail(Integer templateId, Account account);
}
