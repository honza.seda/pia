package cz.seda.spring.service.interfaces;

import cz.seda.spring.model.BankCode;
import cz.seda.spring.util.Paginator;

import java.util.List;

public interface IBankCodeService {

    /**
     * Finds all bank codes from repository.
     * @param paginator Pageable for query paging
     * @return List of BankCode entities
     */
    List<BankCode> findAll(Paginator paginator);

    /**
     * Returns count of all bank codes in repository.
     * @return Integer count
     */
    Integer countAll();

    /**
     * Updates bank codes in repository from an external source.
     * Source URL is specified in application.properties under pia.settings.bankCodeSource.url.
     * @return boolean result of update
     */
    boolean updateBankCodesFromExternalSource();
}
