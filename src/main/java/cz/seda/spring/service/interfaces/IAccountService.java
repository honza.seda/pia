package cz.seda.spring.service.interfaces;

import cz.seda.spring.model.Account;
import cz.seda.spring.model.Card;
import cz.seda.spring.model.User;
import org.springframework.transaction.annotation.Transactional;

@Transactional
public interface IAccountService {

    /**
     * Returns bank code string saved in config
     * @return String code
     */
    String getBankCode();

    /**
     * Saves Account entity into repository.
     * @param account Account entity
     * @return boolean of saving result
     */
    boolean save(Account account);

    /**
     * Generates new account for user and saves it into repository.
     * @param user User entity
     * @return generated Account entity
     */
    Account createAccountAndCardForUser(User user);

    /**
     * Retrieves card info from repository assigned to specified account.
     * @param account Account entity
     * @return Card entity
     */
    Card getAccountCard(Account account);

    /**
     * Retrieves account from repository by account number.
     * @param accountNumber String with account number
     * @return Account entity
     */
    Account findAccountByAccountNumber(String accountNumber);

    /**
     * Retrieves account from repository by user info.
     * @param user User entity
     * @return Account entity
     */
    Account findAccountByUser(User user);

    /**
     * Updates account settings from form
     * @param account Account entity to be updated
     * @param accountSettings Account entity with changed settings
     * @param cardSettings Card entity with changed settings
     */
    void updateUserSettings(Account account, Account accountSettings, Card cardSettings);
}
