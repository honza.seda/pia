package cz.seda.spring.service.interfaces;

import cz.seda.spring.model.Account;
import cz.seda.spring.model.StandingTransactionSettings;
import cz.seda.spring.model.TransactionDetail;
import cz.seda.spring.model.TransactionQueue;
import cz.seda.spring.util.Paginator;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface ITransactionService {

    /**
     * Saves new transaction values into database.
     * Maps account numbers to Account entity if account number exists in database.
     * Inserts transaction into queue TransactionQueue to be processed later.
     * @param account Account entity where transaction has been created
     * @param transactionDetail TransactionDetail entity
     */
    void saveNewOrder(Account account, TransactionDetail transactionDetail);

    /**
     * Saves new standing transaction values into database.
     * Maps account numbers to Account entity if account number exists in database.
     * Inserts first repeating of transaction into queue TransactionQueue.
     * @param account Account entity where transaction has been created
     * @param transactionDetail TransactionDetail entity
     * @param standingTransactionSettings StandingTransactionSettings entity
     */
    void saveNewStandingTransaction(Account account, TransactionDetail transactionDetail, StandingTransactionSettings standingTransactionSettings);

    /**
     * Computes date of next repeat and inserts a new queue record.
     * @param account Account entity
     * @param transactionDetail TransactionDetail entity
     */
    void addNewStandingTransactionPaymentQueue(Account account, TransactionDetail transactionDetail);

    /**
     * Updates TransactionDetail entity and StandingTransactionSettings entity of existing standing transaction.
     * If first repeat of standing transaction has not yet occured,
     * it is removed from queue and replaced with new record computed from updated objects.
     * @param account Account entity of account where transaction has been created
     * @param transactionDetail TransactionDetail entity
     * @param standingTransactionSettings StandingTransactionSettings entity
     */
    void editStandingTransaction(Account account, TransactionDetail transactionDetail, StandingTransactionSettings standingTransactionSettings);

    /**
     * Retrieves all standing transactions created on account.
     * @param account Account entity
     * @param paginator Pageable for query paging
     * @return List of TransactionDetail entities
     */
    List<TransactionDetail> getStandingOrders(Account account, Paginator paginator);

    /**
     * Retrieves count of all standing transactions created on account.
     * @param account Account entity
     * @return Integer count of transactions
     */
    Integer getStandingOrdersCount(Account account);

    /**
     * Retrieves list of all processed transactions on account.
     * @param account Account entity
     * @param pageable Pageable for query paging
     * @return List of TransactionDetail entities
     */
    List<TransactionQueue> getTransactionHistory(Account account, Pageable pageable);

    /**
     * Retrieves count of all processed transactions on account.
     * @param account Account entity
     * @return Integer count of transactions
     */
    Integer getTransactionHistoryCount(Account account);

    /**
     * Finds a single queue record of TransactionDetail by id.
     * @param id transactionDetail id
     * @param account Account entity
     * @return TransactionQueue entity
     */
    TransactionQueue getTransactionQueueRecord(Integer id, Account account);

    /**
     * Retrieves all queue records of TransactionDetail on account that are not yet processed.
     * @param account Account entity
     * @param paginator Pageable for query paging
     * @return List of TransactionQueue entities
     */
    List<TransactionQueue> getWaitingOrders(Account account, Paginator paginator);

    /**
     * Retrieves TransactionDetail of single transaction specified by id.
     * @param account Account entity on which transaction has been created
     * @param id id of transaction
     * @return TransactionDetail entity
     */
    TransactionDetail getTransactionDetail(Account account, Integer id);

    /**
     * Cancels specified standing transaction by updating the 'active' flag. Removes transaction from queue.
     * @param account Account entity on which transaction has been created
     * @param id id of transaction
     * @return updated TransactionDetail entity
     */
    TransactionDetail cancelStandingTransaction(Account account, Integer id);

    /**
     * Gets all transactions of standing transaction from queue that have been already processed.
     * @param transactionDetail TransactionDetail entity of standing transaction
     * @return List of TransactionQueue entities
     */
    List<TransactionQueue> getAllStandingTransactionProcessedPayments(TransactionDetail transactionDetail);


}
