package cz.seda.spring.service;

import com.google.common.base.Charsets;
import cz.seda.spring.model.TransactionDetail;
import cz.seda.spring.model.User;
import cz.seda.spring.service.interfaces.IMailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import javax.mail.internet.InternetAddress;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

@Service
public class MailService implements IMailService {
    private JavaMailSender mailSender;

    @Value("${spring.mail.properties.from}")
    private String fromMail;

    @Autowired
    public MailService(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    @Override
    public boolean prepareAndSend(String recipient, String subject, String content) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom(getFromMail());
            messageHelper.setTo(InternetAddress.parse(recipient));
            messageHelper.setSubject(subject);
            String htmlContent = loadEmailTemplate("headerTemplate");
            htmlContent += content;
            htmlContent += loadEmailTemplate("footerTemplate");
            messageHelper.setText(htmlContent, true);
        };
        try {
            mailSender.send(messagePreparator);
            return true;
        } catch (MailException e) {
            return false;
        }
    }

    @Override
    public String loadEmailTemplate(String templateName) {
        StringBuilder stringBuilder = new StringBuilder();
        String line = null;

        try (BufferedReader bufferedReader = new BufferedReader(
                new InputStreamReader(
                        new ClassPathResource("email/" + templateName + ".html").getInputStream(),
                        Charsets.UTF_8
                )
        )) {
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return stringBuilder.toString();
    }

    @Override
    public String getFromMail() {
        return this.fromMail;
    }

    @Override
    public boolean sendRegistrationMail(User user, String password){
        if(user == null) {
            return false;
        }
        String htmlTemplate = loadEmailTemplate("contentTemplate/newRegistration");
        String content = String.format(
                htmlTemplate,
                user.getLogin(),
                password);
        return prepareAndSend(user.getEmail(), "Registrace konta - online bankovnictví GreyBank", content);
    }

    @Override
    public boolean sendPaymentProcessedInfo(String recipients, TransactionDetail transactionDetail) {
        String htmlTemplate = loadEmailTemplate("contentTemplate/paymentProcessedInfo");
        String content = String.format(
                htmlTemplate,
                transactionDetail.getSenderAccountNumber(),
                transactionDetail.getSenderBankCode(),
                transactionDetail.getTargetAccountNumber(),
                transactionDetail.getTargetBankCode(),
                transactionDetail.getAmount(),
                transactionDetail.getVs() == null ? 0 : transactionDetail.getVs(),
                transactionDetail.getKs() == null ? 0 : transactionDetail.getKs(),
                transactionDetail.getSs() == null ? 0 : transactionDetail.getSs(),
                transactionDetail.getMessage());
        return prepareAndSend(recipients, "Oznámení o platbách - online bankovnictví GreyBank", content);
    }

    @Override
    public boolean sendEditedUser(User user) {
        if (user == null) {
            return false;
        }
        String htmlTemplate = loadEmailTemplate("contentTemplate/userEdit");
        String content = String.format(
                htmlTemplate,
                user.getFirstName(),
                user.getLastName(),
                user.getPin(),
                user.getStreet(),
                user.getHouseNumber(),
                user.getCity(),
                user.getZip(),
                user.getCountry().getLabelName(),
                user.getEmail(),
                user.getPhone());
        return prepareAndSend(user.getEmail(), "Úprava osobních údajů - online bankovnictví GreyBank", content);
    }

    @Override
    public boolean sendDeletedUser(String mail) {
        String htmlTemplate = loadEmailTemplate("contentTemplate/userDelete");
        return prepareAndSend(mail, "Deaktivace konta - online bankovnictví GreyBank", htmlTemplate);
    }

    @Override
    public boolean sendStandingTransactionLowBalance(String mail, String standingTransactionName) {
        String htmlTemplate = loadEmailTemplate("contentTemplate/standingTransactionLowBalance");
        String content = String.format(
                htmlTemplate,
                standingTransactionName);
        return prepareAndSend(mail, "Nízký zůstatek pro trvalý příkaz - online bankovnictví GreyBank", content);

    }
}