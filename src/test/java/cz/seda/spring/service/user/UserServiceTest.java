package cz.seda.spring.service.user;

import cz.seda.spring.model.Account;
import cz.seda.spring.model.User;
import cz.seda.spring.repository.UserRepository;
import cz.seda.spring.service.AccountService;
import cz.seda.spring.service.MailService;
import cz.seda.spring.service.UserService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(SpringRunner.class)
public class UserServiceTest {
    private UserService userService;

    private UserRepository userRepository = Mockito.mock(UserRepository.class);
    private AccountService accountService = Mockito.mock(AccountService.class);
    private BCryptPasswordEncoder bCryptPasswordEncoder = Mockito.mock(BCryptPasswordEncoder.class);
    private MailService mailService = Mockito.mock(MailService.class);


    @Before
    public void setUp() {
        userService = new UserService(userRepository, bCryptPasswordEncoder, accountService, mailService);
    }

    @After
    public void tearDown() throws Exception {

    }

    /**
     * successful save
     */
    @Test
    public void saveUser_shouldPass() {
        User user = new User();
        user.setFirstName("jmeno");
        user.setLastName("prijmeni");
        user.setPin("1234567890");
        user.setEmail("asdf@as.df");

        Mockito.when(userRepository.save(user)).thenReturn(user);

        User result = userService.saveUser(user);
        Assert.assertNotNull(result);
    }

    /**
     * Passed user missing some required parameters
     */
    @Test
    public void saveUser_shouldFail() {
        User user = new User();
        user.setFirstName("jmeno");
        user.setLastName("prijmeni");
        user.setPin("1234567890");

        Mockito.when(userRepository.save(user)).thenReturn(null);

        User result = userService.saveUser(user);
        Assert.assertNull(result);
    }

    /**
     * existing login
     */
    @Test
    public void findUserByLogin_shouldPass() {
        String login = "User0001";
        User shouldReturnUser = new User();
        shouldReturnUser.setLogin("User0001");

        Mockito.when(userRepository.findUserByLogin(login)).thenReturn(shouldReturnUser);

        User result = userService.findUserByLogin(login);
        Assert.assertNotNull(result);
        Assert.assertEquals(shouldReturnUser, result);
    }

    /**
     * Non-existing login
     */
    @Test
    public void findUserByLogin_shouldFail() {
        String login = "User1";

        Mockito.when(userRepository.findUserByLogin(login)).thenReturn(null);

        User result = userService.findUserByLogin(login);
        Assert.assertNull(result);
    }

    /**
     * successful save of new user and generation of user credentials
     */
    @Test
    public void saveNewUser_shouldPass() {
        User userToSave = new User();
        userToSave.setFirstName("jmeno");
        userToSave.setLastName("prijmeni");
        userToSave.setPin("1234567890");
        userToSave.setEmail("asdf@as.df");

        Mockito.when(userRepository.save(any())).thenAnswer(i -> i.getArguments()[0]);
        Mockito.when(accountService.createAccountAndCardForUser(userToSave)).thenReturn(new Account());
        Mockito.when(mailService.sendRegistrationMail(userToSave, "1234")).thenReturn(true);
        Mockito.when(bCryptPasswordEncoder.encode(anyString())).thenAnswer(i -> i.getArguments()[0]);

        User result = userService.saveNewUser(userToSave);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getPassword());
        Assert.assertNotNull(result.getLogin());
    }

    /**
     * save of new user failed on user insertion
     */
    @Test
    public void saveNewUser_shouldFailOnUserCreation() {
        User userToSave = new User();
        userToSave.setFirstName("jmeno");
        userToSave.setLastName("prijmeni");
        userToSave.setEmail("asdf@as.df");

        Mockito.when(userRepository.save(userToSave)).thenReturn(null);
        Mockito.when(accountService.createAccountAndCardForUser(userToSave)).thenReturn(new Account());
        Mockito.when(mailService.sendRegistrationMail(userToSave, "1234")).thenReturn(true);

        User result = userService.saveNewUser(userToSave);
        Assert.assertNull(result);
    }

    /**
     * save of new user failed on account creation
     */
    @Test
    public void saveNewUser_shouldFailOnAccountCreation() {
        User userToSave = new User();
        userToSave.setFirstName("jmeno");
        userToSave.setLastName("prijmeni");
        userToSave.setPin("1234567890");
        userToSave.setEmail("asdf@as.df");

        Mockito.when(userRepository.save(userToSave)).thenReturn(userToSave);
        Mockito.when(accountService.createAccountAndCardForUser(userToSave)).thenReturn(null);
        Mockito.when(mailService.sendRegistrationMail(userToSave, "1234")).thenReturn(true);

        User result = userService.saveNewUser(userToSave);
        Assert.assertNull(result);
    }

    /**
     * successfull deletion of user
     */
    @Test
    public void deleteUser_shouldPass() {
        int userId = 2;
        User deletedUser = new User();
        deletedUser.setFirstName("jmeno");
        deletedUser.setDeleted(true);

        Mockito.when(userRepository.save(deletedUser)).thenReturn(deletedUser);
        Mockito.when(userRepository.findUserById(userId)).thenReturn(deletedUser);

        User result = userService.deleteUser(userId);
        Assert.assertEquals(deletedUser, result);
        Assert.assertTrue(deletedUser.isDeleted());
    }

    /**
     * deleting user that does not exist
     */
    @Test
    public void deleteUser_shouldFail() {
        int userId = 5;

        Mockito.when(userRepository.findUserById(userId)).thenReturn(null);

        User result = userService.deleteUser(userId);
        Assert.assertNull(result);
    }

    /**
     * successfull edit of user
     */
    @Test
    public void editUser_shouldPass() {
        User findUser = new User();
        findUser.setRole("ROLE_USER");
        findUser.setLogin("login");
        findUser.setPassword("123");

        User editedUser = new User();
        editedUser.setId(2);
        editedUser.setFirstName("jmeno");
        editedUser.setLastName("prijmeni");

        Mockito.when(userRepository.findUserById(editedUser.getId())).thenReturn(findUser);
        Mockito.when(userRepository.save(editedUser)).thenReturn(editedUser);

        User result = userService.editUser(editedUser);
        Assert.assertEquals(editedUser, result);
        Assert.assertNotNull(editedUser.getLogin());
        Assert.assertNotNull(editedUser.getPassword());
    }

    /**
     * editing user that does not exist
     */
    @Test
    public void editUser_shouldFail() {
        User findUser = new User();
        findUser.setId(5);

        Mockito.when(userRepository.findUserById(findUser.getId())).thenReturn(null);

        User result = userService.editUser(findUser);
        Assert.assertNull(result);
    }
}
