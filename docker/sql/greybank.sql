-- MySQL dump 10.13  Distrib 8.0.12, for Win64 (x86_64)
--
-- Host: localhost    Database: greybank
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.36-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `account`
--

DROP TABLE IF EXISTS `account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `account` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `balance` double NOT NULL DEFAULT '0',
  `available_balance` double NOT NULL DEFAULT '0',
  `account_number` varchar(45) NOT NULL,
  `currency` varchar(3) DEFAULT NULL,
  `send_notifications` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `account_user_id_fk_idx` (`user_id`),
  CONSTRAINT `account_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` VALUES
 (1,2,138189.30000000002,138189.30000000002,'4655788890','CZK',1),
 (2,3,32449.200000000004,32449.200000000004,'1071456789','CZK',0);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bank_code`
--

DROP TABLE IF EXISTS `bank_code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `bank_code` (
  `code` varchar(4) NOT NULL,
  `name` varchar(100) NOT NULL,
  `swift` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bank_code`
--

LOCK TABLES `bank_code` WRITE;
/*!40000 ALTER TABLE `bank_code` DISABLE KEYS */;
INSERT INTO `bank_code` VALUES
('0100','Komerční banka, a.s.','KOMBCZPP'),
('0300','Československá obchodní banka, a.s.','CEKOCZPP'),
('0600','MONETA Money Bank, a.s.','AGBACZPP'),
('0710','Česká národní banka','CNBACZPP'),
('0800','Česká spořitelna, a.s.','GIBACZPX'),
('0880','GreyBank','GREYCZPP'),
('2010','Fio banka, a.s.','FIOBCZPP'),
('2700','UniCredit Bank Czech Republic and Slovakia, a.s.','BACXCZPP'),
('3030','Air Bank a.s.','AIRACZPP'),
('3500','ING Bank N.V.','INGBCZPP'),
('5500','Raiffeisenbank a.s.','RZBCCZPP'),
('6100','Equa bank a.s.','EQBKCZPP');
/*!40000 ALTER TABLE `bank_code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `card`
--

DROP TABLE IF EXISTS `card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `card` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `card_number` varchar(16) NOT NULL,
  `expiration_month` int(2) NOT NULL,
  `expiration_year` int(4) NOT NULL,
  `account_id` int(10) unsigned NOT NULL,
  `pin` int(10) unsigned NOT NULL,
  `international_payments` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `card_account_id_fk_idx` (`account_id`),
  CONSTRAINT `card_account_id_fk` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `card`
--

LOCK TABLES `card` WRITE;
/*!40000 ALTER TABLE `card` DISABLE KEYS */;
INSERT INTO `card` VALUES
(1,'4779456812218754',12,2023,1,4556,0),
(2,'4556233865782415',1,2024,2,1234,1);
/*!40000 ALTER TABLE `card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `standing_transaction_settings`
--

DROP TABLE IF EXISTS `standing_transaction_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `standing_transaction_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime DEFAULT NULL,
  `frequency` enum('min','day','week','month') COLLATE utf8_czech_ci NOT NULL DEFAULT 'day',
  `repeats` int(11) DEFAULT NULL,
  `last_transaction` datetime DEFAULT NULL,
  `paid_count` int(11) DEFAULT '0',
  `active` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `standing_transaction_settings`
--

LOCK TABLES `standing_transaction_settings` WRITE;
/*!40000 ALTER TABLE `standing_transaction_settings` DISABLE KEYS */;
INSERT INTO `standing_transaction_settings` VALUES
(1,'Trvalý příkaz každou minutu 5x','2019-01-05 00:00:00',NULL,'min',5,'2019-01-05 22:27:06',5,0);
/*!40000 ALTER TABLE `standing_transaction_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_detail`
--

DROP TABLE IF EXISTS `transaction_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `transaction_detail` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(10) unsigned DEFAULT NULL,
  `sender_bank_code` varchar(4) NOT NULL,
  `sender_account_number` varchar(45) NOT NULL,
  `amount` double NOT NULL,
  `target_account_number` varchar(45) NOT NULL,
  `target_bank_code` varchar(4) NOT NULL,
  `target_account_id` int(10) unsigned DEFAULT NULL,
  `due_date` datetime NOT NULL,
  `ks` bigint(11) DEFAULT NULL,
  `vs` bigint(11) DEFAULT NULL,
  `ss` bigint(11) DEFAULT NULL,
  `message` varchar(200) DEFAULT NULL,
  `standing_transaction_settings_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `account_id_fk_idx` (`account_id`),
  KEY `transaction_target_account_id_fk_idx` (`target_account_id`),
  KEY `transaction_detail_standing_transaction_settings_fk_idx` (`standing_transaction_settings_id`),
  CONSTRAINT `account_id_fk` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `transaction_detail_standing_transaction_settings_fk` FOREIGN KEY (`standing_transaction_settings_id`) REFERENCES `standing_transaction_settings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `transaction_target_account_id_fk` FOREIGN KEY (`target_account_id`) REFERENCES `account` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=67;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_detail`
--

LOCK TABLES `transaction_detail` WRITE;
/*!40000 ALTER TABLE `transaction_detail` DISABLE KEYS */;
INSERT INTO `transaction_detail` VALUES
(57,NULL,'0100','12-24456200',140000,'4655788890','0880',1,'2019-01-05 00:00:00',12,352655,NULL,'Vyplacení stavebního spoření',NULL),
(58,NULL,'0300','235022565',35000,'1071456789','0880',2,'2019-01-04 00:00:00',232,108256555,3,'Mzda za 12. měsíc',NULL),
(59,1,'0880','4655788890',960,'103546855','0300',NULL,'2019-01-05 00:00:00',NULL,NULL,NULL,'platba mimo tuto banku',NULL),
(60,1,'0880','4655788890',399.9,'1071456789','0880',2,'2019-01-05 00:00:00',2,123456,NULL,'poznámka k úhradě',NULL),
(61,2,'0880','1071456789',1250.6,'4655788890','0880',1,'2019-01-05 00:00:00',1,NULL,NULL,'úhrada od uživatele User0002',NULL),
(62,1,'0880','4655788890',880,'12365589','2700',NULL,'2019-01-05 00:00:00',NULL,NULL,NULL,'',NULL),
(63,1,'0880','4655788890',399.9,'1071456789','0880',2,'2019-01-05 00:00:00',2,123456,NULL,'poznámka k úhradě',NULL),
(64,1,'0880','4655788890',250.5,'2056559863','0800',NULL,'2019-01-05 00:00:00',5,555,NULL,'test trvalých příkazů. 5x po minutě',1),
(65,2,'0880','1071456789',2100,'4655788890','0880',1,'2019-01-05 00:00:00',NULL,NULL,NULL,'',NULL),
(66,1,'0880','4655788890',1269,'452600032','0100',NULL,'2019-01-05 00:00:00',2001,78945612,66,'',NULL);
/*!40000 ALTER TABLE `transaction_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_queue`
--

DROP TABLE IF EXISTS `transaction_queue`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `transaction_queue` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_id` int(10) unsigned NOT NULL,
  `scheduled_time` datetime NOT NULL,
  `processed_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `transaction_queue_transaction_id_fk_idx` (`transaction_id`),
  CONSTRAINT `transaction_queue_transaction_id_fk` FOREIGN KEY (`transaction_id`) REFERENCES `transaction_detail` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_queue`
--

LOCK TABLES `transaction_queue` WRITE;
/*!40000 ALTER TABLE `transaction_queue` DISABLE KEYS */;
INSERT INTO `transaction_queue` VALUES
(1,57,'2019-01-05 18:24:38','2019-01-05 18:24:38'),
(2,58,'2019-01-04 09:15:02','2019-01-04 09:15:02'),
(3,59,'2019-01-05 21:39:05','2019-01-05 21:39:22'),
(4,60,'2019-01-05 21:44:18','2019-01-05 21:44:22'),
(5,61,'2019-01-05 21:45:31','2019-01-05 21:46:00'),
(6,62,'2019-01-05 21:47:07','2019-01-05 21:48:00'),
(7,63,'2019-01-05 21:56:33','2019-01-05 21:57:00'),
(8,64,'2019-01-05 22:23:06','2019-01-05 22:24:00'),
(9,64,'2019-01-05 22:24:06','2019-01-05 22:25:00'),
(10,64,'2019-01-05 22:25:06','2019-01-05 22:26:00'),
(11,64,'2019-01-05 22:26:06','2019-01-05 22:27:00'),
(12,64,'2019-01-05 22:27:06','2019-01-05 22:28:00'),
(13,65,'2019-01-05 23:01:37','2019-01-05 23:01:55'),
(14,66,'2019-01-05 23:08:44','2019-01-05 23:08:55');
/*!40000 ALTER TABLE `transaction_queue` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_template`
--

DROP TABLE IF EXISTS `transaction_template`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `transaction_template` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `account_id` int(10) unsigned NOT NULL,
  `name` varchar(100) NOT NULL,
  `amount` double NOT NULL,
  `account_number` varchar(45) NOT NULL,
  `bank_code` varchar(4) NOT NULL,
  `ks` bigint(11) DEFAULT NULL,
  `vs` bigint(11) DEFAULT NULL,
  `ss` bigint(11) DEFAULT NULL,
  `message` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transaction_template_account_id_fk_idx` (`account_id`),
  CONSTRAINT `transaction_template_account_id_fk` FOREIGN KEY (`account_id`) REFERENCES `account` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_template`
--

LOCK TABLES `transaction_template` WRITE;
/*!40000 ALTER TABLE `transaction_template` DISABLE KEYS */;
INSERT INTO `transaction_template` VALUES
(1,2,'Příkaz k úhradě na účet uživatele User0001',1250.6,'4655788890','0880',1,NULL,NULL,'úhrada od uživatele User0002'),
(2,1,'Platba uživateli User0002',399.9,'1071456789','0880',2,123456,NULL,'poznámka k úhradě');
/*!40000 ALTER TABLE `transaction_template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(8) NOT NULL COMMENT 'Generated 8 character long alphanumeric login',
  `password` varchar(255) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `pin` varchar(10) NOT NULL COMMENT 'Personal identification number without slash separator',
  `street` varchar(100) DEFAULT NULL,
  `house_number` varchar(5) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` enum('cz','sk') COLLATE utf8_czech_ci DEFAULT 'cz',
  `zip` varchar(5) DEFAULT NULL,
  `email` varchar(65) NOT NULL,
  `phone` varchar(13) DEFAULT NULL,
  `role` varchar(20) NOT NULL DEFAULT 'ROLE_USER',
  `deleted` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES
(1,'Admin001','$2a$10$lS3hfjyqPAYZ0gzd1RO7IOwCIvjFVTVUsZVEGrfZoBsEsl3G9uQ3.','Jan','Šedivý','0',NULL,NULL,NULL,NULL,NULL,'admin@greybank.cz',NULL,'ROLE_ADMIN',0),
(2,'User0001','$2a$10$IqFLVOiSIM50XxFotunYJ.WhJM0RzjwM155ie2Jfqr9k7tLlQIfV.','Petr','Novák','9010051324','ulice','123','město','cz','12345','novpetr@seznam.cz','721355966','ROLE_USER',0),
(3,'User0002','$2a$10$VqbzWmMvjdSo5Og5xIc0xuH18adSnSKI3HnKo4MnEgXWsY10lG6Qe','František','Jednička','7505268844','','','',NULL,'','franta.pepa@jednicka.cz','123456','ROLE_USER',0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-05 23:24:24
