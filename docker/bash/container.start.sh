#!/usr/bin/env bash
#/etc/init.d/mysql start
service mysql start
mysql --execute "CREATE DATABASE greybank CHARACTER SET utf8 COLLATE utf8_czech_ci;"
mysql greybank < /home/root/sql/greybank.sql

echo "2" | update-alternatives --config java
mvn -f /home/root/greybank/pom.xml clean package

./usr/local/bin/MailHog &
java -jar /home/root/greybank/target/greybank-1.0-SNAPSHOT.war

